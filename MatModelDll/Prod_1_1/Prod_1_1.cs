using System;

public class Prod_1_1
{
    FormulaParameters fp;

    public Prod_1_1(FormulaParameters _fp)
    {
        fp = _fp;
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (1.1.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        fp.Pz = fp.Pzrezh + fp.Qrezh * fp.oilViscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
    }
        
    /// <summary>
    /// Давление на забое скважины (Па) (1.1.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        fp.Pz = fp.Pzrezh - fp.Qrezh * fp.oilViscosity * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * 
            time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) / (4.0 * fp.pi * fp.k * fp.h);
    }
    
    /// <summary>
    /// Количество свободного газа (д. ед.) (1.2)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void ng(double Pz)
    {
        var tempVar = fp.gasFactor * (Pz - fp.Pnas0) / (fp.Pnu - fp.Pnas0) * 
            (1.0 - fp.nv) * fp.Pnu * fp.Tpl * fp.Z / (Pz * fp.Tnu);
        fp.ng = tempVar / (1.0 + tempVar);
    }

    /// <summary>
    /// Показатель степени в формуле (1.4) (д. ед.) (1.3)
    /// </summary>
    public void nst()
    {
        if (fp.Pnas0 < fp.Pz) fp.nst = 1.0;
        else fp.nst = 1.0 - 0.03 * fp.ng;
    }
    
    /// <summary>
    /// Дебит скважины  (м3 / с) (1.4)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Q(double Pz)
    {
        fp.Q = 2.0 * fp.pi * fp.k * fp.h * Math.Pow(fp.reservoirPressurePa - Pz, fp.nst) / 
                  (fp.oilViscosity * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))));
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (1.5, 1.6)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
    }

    /// <summary>
    /// Обводненность продукции (д. ед.) (1.7)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void nv(double Pz)
    {
        fp.nv = fp.productWaterCut0 + (fp.productWaterCutMax - fp.productWaterCut0) * 
            Math.Pow(fp.reservoirPressurePa - Pz, 2) / fp.DPmax;
    }    
        
    /// <summary>
    /// Скорость подъема жидкости в ЭК (м/с) (1.8, 1.45)
    /// </summary>
    public void Vzhek()
    {
        fp.Vzhek = 4.0 * fp.Q / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
    }
    
    /// <summary>
    /// Плотность жидкости (кг/м3) (1.9, 1.15, 1.41)
    /// </summary>
    public void Rozh()
    {
        fp.Rozh = fp.densityOfOil * (1.0 - fp.nv) + fp.densityOfWater * fp.nv;
    }
    
    /// <summary>
    /// Число Рейнольдса в ЭК (1.10, 1.46)
    /// </summary>
    public void Reek()
    {
        fp.Reek = fp.Vzhek * fp.Rozh * fp.ECInnerDiameter / fp.oilViscosity;
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК (1.11, 1.47)
    /// </summary>
    public void LambdaEk()
    {
        fp.LambdaEk = 0.067 * (158.0 / fp.Reek + 2.0 * fp.EpsEk / fp.ECInnerDiameter);
    }
    
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ (м) (1.12)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg1(double Pz)
    {
        fp.Hg1 = fp.Lk - (fp.reservoirPressurePa - Pz) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                            fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
    }
        
    /// <summary>
    /// Давление на приеме в НКТ (Па) (Па) (1.13)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Ppr2(double Pz)
    {
        if (fp.Hg1 < fp.Lcp) fp.Ppr = Pz - (fp.Lk - fp.Lcp) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                               fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
    }
      
    /// <summary>
    /// Скорость подъема жидкости в НКТ (м/с) (1.14, 1.42)
    /// </summary>
    public void Vzhnkt()
    {
        fp.Vzhnkt = 4.0 * fp.Q / (fp.pi * Math.Pow(fp.dnkt, 2));
    }
      
    /// <summary>
    /// Число Рейнольдса в НКТ (1.16, 1.43)
    /// </summary>
    public void Renkt()
    {
        fp.Renkt = fp.Vzhnkt * fp.Rozh * fp.dnkt / fp.Mjuzh;
    }
    
    /// <summary>
    /// Коэффициент гидравлического трения потока в НКТ (1.17, 1.44)
    /// </summary>
    public void LambdaNkt()
    {
        fp.LambdaNkt = 0.067 * (158.0 / fp.Renkt + 2.0 * fp.EpsNkt / fp.dnkt);
    }
    
    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ в НКТ (м) (1.18)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void Hg2(double Ppr, double Pz)
    {
        fp.Hg2 = fp.Lcp - (Ppr - Pz) / (fp.Rozh * fp.g * Math.Cos(fp.alfa) + 
                                       fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
    }    
    
    /// <summary>
    /// Буферное давление, рассчитанное при помощи распределения давления (Па) (1.19)
    /// </summary>
    /// <param name="Ppr">Давление на приеме в НКТ, Па</param>
    /// <param name="Vzhnkt">Скорость подъема жидкости в НКТ, м/с</param>
    public void Pbufpk(double Ppr, double Vzhnkt)
    {
        fp.Pbufpk = Ppr - fp.Lcp * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                       fp.LambdaNkt * Math.Pow(Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
    }
    
    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера (м) (1.20, 1.67)
    /// </summary>
    public void dl1()
    {
        fp.dl = (fp.L2 - fp.L1) / fp.Nint;
    }
    
    /// <summary>
    /// Температурный градиент потока (К/м) (1.21, 1.51, 1.68)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Q">Дебит скважины, м3/с</param>
    public void Tgrp(double InnerDiam, double Q)
    {
        fp.Tgrp = (0.0034 + 0.79 * fp.Gtgr) / Math.Pow(10, Q / (20.0 * Math.Pow(InnerDiam, 2.67)));
    }
    
    /// <summary> 
    /// Температура в интервале H[j] (К) (1.22, 1.52, 1.69)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Tj(int j)
    {
        fp.T[j] = fp.Tpl - fp.Tgrp * (fp.Lk - fp.H[j]);
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (1.23)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj1(int j)
    {
        fp.Pnas[j] = (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
    }
        
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.24, 1.54, 1.71)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void RfP(int j)
    {
        fp.RfP = (1.0 + Math.Log10(1.0E-6 * fp.P[j])) / (1.0 + Math.Log10(1.0E-6 * fp.Pnas[j])) - 1.0;
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.25, 1.55, 1.72)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void mfT(int j)
    {
        fp.mfT = 1.0 + 0.029 * (fp.T[j] - 293.0) * (fp.densityOfOil * fp.densityOfGas * 1.0E-3 - 0.7966);
    }
    
    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа (1.26, 1.56, 1.73)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void DfT(int j)
    {
        fp.DfT = 1.0E-3 * fp.densityOfOil * fp.densityOfGas * (4.5 - 0.00305 * (fp.T[j] - 293.0)) - 4.785;
    }
    
    /// <summary>
    /// Удельный объем выделившегося газа (м3/м3) (1.27, 1.57, 1.74)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vgvj(int j)
    {
        fp.Vgv[j] = fp.gasFactor * fp.RfP * fp.mfT * (fp.DfT * (1 + fp.RfP) - 1.0);
    }
    
    /// <summary>
    /// Удельный объем смеси (м3/м3) (1.28, 1.58, 1.75)
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Vcm(int j)
    {
        fp.Vcm = fp.bOil + fp.Vgv[j] * fp.Z * fp.Pnu * fp.T[j] / (fp.P[j] * fp.Tnu) + 
                 fp.nv / (1.0 - fp.nv);
    }
    
    /// <summary>
    /// Удельная масса смеси (кг/м3) (1.29, 1.59, 1.76)
    /// </summary>
    public void Mcm()
    {
        fp.Mcm = fp.densityOfOil + fp.densityOfGas * fp.gasFactor + 
                 fp.densityOfWater * fp.nv / (1.0 - fp.nv);
    }
    
    /// <summary>
    /// Плотность газожидкостной смеси (кг/м3) (1.30, 1.60, 1.77)
    /// </summary>
    public void Rocm()
    {
        fp.Rocm = fp.Mcm / fp.Vcm;
    }
    
    /// <summary>
    /// Число Рейнольдса (1.31, 1.61, 1.78)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    public void Re(double InnerDiam)
    {
        fp.Re = 0.99E-5 * 86400.0 * fp.Q * (1.0 - fp.nv) * fp.Mcm / InnerDiam;
    }
    
    /// <summary>
    /// Корреляционный коэффициент (1.32, 1.62, 1.79)
    /// </summary>
    public void f()
    {
        fp.f = Math.Pow(10, 19.66 * Math.Pow(1.0 + Math.Log10(fp.Re), -0.25) - 17.713);
    }
    
    /// <summary>
    /// Дифференциал (Па/м) (1.33, 1.63, 1.80)
    /// </summary>
    /// <param name="InnerDiam">Внутренний диаметр, м</param>
    /// <param name="Rocm">Плотность газожидкостной смеси, кг/м3</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dPdH(double InnerDiam, double Rocm, int j)
    {
        fp.dPdH[j] = Rocm * fp.g * Math.Cos(fp.alfa) + 
                     fp.f * Math.Pow(86400.0 * fp.Q, 2) * Math.Pow(1.0 - fp.nv, 2) * Math.Pow(fp.Mcm, 2) /
                     (2.3024E9 * Rocm * Math.Pow(InnerDiam, 5));
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.34)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void Hj1(int j)
    {
        fp.H[j] = fp.L1 - fp.dl * j;
    }
  
    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.35)
    /// </summary>
    public void hdin1()
    {
        fp.hdin = fp.Lcp - (fp.Ppr - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.densityOfOil * fp.g * Math.Cos(fp.alfa)); 
    } 
    
    /// <summary>
    /// Давление на затрубе (Па) (1.36)
    /// </summary>
    public void Pzatr1()
    {
        fp.Pzatr = (fp.Ppr - fp.densityOfOil * fp.g * (fp.Lcp - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
    }
     
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле (Па) (1.37)
    /// </summary>
    public void Pbuf1()
    {
        fp.Pbuf1 = 0.0729 * Math.Pow(1.0, 2) * fp.Qg * 86400.0 * fp.densityOfGas * fp.Plin /
                   Math.Pow(1000.0 * fp.dsht, 2);
    }
       
    /// <summary>
    /// Расход газа (м3/с) (1.38)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Qg(double Q)
    {
        fp.Qg = Q * fp.Vgvust * fp.Pst * fp.Tu * fp.Z / (fp.Pbufm * fp.Tst);
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле (Па) (1.39)
    /// </summary>
    /// <param name="Q">Дебит скважины, м</param>
    public void Pbuf2(double Q)
    {
        fp.Pbuf2 = fp.Plin + Math.Pow(Q, 2) / (78.8768 * Math.Pow(1000.0 * fp.dsht, 4) * fp.g);
    }
        
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам (Па) (1.40)
    /// </summary>
    /// <param name="Pbuf1">Буферное давление, рассчитанное по первой формуле, Па</param>
    /// <param name="Pbuf2">Буферное давление, рассчитанное по второй формуле, Па</param>
    public void Pbufm(double Pbuf1, double Pbuf2)
    {
        fp.Pbufm = Math.Max(Pbuf1, Pbuf2);
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (1.48)
    /// </summary>
    public void Ppr()
    {
        fp.Ppr = fp.P1nkt + (fp.Lcp - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                fp.LambdaNkt * Math.Pow(fp.Vzhnkt, 2) * fp.Rozh / (2.0 * fp.dnkt));
    }
    
    /// <summary>
    /// Давление на выходе из насоса (Па) (1.49)
    /// </summary>
    public void Pzpk()
    {
        fp.Pzpk = fp.P2ek + (fp.Lk - fp.L1) * (fp.Rozh * fp.g * Math.Cos(fp.alfa) +
                                                fp.LambdaEk * Math.Pow(fp.Vzhek, 2) * fp.Rozh / (2.0 * fp.ECInnerDiameter));
    }
    
    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера (Па) (1.50)
    /// </summary>
    public void dP()
    {
        fp.dP = (fp.Pnas0 - fp.P1nkt) / fp.Nint;
    }
    
    /// <summary>
    /// Давление насыщения нефти газом при T[j] (Па) (1.53, 1.70)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pnasj2(int j)
    {
        fp.Pnas[j] = fp.Pnas0 - (fp.Tpl - fp.T[j]) * 1.0E6 / (9.157 + 701.8 / (fp.gasFactor * (fp.Nc - 0.8 * fp.Na)));
    }
    
    /// <summary>
    /// Число, обратное дифференциалу (1.63) (м/Па) (1.64)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void dHdP(int j)
    {
        fp.dHdP[j] = 1.0 / fp.dPdH[j];
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.65)
    /// </summary>
    /// <param name="N">Количество элементов в массиве dHdP, шт.</param>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    /// <param name="dHdP">Массив значений дифференциала dH/dP на разных интервалах Hj</param>
    public void Hj2(int N, int j, double[] dHdP)
    {
        var sum = 0.0;
        for (var i = 0; i < N; i++) sum += dHdP[i];
        fp.H[j] = fp.L1 + fp.dl * ((dHdP[0] + dHdP[j]) / 2.0 + sum);
    }
    
    /// <summary>
    /// Давление в интервале Н[j] (Па) (1.66)
    /// </summary>
    /// <param name="j">Номер интервала H[j], на котором рассчитывается давление</param>
    public void Pj(int j)
    {
        fp.P[j] = fp.P1nkt + fp.dP * j;
    }
    
    /// <summary>
    /// Интервал, на котором рассчитывается давление и температура (м) (1.81)
    /// </summary>
    /// <param name="j">Номер интервала Hj, на котором рассчитывается давление</param>
    public void Hj3(int j)
    {
        fp.H[j] = fp.L1 + fp.dl * j;
    }    

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (1.82)
    /// </summary>
    public void DPpzo1()
    {
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (1.83)
    /// </summary>
    public void tzab1()
    {
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.oilViscosity)) * fp.rc / 
                   (2.25 * fp.piezoconductivityOfTheFormation); 
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (1.84, 1.85)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz(double time)
    {
        if (time < fp.tzab1)  fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
        else  fp.Pz = fp.Pzost + fp.Qrezh * fp.oilViscosity / (4.0 * fp.pi * fp.k * fp.h) *
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
    }
 
    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.86)
    /// </summary>
    public void htr1()
    {
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3) / (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
    }   

    /// <summary>
    /// Уровень жидкости в НКТ (м) (1.87)
    /// </summary>
    public void htr2()
    {
        fp.htr = fp.Lk - (fp.Pz - fp.Pbuf3 * Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z))) / 
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле (Па) (1.88)
    /// </summary>
    public void Pbuf3()
    {
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
    }

    /// <summary>
    /// Величина динамического уровня в скважине (м) (1.89)
    /// </summary>
    public void hdin2()
    {
        fp.hdin = fp.Lk - (fp.Pz - fp.Pzatr * Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z))) /
            (fp.Rozh * fp.g * Math.Cos(fp.alfa)); 
    }

    /// <summary>
    /// Давление на затрубе (Па) (1.90)
    /// </summary>
    public void Pzatr2()
    {
        fp.Pzatr = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.hdin) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.hdin * fp.densityOfGas / (fp.Tu * fp.Z)); 
    }
    
    
}