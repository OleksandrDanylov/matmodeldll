using System;
using System.Collections.Generic;

public class Prod_1_1_BlockSchemes
{
    private FormulaParameters fp;
    public bool PNPT = false; 
    
    public Prod_1_1_BlockSchemes(FormulaParameters _fp)
    {
        fp = _fp;
    }

    // Алгоритм расчета давления в скважине от забоя при фонтанном способе эксплуатации
    public void Diagram_1_1()
    {
        if (fp.Pnas0 < fp.Pz)
        {
            fp.p11.Vzhek(); // 1.8
            fp.p11.Rozh(); // 1.9
            fp.p11.Reek(); // 1.10
            fp.p11.LambdaEk(); // 1.11
            fp.p11.Hg1(fp.Pz); // 1.12
            if (fp.Lcp < fp.Hg1)
            {
                fp.p11.Ppr2(fp.Pz); // 1.13
                fp.L2 = fp.Lcp;
            }
            else
            {
                fp.L1 = fp.Hg1;
                fp.L2 = fp.Lcp;
                fp.P1nkt = fp.Pnas0;
            }
        }
        else
        {
            fp.L1 = fp.Lk;
            fp.L2 = fp.Lcp;
            fp.P1nkt = fp.Pz;
        }

        if (fp.Lcp < fp.L1)
        {
            fp.p11.dl1(); // 1.20
            fp.p11.Tgrp(fp.ECInnerDiameter, fp.Q); // 1.21
            for (var j = 0; j < fp.N; j++)
            {
                fp.p11.Tj(j); // 1.22
                fp.p11.Pnasj1(j); // 1.23
                fp.p11.RfP(j); // 1.24
                fp.p11.mfT(j); // 1.25
                fp.p11.DfT(j); // 1.26
                fp.p11.Vgvj(j); // 1.27
                fp.p11.Vcm(j); // 1.28
                fp.p11.Mcm(); // 1.29
                fp.p11.Rocm(); // 1.30
                fp.p11.Re(fp.ECInnerDiameter); // 1.31
                fp.p11.f(); // 1.32
                fp.p11.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.33
                fp.p11.Hj1(j); // 1.34
                if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;
            }
        }

        if (fp.Pnas0 < fp.Ppr)
        {
            fp.p11.Vzhnkt(); // 1.14
            fp.p11.Rozh(); // 1.15
            fp.p11.Renkt(); // 1.16
            fp.p11.LambdaNkt(); // 1.17
            fp.p11.Hg2(fp.Ppr, fp.Pz); // 1.18
            if (fp.Hg2 < 0)
            {
                fp.p11.Pbufpk(fp.Ppr, fp.Vzhnkt); // 1.19
                fp.L1 = 0.0;
            }
            else
            {
                fp.L1 = fp.Hg2;
                fp.L2 = 0.0;
                fp.P1nkt = fp.Pnas0;
            }
        }
        else
        {
            fp.L1 = fp.Lcp;
            fp.L2 = 0.0;
            fp.P1nkt = fp.Ppr;
        }
        
        if (0.0 < fp.L1) 
        {
            fp.p11.dl1(); // 1.20
            fp.p11.Tgrp(fp.ECInnerDiameter, fp.Q); // 1.21
            for (var j = 0; j < fp.N; j++)
            {
                fp.p11.Tj(j); // 1.22
                fp.p11.Pnasj1(j); // 1.23
                fp.p11.RfP(j); // 1.24
                fp.p11.mfT(j); // 1.25
                fp.p11.DfT(j); // 1.26
                fp.p11.Vgvj(j); // 1.27
                fp.p11.Vcm(j); // 1.28
                fp.p11.Mcm(); // 1.29
                fp.p11.Rocm(); // 1.30
                fp.p11.Re(fp.ECInnerDiameter); // 1.31
                fp.p11.f(); // 1.32
                fp.p11.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.33
                fp.p11.Hj1(j); // 1.34
                if (Math.Abs(fp.H[j]) < 1.0E-5)
                {
                    fp.P[j] = fp.Pbufpk;
                    fp.Vgv[j] = fp.Vgvust;
                }
            }        
        }
    }
    
    
    // Расчет давления в скважине от устья при фонтанном способе эксплуатации
    public void Diagram_1_2()
    {
        if (fp.Pnas0 <= fp.Pbuf2)
        {
            fp.L1 = 0.0;
            fp.P1nkt = fp.Pbuf2;
            fp.p11.Rozh(); // 1.41
            fp.p11.Vzhnkt(); // 1.42
            fp.p11.Renkt(); // 1.43
            fp.p11.LambdaNkt(); // 1.44
            fp.p11.Ppr(); // 1.48
            fp.L2 = fp.Lcp;
            fp.P2ek = fp.Ppr;
            fp.p11.Rozh(); // 1.41
            fp.p11.Vzhek(); // 1.45
            fp.p11.Reek(); // 1.46
            fp.p11.LambdaEk(); // 1.47
            fp.p11.Pzpk(); // 1.49
        }
        else
        {
            fp.L1 = 0;
            fp.P1nkt = fp.Pbuf2;
            fp.p11.dP(); // 1.50
            fp.p11.Tgrp(fp.dnkt, fp.Q); // 1.51
            for (var j = 0; j < fp.N; j++)
            {
                fp.p11.Tj(j); // 1.52
                fp.p11.Pnasj2(j); // 1.53
                fp.p11.RfP(j); // 1.54
                fp.p11.mfT(j); // 1.55
                fp.p11.DfT(j); // 1.56
                fp.p11.Vgvj(j); // 1.57
                fp.p11.Vcm(j); // 1.58
                fp.p11.Mcm(); // 1.59
                fp.p11.Rocm(); // 1.60
                fp.p11.Re(fp.dnkt); // 1.61
                fp.p11.f(); // 1.62
                fp.p11.dPdH(fp.dnkt, fp.Rocm, j); // 1.63
                fp.p11.dHdP(j); // 1.64
                fp.p11.Hj2(fp.N, j, fp.dHdP); // 1.65
                fp.p11.Pj(j); // 1.66
                if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg2;
            }

            if (fp.Hg2 < fp.Lcp)
            {
                fp.L1 = fp.Lcp;
                fp.P1nkt = fp.Pbuf2;
                fp.p11.dl1(); // 1.67
                fp.p11.Tgrp(fp.dnkt, fp.Q); // 1.68
                for (var j = 0; j < fp.N; j++)
                {
                    fp.p11.Tj(j); // 1.69
                    fp.p11.Pnasj2(j); // 1.70
                    fp.p11.RfP(j); // 1.71
                    fp.p11.mfT(j); // 1.72
                    fp.p11.DfT(j); // 1.73
                    fp.p11.Vgvj(j); // 1.74
                    fp.p11.Vcm(j); // 1.75
                    fp.p11.Mcm(); // 1.76
                    fp.p11.Rocm(); // 1.77
                    fp.p11.Re(fp.dnkt); // 1.78
                    fp.p11.f(); // 1.79
                    fp.p11.dPdH(fp.dnkt, fp.Rocm, j); // 1.80
                    fp.p11.Hj3(j); // 1.81
                    if (Math.Abs(fp.H[j] - fp.Lcp) < 1.0E-5) fp.P[j] = fp.Ppr;
                }

                fp.L1 = fp.Lcp;
                fp.P1nkt = fp.Ppr;
                fp.p11.dP(); // 1.50
                fp.p11.Tgrp(fp.ECInnerDiameter, fp.Q); // 1.51
                for (var j = 0; j < fp.N; j++)
                {
                    fp.p11.Tj(j); // 1.52
                    fp.p11.Pnasj2(j); // 1.53
                    fp.p11.RfP(j); // 1.54
                    fp.p11.mfT(j); // 1.55
                    fp.p11.DfT(j); // 1.56
                    fp.p11.Vgvj(j); // 1.57
                    fp.p11.Vcm(j); // 1.58
                    fp.p11.Mcm(); // 1.59
                    fp.p11.Rocm(); // 1.60
                    fp.p11.Re(fp.ECInnerDiameter); // 1.61
                    fp.p11.f(); // 1.62
                    fp.p11.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.63
                    fp.p11.dHdP(j); // 1.64
                    fp.p11.Hj2(fp.N, j, fp.dHdP); // 1.65
                    fp.p11.Pj(j); // 1.66
                    if (Math.Abs(fp.P[j] - fp.Pnas0) < 1.0E-5) fp.H[j] = fp.Hg1;
                }

                if (fp.Hg1 < fp.Lk)
                {
                    fp.L1 = fp.Lk;
                    fp.P1nkt = fp.Ppr;
                    fp.p11.dl1(); // 1.67
                    fp.p11.Tgrp(fp.ECInnerDiameter, fp.Q); // 1.68
                    for (var j = 0; j < fp.N; j++)
                    {
                        fp.p11.Tj(j); // 1.69
                        fp.p11.Pnasj2(j); // 1.70
                        fp.p11.RfP(j); // 1.71
                        fp.p11.mfT(j); // 1.72
                        fp.p11.DfT(j); // 1.73
                        fp.p11.Vgvj(j); // 1.74
                        fp.p11.Vcm(j); // 1.75
                        fp.p11.Mcm(); // 1.76
                        fp.p11.Rocm(); // 1.77
                        fp.p11.Re(fp.ECInnerDiameter); // 1.78
                        fp.p11.f(); // 1.79
                        fp.p11.dPdH(fp.ECInnerDiameter, fp.Rocm, j); // 1.80
                        fp.p11.Hj3(j); // 1.81
                        if (Math.Abs(fp.H[j] - fp.Lk) < 1.0E-5) fp.P[j] = fp.Pz;
                    }
                }
                else
                {
                    fp.L2 = fp.Hg1;
                    fp.P2ek = fp.Pnas0;
                    fp.p11.Rozh(); // 1.41
                    fp.p11.Vzhek(); // 1.45
                    fp.p11.Reek(); // 1.46
                    fp.p11.LambdaEk(); // 1.47
                    fp.p11.Pzpk(); // 1.49
                }
            }
            else
            {
                fp.L1 = fp.Hg2;
                fp.P1nkt = fp.Pnas0;
                fp.p11.Rozh(); // 1.41
                fp.p11.Vzhnkt(); // 1.42
                fp.p11.Renkt(); // 1.43
                fp.p11.LambdaNkt(); // 1.44
                fp.p11.Ppr(); // 1.48
                fp.L2 = fp.Lcp;
                fp.P2ek = fp.Ppr;
                fp.p11.Rozh(); // 1.41
                fp.p11.Vzhek(); // 1.45
                fp.p11.Reek(); // 1.46
                fp.p11.LambdaEk(); // 1.47
                fp.p11.Pzpk(); // 1.49            }
            }
        }
    }

    // Алгоритм вывода работы скважины на режим при фонтанном способе эксплуатации
    public void Diagram_1_3(double time)
    {
        fp.p11.Pz1(time); // 1.1.1
        if (fp.Pz <= fp.Pnas0)
        {
            fp.p11.ng(fp.Pz); // 1.2
            fp.p11.nst(); // 1.3
            fp.p11.Q(fp.Pz); // 1.4
        }
        fp.p11.MP(fp.Pz); // 1.5, 1.6
        fp.p11.nv(fp.Pz); // 1.7
        Diagram_1_1();
        fp.p11.hdin1(); // 1.35
        fp.p11.Pzatr1(); // 1.36
        fp.p11.Pbuf1(); // 1.37
        fp.p11.Qg(fp.Q); // 1.38
        fp.p11.Pbuf2(fp.Q); // 1.39
        fp.p11.Pbufm(fp.Pbuf1, fp.Pbuf2); // 1.40
        if (1.0E-5 < fp.dsht)
        {
            if (fp.Pbufpk <= fp.Pbuf2)  Diagram_1_4(time);
        } 
        else Diagram_1_5(time);
    }
    
    // Алгоритм установившегося режима работы скважины при фонтанном способе эксплуатации
    public void Diagram_1_4(double time)
    {
        var Pbufmdl = 0.5 * (fp.Pbuf2 + fp.Pbufpk);
        Diagram_1_2();
        var Pzmdl = 0.5 * (fp.Pz_1 + fp.Pz);
        if (Pzmdl <= fp.Pnas0)  
        {
            fp.p11.ng(Pzmdl); // 1.2
            fp.p11.nst(); // 1.3
            fp.p11.Q(Pzmdl); // 1.4
        }
        fp.p11.MP(fp.Pz); // 1.5, 1.6
        fp.p11.nv(fp.Pz); // 1.7
        fp.p11.hdin1(); // 1.35
        fp.p11.Pzatr1(); // 1.36
        fp.p11.Pbuf1(); // 1.37
        fp.p11.Qg(fp.Q); // 1.38
        fp.p11.Pbuf2(fp.Q); // 1.39
        fp.p11.Pbufm(fp.Pbuf1, fp.Pbuf2); // 1.40
        Diagram_1_1();
        if (1.0E-5 < fp.dsht)
        {
            if (1.0E-5 < Math.Abs(fp.dsht - fp.dsht_1))  Diagram_1_3(time);
        } 
        else Diagram_1_5(time);
    }
    
    
    // Алгоритм остановки работы скважины при фонтанном способе эксплуатации
    public void Diagram_1_5(double time)
    {
        if (!PNPT)
        {
            fp.p11.DPpzo1(); // 1.82
            fp.p11.tzab1(); // 1.83
            PNPT = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            fp.p11.Pz(time); // 1.84, 1.85
            fp.p11.Q(fp.Pz); // 1.4
            fp.p11.MP(fp.Pz); // 1.5, 1.6
            fp.p11.Pbuf3(); // 1.88
            fp.p11.htr1(); // 1.86
            fp.p11.htr2(); // 1.87
            fp.p11.hdin2(); // 1.89
            fp.p11.Pzatr2(); // 1.90
        }
    }
}