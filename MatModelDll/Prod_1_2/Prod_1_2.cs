using System;
using System.Collections.Generic;

public class Prod_1_2
{
    FormulaParameters fp;

    public Prod_1_2(FormulaParameters _fp)
    {
        fp = _fp;
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.1.1)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz1(double time)
    {
        fp.Pz = Math.Sqrt(Math.Pow(fp.Pzrezh, 2) + fp.Qrezh * fp.Mjug * fp.Patm * 
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                     Math.Pow((fp.rc * Math.Exp(-fp.skinFactor)), 2)) / 
            (2.0 * fp.pi * fp.k * fp.h));
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.1.2)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz2(double time)
    {
        fp.Pz = Math.Sqrt(Math.Pow(fp.Pzrezh, 2) - fp.Qrezh * fp.Mjug * fp.Patm * 
            Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                     Math.Pow((fp.rc * Math.Exp(-fp.skinFactor)), 2)) / 
            (2.0 * fp.pi * fp.k * fp.h));
    }
    
    /// <summary>
    /// Пластовое давление (Па) (2.2)
    /// </summary>
    public void reservoirPressurePa()
    {
        fp.reservoirPressurePa = Math.Sqrt(Math.Pow(fp.Pz, 2) + fp.Afs * fp.Qg0 + fp.Bfs * Math.Pow(fp.Qg0, 2));
    }
        
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (2.3)
    /// </summary>
    public void Afs()
    {
        fp.Afs = fp.Mjug * fp.Z * fp.Pst * fp.Tpl * Math.Log(fp.Rk / (fp.rc * Math.Exp(-fp.skinFactor))) / 
                 (fp.pi * fp.k * fp.h * fp.Tst);
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления (2.4)
    /// </summary>
    public void Bfs()
    {
        fp.Bfs = fp.Rogst * fp.Z * fp.Pst * fp.Tpl * (1.0 / fp.rc - 1.0 / fp.Rk) /
                 (fp.Kmshpl * Math.Pow(fp.h, 2) * fp.Tst);
    }
    
    /// <summary>
    /// Коэффициент макрошероховатости пласта (м) (2.5)
    /// </summary>
    public void Kmshpl()
    {
        fp.Kmshpl = fp.m * Math.Pow(fp.k / fp.m, 1.0 / 6.0) / 0.05;
    }

    /// <summary>
    /// Содержание механических примесей (кг/м3) (2.6, 2.7)
    /// </summary>
    /// <param name="Pz">Давление на забое скважины, Па</param>
    public void MP(double Pz)
    {
        fp.MP = 0.0;
        if ((fp.reservoirPressurePa - Pz) <= fp.deltaPpred) fp.MP = fp.A1 * (fp.reservoirPressurePa - Pz);
        else fp.MP = fp.A2 * (fp.reservoirPressurePa - Pz) + fp.deltaPpred * (fp.A1 - fp.A2);
    }
    
    /// <summary>
    /// Дебит скважины по воде (м3/с) (2.8)
    /// </summary>
    public void Qzh()
    {
        fp.Qzh = fp.Bzh * fp.DP / fp.DPkr - fp.Bzh;
    }
    
    /// <summary>
    /// Фактическая скорость газа (м/с) (2.9)
    /// </summary>
    /// <param name="Tz">Температура на забое скважины, Па</param>
    public void Vgf(double Tz)
    {
        fp.Vgf = 4.0 * fp.Qg0 * fp.Pst * Tz * fp.Z / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2) * fp.Pz * fp.Tst);
    }
    
    /// <summary>
    /// Плотность газа (кг/м3) (2.10
    /// </summary>
    /// <param name="Tz">Температура на забое скважины, Па</param>
    public void densityOfGas(double Tz)
    {
        fp.densityOfGas = fp.Rogst * fp.Pz * fp.Tst / (fp.Pst * Tz * fp.Z);
    }
    
    /// <summary>
    /// Критическая скорость газа (м/с) (2.11)
    /// </summary>
    public void Vgkr()
    {
        fp.Vgkr = 2.5 * Math.Sqrt(fp.g * fp.dv * (fp.densityOfWater - fp.densityOfGas) / fp.densityOfGas);
    }
    
    /// <summary>
    /// Уровень жидкости в ЭК (м) (2.12)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Hzhek(double tz1, double tz2)
    {
        fp.Hzhek = 4.0 * fp.Qskv * (tz2 - tz1) / (fp.pi * Math.Pow(fp.ECInnerDiameter, 2));
    }
    
    /// <summary>
    /// Уровень жидкости в НКТ (м) (2.13)
    /// </summary>
    /// <param name="tz1">Время первого замера, секунды</param>
    /// <param name="tz2">Время второго замера, секунды</param>
    public void Hzhnkt(double tz1, double tz2)
    {
        fp.Hzhnkt = 4.0 * fp.Qskv * (tz2 - tz1) / (fp.pi * Math.Pow(fp.dnkt, 2));
    }
    
    /// <summary>
    /// Массовый расход газа (кг/с) (2.14, 2.28)
    /// </summary>
    public void G()
    {
        fp.G = fp.Qgpt * fp.Rogst;
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на уровне пласта (K/Па) (2.15)
    /// </summary>
    public void Dpl()
    {
        fp.Dpl = (-0.02034 * (fp.Tpl - 273.0) - 0.124 * fp.reservoirPressurePa * 1.0E-6 + 4.6437) * 1.0E-6;
    }
    
    /// <summary>
    /// Разность температур на уровне пласта и на забое (K) (2.16)
    /// </summary>
    public void DTplz()
    {
        fp.DTplz = fp.Dpl * (fp.reservoirPressurePa - fp.Pz) * 
                   Math.Log10(1.0 + fp.G * fp.Cg * fp.tn / (fp.pi * fp.h * fp.Cgp * Math.Pow(fp.rc, 2))) / 
                   Math.Log10(fp.Rk / fp.rc);
    }
    
    /// <summary>
    /// ??? (???) (2.17, 2.31)
    /// </summary>
    public void fes()
    {
        fp.fes = Math.Log(1.0 + Math.Sqrt(fp.pi * fp.Lgp * fp.tn / Math.Pow(Math.Log(fp.rc), 2)));
    }
    
    /// <summary>
    /// Средний зенитный угол ствола скважины (град.) (2.18, 2.32)
    /// </summary>
    public void alfa()
    {
        fp.alfa = 2.0 * fp.pi * fp.Lgp / (fp.G * fp.Cg * fp.fes);
    }
    
    /// <summary>
    /// Температура на забое скважины (К) (2.19)
    /// </summary>
    public void Tz()
    {
        fp.Tz = fp.Tpl - fp.DTplz;
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на забое (K/Па) (2.20)
    /// </summary>
    public void Dz()
    {
        fp.Dz = (-0.02034 * (fp.Tz - 273.0) - 0.124 * fp.Pz * 1.0E-6 + 4.6437) * 1.0E-6;
    }
    
    /// <summary>
    /// Температура на приеме НКТ (K) (2.21)
    /// </summary>
    public void Tpr()
    {
        fp.Tpr = fp.Tpl - fp.Tgr * (fp.Lk - fp.Lcp) - fp.DTplz * Math.Exp(-fp.alfa * (fp.Lk - fp.Lcp)) +
                 (fp.Tgr - fp.Dz * (fp.Pz - fp.Ppr) / fp.Lk) * (1.0 - Math.Exp(-fp.alfa * (fp.Lk - fp.Lcp))) / fp.alfa;
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па) (2.22)
    /// </summary>
    public void Ppr1()
    {
        fp.Ppr = Math.Sqrt((Math.Pow(fp.Pz, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
    }
    
    /// <summary>
    ///  Средняя температура (K) (2.23, 2.41, 2.79, 2.93)
    /// </summary>
    public void Tcp1()
    {
        fp.Tcp = (fp.Tz + fp.Tpr) / 2.0;
    }
    
    /// <summary>
    ///  Среднее давление (Па) (2.24, 2.42, 2.80, 2.94)
    /// </summary>
    public void Pcp1()
    {
        fp.Pcp = (fp.Pz + fp.Ppr) / 2.0;
    }
    
    /// <summary>
    ///  Объемный расход газа при Р и Т (м3/с) (2.25, 2.36, 2.43, 2.51, 2.75, 2.81, 2.88, 2.95)
    /// </summary>
    public void Qgpt1()
    {
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tcp * fp.Z / (fp.Pcp * fp.Tst);
    }
    
    /// <summary>
    ///  Коэффициент (2.26, 2.44, 2.82, 2.96)
    /// </summary>
    public void KS1()
    {
        fp.KS = 0.03415 * fp.Rogotn * (fp.Lk - fp.Lcp - fp.Hzhek) / (fp.Tcp * fp.Z);
    }
    
    /// <summary>
    ///  Коэффициент (2.27, 2.45, 2.83, 2.97)
    /// </summary>
    public void Teta1()
    {
        fp.Teta = 0.01377 * fp.Lambda * Math.Pow(fp.Tcp * fp.Z, 2) *
            (Math.Exp(2.0 * fp.KS) - 1.0) / Math.Pow(fp.ECInnerDiameter, 5); 
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на спуске НКТ (K/Па) (2.29)
    /// </summary>
    public void Dsp()
    {
        fp.Dsp = (-0.02034 * (fp.Tsp - 273.0) - 0.124 * fp.Psp * 1.0E-6 + 4.6437) * 1.0E-6;
    }
    
    /// <summary>
    /// Температура на буфере (К) (2.30)
    /// </summary>
    public void Tbuf1()
    {
        fp.Tbuf = fp.Tpl - fp.Tgr * fp.Lk - fp.DTplz * Math.Exp(-fp.alfa * fp.Lk) +
                  (fp.Tgr - fp.Dsp * (fp.Pz - fp.Pu) / fp.Lk) * (1.0 - Math.Exp(-fp.alfa * fp.Lk)) / fp.alfa;
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.33)
    /// </summary>
    public void Pbuf1()
    {
        fp.Pbuf1 = Math.Sqrt((Math.Pow(fp.Ppr, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
    }
    
    /// <summary>
    ///  Средняя температура (K) (2.34, 2.49, 2.73, 2.86)
    /// </summary>
    public void Tcp2()
    {
        fp.Tcp = (fp.Tbuf + fp.Tpr) / 2.0;
    }
    
    /// <summary>
    ///  Среднее давление (Па) (2.35, 2.50, 2.74, 2.87)
    /// </summary>
    public void Pcp2()
    {
        fp.Pcp = (fp.Ppr + fp.Pbuf1) / 2.0;
    }
    
    /// <summary>
    ///  Коэффициент (2.37, 2.52, 2.76, 2.89)
    /// </summary>
    public void KS2()
    {
        fp.KS = 0.03415 * fp.Rogotn * (fp.Lcp - fp.Hzhnkt) / (fp.Tcp * fp.Z);
    }

    /// <summary>
    /// Давление на уровне жидкости в ЭК (Па) (2.39, 2.92-1)
    /// </summary>
    public void Pur1()
    {
        fp.Pur1 = fp.Pz - fp.densityOfWater * fp.g * fp.Hzhek;
    }

    /// <summary>
    ///  Коэффициент (2.38, 2.53, 2.77, 2.90)
    /// </summary>
    public void Teta2()
    {
        fp.Teta = 0.01377 * fp.Lambda * Math.Pow(fp.Tcp * fp.Z, 2) *
            (Math.Exp(2.0 * fp.KS) - 1.0) / Math.Pow(fp.dnkt, 5); 
    }

    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.40, 2.92-2)
    /// </summary>
    public void Ppr2()
    {
        fp.Ppr = Math.Sqrt((Math.Pow(fp.Pur1, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.46, 2.91)
    /// </summary>
    public void Ppr3()
    {
        fp.Ppr = fp.Pz - fp.densityOfWater * fp.g * (fp.Lk - fp.Lcp);
    }
    
    /// <summary>
    /// Давление на уровне жидкости в НКТ (Па) (2.47, 2.84)
    /// </summary>
    public void Pur2()
    {
        fp.Pur2 = fp.Ppr - fp.densityOfWater * fp.g * fp.Hzhnkt;
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.48, 2.85)
    /// </summary>
    public void Pbuf2()
    {
        fp.Pbuf2 = Math.Sqrt((Math.Pow(fp.Pur2, 2) - fp.Teta * Math.Pow(fp.Qgpt, 2)) / Math.Exp(2.0 * fp.KS));
    }
        
    /// <summary>
    /// Линейное давление (Па)  (2.54)
    /// </summary>
    public void Plin1()
    {
        fp.Plin = Math.Pow(1000.0 * fp.dsht, 2) * fp.Pbuf1 / 
                  (0.0729 * Math.Pow(1.0, 2) * fp.Qg0 * 86400.0 * fp.densityOfGas);
    }
    
    /// <summary>
    ///  Объемный расход газа при Р и Т (м3/с) (2.55)
    /// </summary>
    public void Qgpt2()
    {
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tbuf * fp.Z / (fp.Pbuf1 * fp.Tst);
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на буфере (K/Па) (2.56, 2.70)
    /// </summary>
    public void Dbuf1()
    {
        fp.Dbuf = (-0.02034 * (fp.Tbuf - 273.0) - 0.124 * fp.Pbuf1 * 1.0E-6 + 4.6437) * 1.0E-6;
    }
    
    /// <summary>
    /// Температура газа после штуцера (К)  (2.57)
    /// </summary>
    public void Tsep1()
    {
        fp.Tsep = fp.Tbuf - (fp.Pbuf1 - fp.Plin) * fp.Dbuf;
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по первой формуле для ДИКТ (Па) (2.58)
    /// </summary>
    public void Pdikt1()
    {
        fp.Pdikt1 = Math.Abs(fp.Plin - fp.DPsep); // Д. Гуменюк, отладка (Abs)
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на ДИКТе (K/Па) (2.59)
    /// </summary>
    public void Ddikt()
    {
        fp.Ddikt = (-0.02034 * (fp.Tsep - 273.0) - 0.124 * fp.Pdikt1 * 1.0E-6 + 4.6437) * 1.0E-6;
    }
    
    /// <summary>
    /// Температура на ДИКТе (K) (2.60)
    /// </summary>
    public void Tdikt()
    {
        fp.Tdikt = fp.Tsep - fp.DPsep * fp.Ddikt;
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по второй формуле для ДИКТ (Па) (2.61)
    /// </summary>
    public void Pdikt2()
    {
        fp.Pdikt2 = fp.Qgpt * Math.Sqrt(fp.Tdikt * fp.Z * fp.Rootn) / (0.864 * fp.Cdikt);
    }
    
    /// <summary>
    /// Объемный расход газа при Р и Т (м3/с) (2.62)
    /// </summary>
    public void Qgpt3()
    {
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tdikt * fp.Z / (fp.Pdikt1 * fp.Tst);
    }
    
    /// <summary>
    /// Коэффициент (2.63, 2.64)
    /// </summary>
    public void Cdikt()
    {
        if (fp.PrDikt == 0) fp.Cdikt = 238966.0 * Math.Pow(fp.dsht, 2.0581);
        else fp.Cdikt = 197817.0 * Math.Pow(fp.dsht, 2.0172);
    }
    
    /// <summary>
    /// Линейное давление (Па)  (2.65)
    /// </summary>
    public void Plin2()
    {
        fp.Plin = fp.Pdikt1 + fp.DPsep;
    }
    
    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на буфере (K/Па) (2.66)
    /// </summary>
    public void Dbuf2()
    {
        fp.Dbuf = (-0.02034 * (fp.Tsep - 273.0) - 0.124 * fp.Pdikt1 * 1.0E-6 + 4.6437) * 1.0E-6;
    }
    
    /// <summary>
    /// Температура газа после штуцера (К)  (2.67)
    /// </summary>
    public void Tsep2()
    {
        fp.Tsep = fp.Tdikt + fp.DPsep * fp.Dbuf;
    }
    
    /// <summary>
    /// Буферное давление (Па) (2.68)
    /// </summary>
    public void Pbuf4()
    {
        fp.Pbuf1 = fp.Plin * 0.00729 * Math.Pow(fp.Fi, 2) * fp.Qgpt * 86400.0 * fp.densityOfGas / 
                   Math.Pow(1000.0 * fp.dsht, 2);
    }
    
    /// <summary>
    ///  Объемный расход газа при Р и Т (м3/с) (2.69)
    /// </summary>
    public void Qgpt4()
    {
        fp.Qgpt = fp.Qg0 * fp.Pst * fp.Tsep * fp.Z / (fp.Plin * fp.Tst);
    }
    
    /// <summary>
    /// Температура на буфере (К) (2.71)
    /// </summary>
    public void Tbuf2()
    {
        fp.Tbuf = fp.Tsep + (fp.Pbuf1 - fp.Plin) * fp.Dbuf;
    }
    
    /// <summary>
    /// Давление на приеме в НКТ (Па)  (2.72)
    /// </summary>
    public void Ppr4()
    {
        fp.Ppr = Math.Sqrt(Math.Pow(fp.Pbuf1, 2) * Math.Exp(2.0 * fp.KS) + fp.Teta * Math.Pow(fp.Qgpt, 2));
    }
    
    /// <summary>
    /// Давление на забое скважины (Па) (2.78)
    /// </summary>
    public void Pz3()
    {
        fp.Pz = Math.Sqrt(Math.Pow(fp.Ppr, 2) * Math.Exp(2.0 * fp.KS) + fp.Teta * Math.Pow(fp.Qgpt, 2));
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента (Па) (2.98)
    /// </summary>
    public void DPpzo1()
    {
        fp.DPpzo1 = 0.95 * (fp.reservoirPressurePa - fp.Pzost); 
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления (с) (2.99)
    /// </summary>
    public void tzab1()
    {
        fp.tzab1 = Math.Exp(4.0 * fp.pi * fp.k * fp.h * fp.DPpzo1 / (fp.Qrezh * fp.Mjug)) * fp.rc /  // Д. Гуменюк, отладка (вязкость)
                   (2.25 * fp.piezoconductivityOfTheFormation); 
    }

    /// <summary>
    /// Давление на забое скважины (Па) (2.100)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz4(double time)
    {
        fp.Pz = fp.Pzost + fp.DPpzo1 * Math.Log(Math.Pow(time, 2)) / Math.Log(fp.tzab1);
    }

    /// <summary>
    /// Давление на забое скважины (Па) (2.101)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void Pz5(double time)
    {
        fp.Pz = Math.Sqrt(fp.KA + fp.KB * Math.Log10(time));
    }

    /// <summary>
    /// Коэффициент (2.102)
    /// </summary>
    public void KA()
    {
        fp.KA = Math.Pow(fp.Pzost, 2) + fp.KB * Math.Log10(2.25 * fp.piezoconductivityOfTheFormation / 
                                                           Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2)) + fp.Kb * Math.Pow(fp.Qrezh, 2); 
    }

    /// <summary>
    /// Коэффициент (2.103)
    /// </summary>
    /// <param name="time">Время, секунды</param>
    public void KB(double time)
    {
        fp.KB = 2.3 * fp.Qrezh * fp.oilViscosity * fp.Z * fp.Pst * fp.reservoirPressurePa * 1.0E-2 / 
            (2.0 * fp.pi * fp.k * fp.h * fp.Tst) * Math.Log(2.25 * fp.piezoconductivityOfTheFormation * time / 
                                                            Math.Pow(fp.rc * Math.Exp(-fp.skinFactor), 2));
    }

    /// <summary>
    /// Буферное давление (Па) (2.104)
    /// </summary>
    public void Pbuf3()
    {
        fp.Pbuf3 = (fp.Pz - fp.Rozh * fp.g * (fp.Lk - fp.htr) * Math.Cos(fp.alfa)) / 
                   Math.Exp(0.03415 * fp.htr * fp.densityOfGas / (fp.Tu * fp.Z));
    }
    
}