using System;

public class Prod_1_2_BlockSchemes
{
    private FormulaParameters fp;
    public bool PNPT = false; 

    public Prod_1_2_BlockSchemes(FormulaParameters _fp)
    {
        fp = _fp;
    }

    public void Diagram_4_1(double time_1, double time)
    {
        if (fp.Vgf < fp.Vgkr)
        {
            if (fp.Hzhek < (fp.Lk - fp.Lcp)) Diagram_4_1_Brench2(time_1, time);
            else Diagram_4_1_Brench3(time_1, time);
        } 
        else Diagram_4_1_Brench1();
        Diagram_4_1_Common();
    }

    public void Diagram_4_2(double time_1, double time)
    {
        Diagram_4_2_Common();
        if (fp.Vgf < fp.Vgkr)
        {
            if (fp.Hzhek < (fp.Lk - fp.Lcp)) Diagram_4_2_Brench2(time_1, time);
            else Diagram_4_2_Brench3(time_1, time);
        } 
        else Diagram_4_2_Brench1();
    }

    public void Diagram_4_3(double time_1, double time)
    {
        fp.p12.Pz2(time); // 2.1
        fp.p12.Afs(); // 2.3
        fp.p12.Bfs(); // 2.4
        fp.p12.reservoirPressurePa(); // 2.2
        fp.p12.Kmshpl(); // 2.5
        fp.p12.MP(fp.Pz); // 2.6, 2.7
        if (fp.DPkr < (fp.reservoirPressurePa - fp.Pz)) 
        {
            fp.p12.Qzh(); // 2.8
            fp.p12.Vgf(fp.Tz); // 2.9
            fp.p12.densityOfGas(fp.Tz); // 2.10
            fp.p12.Vgkr(); // 2.11
        }
        Diagram_4_1(time_1, time);
        fp.p12.Cdikt(); // 2.63, 2.64
        fp.p12.Pdikt2(); // 2.61
        fp.p12.Qgpt3(); // 2.62
    }

    public void Diagram_4_4(double time_1, double time)
    {
        fp.p12.Pdikt1(); // 2.58
        fp.p12.Pdikt2(); // 2.61
        fp.PdiktMdl = 0.5 * (fp.Pdikt1 + fp.Pdikt2);
        Diagram_4_2(time_1, time);
        fp.p12.Pz3(); // 2.78
        var PzMdl = 0.5 * (fp.Pz_1 + fp.Pz);
        fp.p12.Afs(); // 2.3
        fp.p12.Bfs(); // 2.4
        fp.p12.reservoirPressurePa(); // 2.2
        fp.p12.Kmshpl(); // 2.5
        fp.p12.MP(fp.Pz); // 2.6, 2.7
        if (fp.DPkr < (fp.reservoirPressurePa - PzMdl))
        {
            fp.p12.Qzh(); // 2.8
            fp.p12.Vgf(fp.Tz); // 2.9
            fp.p12.densityOfGas(fp.Tz); // 2.10
            fp.p12.Vgkr(); // 2.11
        }
        else
        {
            Diagram_4_1(time_1, time);
            fp.p12.Cdikt(); // 2.63, 2.64
            fp.p12.Pdikt2(); // 2.61
            fp.p12.Qgpt3(); // 2.62
            if (1.0E-5 < Math.Abs(fp.dsht - fp.dsht_1)) Diagram_4_3(time_1, time);
        }
    }

    public void Diagram_4_5(double time_1, double time)
    {
        if (!PNPT)
        {
            fp.p12.DPpzo1(); // 2.98
            fp.p12.tzab1(); // 2.99
            PNPT = true;
        }
        if (fp.Pz < fp.reservoirPressurePa) 
        {
            if (fp.tzab1 < time)
            {
                fp.p12.KA(); // 2.102
                fp.p12.KB(time); // 2.103
                fp.p12.Pz5(time); // 2.101
            }
            else fp.p12.Pz4(time); // 2.100
            fp.p12.Pbuf3(); // 2.104
        }
    }

    private void Diagram_4_1_Brench1()
    {
        fp.p12.G(); // 2.14
        fp.p12.Dpl(); // 2.15
        fp.p12.DTplz(); // 2.16
        fp.p12.fes(); // 2.17
        fp.p12.alfa(); // 2.18
        fp.p12.Tz(); // 2.19
        fp.p12.Dz(); // 2.20
        fp.p12.Tpr(); // 2.21
        fp.p12.Tcp1(); // 2.23
        fp.p12.Pcp1(); // 2.24
        fp.p12.Qgpt1(); // 2.25
        fp.p12.KS1(); // 2.26
        fp.p12.Teta1(); // 2.27
        fp.p12.Ppr1(); // 2.22
        fp.p12.G(); // 2.28
        fp.p12.Dsp(); // 2.29
        fp.p12.Tbuf1(); // 2.30
        fp.p12.fes(); // 2.31
        fp.p12.alfa(); // 2.32
        fp.p12.Tcp2(); // 2.34
        fp.p12.Pcp2(); // 2.35
        fp.p12.Qgpt1(); // 2.36
        fp.p12.KS2(); // 2.37
        fp.p12.Teta2(); // 2.38
        fp.p12.Pbuf1(); // 2.33
    }

    private void Diagram_4_1_Brench2(double time_1, double time)
    {
        fp.p12.Hzhek(time_1, time); // 2.12
        fp.p12.G(); // 2.14
        fp.p12.Dpl(); // 2.15
        fp.p12.DTplz(); // 2.16
        fp.p12.fes(); // 2.17
        fp.p12.alfa(); // 2.18
        fp.p12.Tz(); // 2.19
        fp.p12.Dz(); // 2.20
        fp.p12.Tpr(); // 2.21
        fp.p12.Tcp1(); // 2.23
        fp.p12.Pcp1(); // 2.24
        fp.p12.Qgpt1(); // 2.25
        fp.p12.KS1(); // 2.26
        fp.p12.Teta1(); // 2.27
        fp.p12.Ppr1(); // 2.22
        fp.p12.G(); // 2.28
        fp.p12.Dsp(); // 2.29
        fp.p12.Tbuf1(); // 2.30
        fp.p12.fes(); // 2.31
        fp.p12.alfa(); // 2.32
        fp.p12.Tcp2(); // 2.34
        fp.p12.Pcp2(); // 2.35
        fp.p12.Qgpt1(); // 2.36
        fp.p12.KS2(); // 2.37
        fp.p12.Teta2(); // 2.38
        fp.p12.Pbuf1(); // 2.33
    }

    private void Diagram_4_1_Brench3(double time_1, double time)
    {
        fp.p12.Hzhnkt(time_1, time); // 2.13
        fp.p12.G(); // 2.14
        fp.p12.Dpl(); // 2.15
        fp.p12.DTplz(); // 2.16
        fp.p12.fes(); // 2.17
        fp.p12.alfa(); // 2.18
        fp.p12.Tz(); // 2.19
        fp.p12.Dz(); // 2.20
        fp.p12.Tpr(); // 2.21
        fp.p12.Pur1(); // 2.39
        fp.p12.Tcp1(); // 2.41
        fp.p12.Pcp1(); // 2.42
        fp.p12.Qgpt1(); // 2.43
        fp.p12.KS1(); // 2.44
        fp.p12.Teta1(); // 2.45
        fp.p12.Ppr2(); // 2.40
        fp.p12.G(); // 2.28
        fp.p12.Dsp(); // 2.29
        fp.p12.Tbuf1(); // 2.30
        fp.p12.fes(); // 2.31
        fp.p12.alfa(); // 2.32
        fp.p12.Ppr3(); // 2.46
        fp.p12.Pur2(); // 2.47
        fp.p12.Tcp2(); // 2.49
        fp.p12.Pcp2(); // 2.50
        fp.p12.Qgpt1(); // 2.51
        fp.p12.KS2(); // 2.52
        fp.p12.Teta2(); // 2.53
        fp.p12.Pbuf2(); // 2.48
    }

    private void Diagram_4_1_Common()
    {
        fp.p12.Plin1(); // 2.54
        fp.p12.Qgpt2(); // 2.55
        fp.p12.Dbuf1(); // 2.56
        fp.p12.Tsep1(); // 2.57
        fp.p12.Pdikt1(); // 2.58
        fp.p12.Ddikt(); // 2.59
        fp.p12.Tdikt(); // 2.60
    }

    private void Diagram_4_2_Common()
    {
        fp.p12.Plin2(); // 2.65
        fp.p12.Tsep2(); // 2.67
        fp.p12.Dbuf2(); // 2.66
        fp.p12.Qgpt4(); // 2.69
        fp.p12.Pbuf4(); // 2.68
        fp.p12.Dbuf1(); // 2.70
        fp.p12.Tbuf2(); // 2.71
    }

    private void Diagram_4_2_Brench1()
    {
        fp.p12.Tcp2(); // 2.73
        fp.p12.Qgpt1(); // 2.75
        fp.p12.KS2(); // 2.76
        fp.p12.Teta2(); // 2.77
        fp.p12.Ppr4(); // 2.72
        fp.p12.Pcp2(); // 2.74
        fp.p12.Tcp1(); // 2.79
        fp.p12.Pcp1(); // 2.80
        fp.p12.Qgpt1(); // 2.81
        fp.p12.KS1(); // 2.82
        fp.p12.Teta1(); // 2.83
        fp.p12.Pz3(); // 2.78
    }

    private void Diagram_4_2_Brench2(double time_1, double time)
    {
        fp.p12.Hzhek(time_1, time); // 2.12
        fp.p12.Ppr3(); // 2.91
        fp.p12.Pur2(); // 2.84
        fp.p12.Tcp2(); // 2.86
        fp.p12.Pcp2(); // 2.87
        fp.p12.Qgpt1(); // 2.88
        fp.p12.KS2(); // 2.89
        fp.p12.Teta2(); // 2.90
        fp.p12.Pbuf2(); // 2.85
        fp.p12.Pur1(); // 2.92-1
        fp.p12.Ppr2(); // 2.92-2
        fp.p12.Tcp1(); // 2.79
        fp.p12.Pcp1(); // 2.80
        fp.p12.Qgpt1(); // 2.81
        fp.p12.KS1(); // 2.82
        fp.p12.Teta1(); // 2.83
        fp.p12.Pz3(); // 2.78
    }

    private void Diagram_4_2_Brench3(double time_1, double time)
    {
        fp.p12.Hzhnkt(time_1, time); // 2.13
        fp.p12.Ppr3(); // 2.91
        fp.p12.Tcp1(); // 2.93
        fp.p12.Pcp1(); // 2.94
        fp.p12.Qgpt1(); // 2.95
        fp.p12.KS1(); // 2.96
        fp.p12.Teta1(); // 2.97
        fp.p12.Pur1(); // 2.92-1
        fp.p12.Ppr2(); // 2.92-2
    }
}