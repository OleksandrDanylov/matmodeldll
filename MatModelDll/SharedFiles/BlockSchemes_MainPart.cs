﻿using System;

namespace MatModelDll
{
    public class BlockSchemes_MainPart
    {
        private FormulaParameters fp1;
        private Prod_1_1_BlockSchemes p11bs1;
        private Prod_1_2_BlockSchemes p12bs1;
        private double deltaTime1 = 1.0;

        public FormulaParameters fp
        {
            get => fp1;
            set => fp1 = value;
        }

        public Prod_1_1_BlockSchemes p11bs
        {
            get => p11bs1;
            set => p11bs1 = value;
        } 
        public Prod_1_2_BlockSchemes p12bs
        {
            get => p12bs1;
            set => p12bs1 = value;
        }

        public double deltaTime
        {
            get => deltaTime1;
            set => deltaTime1 = value;
        }
        // private static int Nmax = 7200;
    
        public BlockSchemes_MainPart()
        {
            fp = new FormulaParameters();
            fp.time = 0.0;
            fp.time_1 = 0.0;
            p11bs = new Prod_1_1_BlockSchemes(fp);
            p12bs = new Prod_1_2_BlockSchemes(fp);
        }

        public void Cycle()
        {
            fp.time_1 = fp.time;
            fp.time += deltaTime;
            fp.tn += deltaTime;
            // Console.WriteLine("Cycle(): time = " + fp.time);

            // task attribute:
            // 0   - Overhaul_1_3,
            // 10  - HydroDynInv_1_1,
            // 20  - Prod_1_5 (Diagram_1_1 - Diagram_1_5),
            // 21  - Prod_1_5 (Diagram_1_6),
            // 25  - Prod_1_5 (Diagram_2_1 - Diagram_2_5),
            // 26  - Prod_1_5 (Diagram_2_6),
            // 30  - HydroDynInv_1_3 (Diagram_4_1 - Diagram_4_3),
            // 31  - HydroDynInv_1_3 (Diagram_4_4),
            // 32  - HydroDynInv_1_3 (Diagram_4_5),
            // 40  - Prod_1_4,
            // 50  - Prod_1_2 (Diagram_4_1 - Diagram_4_3),
            // 51  - Prod_1_2 (Diagram_4_4),
            // 52  - Prod_1_2 (Diagram_4_5),
            // 60  - Overhaul_1_6,
            // 70  - Prod_1_6 (Diagram_1_1 - Diagram_1_3),
            // 71  - Prod_1_6 (Diagram_1_4),
            // 72  - Prod_1_6 (Diagram_1_5),
            // 80  - HydroDynInv_1_6
            // 90  - Prod_1_1 (Diagram_1_1 - Diagram_1_3)
            // 91  - Prod_1_1 (Diagram_1_1, Diagram_1_2,  Diagram_1_4)
            // 92  - Prod_1_1 ( Diagram_1_5)
            // 100 - Prod_1_3 (Diagram_2_1 - Diagram_2_6)
            // 101 - Prod_1_3 (Diagram_2_1 - Diagram_2_7)
            // 102 - Prod_1_3 (Diagram_2_8)
            // 110 - Overhaul_1_7
            switch (fp.pTask)
            {
                case 0:
                    // o13bs.Diagram(fp.time);
                    break;
                case 10: 
                    break;
                case 20: 
                    // p15bs.Diagram_1_1();
                    // p15bs.Diagram_1_2();
                    // p15bs.Diagram_1_3();
                    // p15bs.Diagram_1_4(fp.time);
                    // p15bs.Diagram_1_5(fp.time);
                    break;
                case 21: 
                    // p15bs.Diagram_1_6(fp.time);
                    break;
                case 25: 
                    // p15bs.Diagram_2_1();
                    // p15bs.Diagram_2_2();
                    // p15bs.Diagram_2_3();
                    // p15bs.Diagram_2_4(fp.time);
                    // p15bs.Diagram_2_5(fp.time);
                    break;
                case 26: 
                    // p15bs.Diagram_2_6(fp.time);
                    break;
                case 30: // HydroDynInv_1_3 (Diagram_4_1 - Diagram_4_3)
                    // if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    //     h13bs.Diagram_4_3(fp.time_1, fp.time);
                    break;
                case 31: // HydroDynInv_1_3 (Diagram_4_4)
                    // if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    //     h13bs.Diagram_4_4(fp.time_1, fp.time);
                    break;
                case 32: // HydroDynInv_1_3 (Diagram_4_5)
                    // if (fp.dsht <= 1.0E-5) h13bs.Diagram_4_5(fp.time_1, fp.time);
                    break;
                case 40: break;
                case 50: // Prod_1_2 (Diagram_4_1 - Diagram_4_3)
                    if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                        p12bs.Diagram_4_3(fp.time_1, fp.time);
                    break;
                case 51: // Prod_1_2 (Diagram_4_4)
                    if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                        p12bs.Diagram_4_4(fp.time_1, fp.time);
                    break;
                case 52: // Prod_1_2 (Diagram_4_5)
                    if (fp.dsht <= 1.0E-5) p12bs.Diagram_4_5(fp.time_1, fp.time);
                    break;
                case 60: // Overhaul_1_6
                    // o16bs.Diagram(fp.time);
                    break;
                case 70: // Prod_1_6 (Diagram_1_1 - Diagram_1_3)
                    // if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    // p16bs.Diagram_1_3(fp.time);
                    break;
                case 71: // Prod_1_6 (Diagram_1_4)
                    // if ((1.0E-5 < fp.dsht) && (1.0E-5 <= Math.Abs(fp.Pdikt1 - fp.Pdikt2)))
                    //     p16bs.Diagram_1_4(fp.time);
                    break;
                case 72: // Prod_1_6 (Diagram_1_5)
                    // if (fp.dsht <= 1.0E-5) p16bs.Diagram_1_5(fp.time);
                    break;
                case 80: break;
                case 90: // Prod_1_1 (Diagram_1_1 - Diagram_1_3)
                    p11bs.Diagram_1_1();
                    p11bs.Diagram_1_2();
                    p11bs.Diagram_1_3(fp.time);
                    break;
                case 91: // Prod_1_1 (Diagram_1_1, Diagram_1_2,  Diagram_1_4)
                    p11bs.Diagram_1_1();
                    p11bs.Diagram_1_2();
                    p11bs.Diagram_1_4(fp.time);
                    break;
                case 92: // Prod_1_1 ( Diagram_1_5)
                    p11bs.Diagram_1_5(fp.time);
                    break;
                case 100: // Prod_1_3 (Diagram_2_1 - Diagram_2_6)
                    // p13bs.Diagram_2_1();
                    // p13bs.Diagram_2_2();
                    // p13bs.Diagram_2_3();
                    // p13bs.Diagram_2_4();
                    // p13bs.Diagram_2_5();
                    // p13bs.Diagram_2_6(fp.time);
                    break;
                case 101: // Prod_1_3 (Diagram_2_1 - Diagram_2_7)
                    // p13bs.Diagram_2_1();
                    // p13bs.Diagram_2_2();
                    // p13bs.Diagram_2_3();
                    // p13bs.Diagram_2_4();
                    // p13bs.Diagram_2_5();
                    // p13bs.Diagram_2_6(fp.time);
                    // p13bs.Diagram_2_7(fp.time);
                    break;
                case 102: // Prod_1_3 (Diagram_2_8)
                    // p13bs.Diagram_2_8(fp.time);
                    break;
                case 110: // Overhaul_1_7
                    break;default: break;
            }
            // Other calculations
            fp.Pz_1 = fp.Pz;
            fp.dsht_1 = fp.dsht;
        }

        static void Main(string[] args)
        {
            BlockSchemes_MainPart BS_MP = new BlockSchemes_MainPart(); 
            // for ( ; ; ) BS_MP.Cycle(); // Использовать только в exe-шнике при отладке
        }
    }
}