using System;

public class FormulaParameters
{
    public Prod_1_1 p11;
    public Prod_1_2 p12;

    // task attribute:
    // 0   - Overhaul_1_3,
    // 10  - HydroDynInv_1_1,
    // 20  - Prod_1_5 (Diagram_1_1 - Diagram_1_5),
    // 21  - Prod_1_5 (Diagram_1_6),
    // 25  - Prod_1_5 (Diagram_2_1 - Diagram_2_5),
    // 26  - Prod_1_5 (Diagram_2_6),
    // 30  - HydroDynInv_1_3 (Diagram_4_1 - Diagram_4_3),
    // 31  - HydroDynInv_1_3 (Diagram_4_4),
    // 32  - HydroDynInv_1_3 (Diagram_4_5),
    // 40  - Prod_1_4,
    // 50  - Prod_1_2 (Diagram_4_1 - Diagram_4_3),
    // 51  - Prod_1_2 (Diagram_4_4),
    // 52  - Prod_1_2 (Diagram_4_5),
    // 60  - Overhaul_1_6,
    // 70  - Prod_1_6 (Diagram_1_1 - Diagram_1_3),
    // 71  - Prod_1_6 (Diagram_1_4),
    // 72  - Prod_1_6 (Diagram_1_5),
    // 80  - HydroDynInv_1_6
    // 90  - Prod_1_1 (Diagram_1_1 - Diagram_1_3)
    // 91  - Prod_1_1 (Diagram_1_1, Diagram_1_2,  Diagram_1_4)
    // 92  - Prod_1_1 ( Diagram_1_5)
    // 100 - Prod_1_3 (Diagram_2_1 - Diagram_2_6)
    // 101 - Prod_1_3 (Diagram_2_1 - Diagram_2_7)
    // 102 - Prod_1_3 (Diagram_2_8)
    // 110 - Overhaul_1_7
    // 120 - Отчет по глушению скважины методом прямой и обратной промывки
    
    private int pTask1 = 90;  
    public int pTask
    {
        get => pTask1;
        set => pTask1 = value;
    }
    public FormulaParameters()
    {
        p11 = new Prod_1_1(this);
        p12 = new Prod_1_2(this);
        setNewPTask(pTask);
    }
    
    public void setNewPTask(int newPTask)
    {
        pTask = newPTask;
        
        densityOfWater1 = 1100.0; reservoirPressurePa1 = 20.0E6; Tpl1 = 30.0; Lcp1 = 1990.0; Kbr1 = 5.0E-2;
        switch(pTask)
        {
            case 0:    break;        
            case 10:   break;        
            case 20:   break;        
            case 21:   goto case 20;
            case 25:   goto case 20;
            case 26:   goto case 20;
            case 30:   reservoirPressurePa1 = 15.0E6; Tpl1 = 60.0; break;        
            case 31:   goto case 30;
            case 32:   goto case 30;
            case 40:   break;        
            case 50:   reservoirPressurePa1 = 15.0E6; Tpl1 = 60.0; break;        
            case 51:   goto case 50;
            case 52:   goto case 50;
            case 60:   break;        
            case 70:   break;        
            case 71:   goto case 70;
            case 72:   goto case 70;
            case 80:   reservoirPressurePa1 = 25.0E6; break;        
            case 90:   break;        
            case 91:   goto case 90;
            case 92:   goto case 90;
            case 100:  reservoirPressurePa1 = 15.0E6; break;        
            case 101:  goto case 100;
            case 102:  goto case 100;
            case 110:  Lcp1 = 1600.0; Kbr1 = 10.0E-2; break;        
            case 120:  break;        
            default:   break;        
        }
        switch(pTask) // ИД от Д. Гуменюка
        {
            case 0:
                speedNumber1 = 2;
                q1 = 8.5E-3;
                Ropzh1 = 1070.3;
                reservoirPressurePa1 = 16.0E6;
                Kbr1 = 5.0E-2;
                Vc1 = 34.6;
                Vnkt1 = 6.0;
                Vtnkt1 = 0.00000001;
                densityOfWater1 = 1100.0;
                Cp1 = 0.036;
                Pu1 = 15.0E6;
                Xnkt1 = 125.27;
                densityOfKillingFluid1 = 1070.0;
                Retr1 = 2320.0;
                LambdaKp1 = 0.037;
                LambdaNkt1 = 0.037;
                Fi1 = 100.0;
                Xkp1 = 0.33;
                break;
            case 10:
                break;
            case 20:
                N1 = 5;
                H1 = new double[5];                
                T1 = new double[5];                
                P1 = new double[5];
                Pnas1 = new double[5];
                // Hg21 = ; 
                
                break;
            case 21:
                goto case 20;
            case 25:
                goto case 20;
            case 26:
                goto case 20;
            case 30:  
                EpsEk1 = 1.0E-4;
                EpsNkt1 = 1.0E-4;
                Fi1 = 100.0;
                Pu1 = 15.0E6;
                Pzatr1 = 15.0E6;
                Pzatr01 = 15.0E6;
                Pz1 = 15505347.2;
                Ppr1 = 15.0E6;
                Rogst1 = 15.0;
                Tsep1 = 189.0;
                Rootn1 = 15.0;
                Tz1 = 287.0;
                Pcp1 = 14.95555E6;
                Qg01 = 1.14;
                Pbuf11 = 14585533.98;
                Pbuf21 = 14585533.98;
                Pbufm1 = 14585533.98;
                Qgpt1 = 0.00162;
                Pzrezh1 = 16.0E6;
                Lk1 = 2000.0;
                Rogotn1 = 15.0;
                Pdikt11 = 1.0E6;
                Pdikt21 = 2.0E6;
                Vgkr1 = 2.58;
                Qrezh1 = 1.45;
                Mjug1 = 0.000016;
                Patm1 = 101300.0;
                piezoconductivityOfTheFormation1 = 6.0;
                rc1 = 0.073;
                skinFactor1 = 2.0;
                k1 = 1.1E-12;
                h1 = 12.0;
                Afs1 = 8.84428E11;
                Bfs1 = 20141985264388.0;
                Z1 = 1.2;
                Pst1 = 101300.0;
                Tpl1 = 273.0;
                Rk1 = 250.0;
                Tst1 = 293.0;
                Kmshpl1 = 1.0E-9;
                m1 = 0.2;
                reservoirPressurePa1 = 16.0E6;
                deltaPpred1 = 25.0E6;
                A11 = 3.0E-3;
                A21 = 3.0E-3;
                Bzh1 = 5.0 * 1.157E-5;
                DP1 = 1.0E6;
                DPkr1 = 6.0E6;
                ECInnerDiameter1 = 153.4E-3;
                dv1 = 0.009;
                densityOfWater1 = 1100.0;
                densityOfGas1 = 1.5;
                Qskv1 = -4.82E-05;
                Cg1 = 2300.0;
                tn1 = 1.0;
                Cgp1 = 2000.0;
                Lgp1 = 5000.0;
                Tgr1 = 0.017;
                Lcp1 = 1990.0;
                Lambda1 = 0.014;
                Tsp1 = 340.286373;
                Psp1 = 15091077.07;
                dnkt1 = 62.0E-3;
                dsht1 = (pTask != 32 ? 0.018 : 0.0);
                DPsep1 = 3.0E4;
                PrDikt1 = 0;
                Pzost1 = 14580387.75;
                oilViscosity1 = 5.0E-3;
                Kb1 = 884427965754.0;
                Tu1 = 302.1567;
                Plin1 = 221897.3302;
                break;        
            case 31:
                Cdikt1 = 61.3;
                goto case 30;
            case 32: goto case 30;
            case 40:
                break;
            case 50:  
                EpsEk1 = 1.0E-4;
                EpsNkt1 = 1.0E-4;
                Fi1 = 100.0;
                Pu1 = 15.0E6;
                Pzatr1 = 15.0E6;
                Pzatr01 = 15.0E6;
                Pz1 = 15505347.2;;
                Ppr1 = 15.0E6;
                Rogst1 = 1.144282744; // 15.0;
                Tsep1 = 189.0;
                Rootn1 = 15.0;
                Tz1 = 287.0;
                Pcp1 = 14.95555E6;
                Qg01 = 1.14;
                Pbuf11 = 14585533.98;
                Pbuf21 = 14585533.98;
                Pbufm1 = 14585533.98;
                Qgpt1 = 0.00162;
                Pzrezh1 = 16.0E6;
                Lk1 = 2000.0;
                Rogotn1 = 15.0;
                Pdikt11 = 1.0E6;
                Pdikt21 = 2.0E6;
                Vgkr1 = 2.58;
                Qrezh1 = 1.45;
                Mjug1 = 0.000016;
                Patm1 = 101300.0;
                piezoconductivityOfTheFormation1 = 6.0;
                rc1 = 0.073;
                skinFactor1 = 2.0;
                k1 = 1.1E-12;
                h1 = 12.0;
                Afs1 = 8.84428E11;
                Bfs1 = 20141985264388.0;
                Z1 = 1.885643833; // 1.2;
                Pst1 = 101300.0;
                Tpl1 = 343.0; // 273.0;
                Rk1 = 300.0; // 250.0;
                Tst1 = 293.0;
                Kmshpl1 = 1.2E-9; // 1.0E-9;
                m1 = 0.2;
                reservoirPressurePa1 = 16.0E6;
                deltaPpred1 = 25.0E6;
                A11 = 3.0E-3;
                A21 = 3.0E-3;
                Bzh1 = 5.0 * 1.157E-5;
                DP1 = 1.0E6;
                DPkr1 = 6.0E6;
                ECInnerDiameter1 = 153.4E-3;
                dv1 = 0.009;
                densityOfWater1 = 1100.0;
                densityOfGas1 = 1.5;
                Qskv1 = -4.82E-05;
                Cg1 = 2300.0;
                tn1 = 1.0;
                Cgp1 = 2000.0;
                Lgp1 = 5000.0;
                Tgr1 = 0.017;
                Lcp1 = 1990.0;
                Lambda1 = 0.014;
                Tsp1 = 340.286373;
                Psp1 = 15091077.07;
                dnkt1 = 62.0E-3;
                dsht1 = (pTask != 52 ? 0.018 : 0.0);
                DPsep1 = 3.0E4;
                PrDikt1 = 0;
                Pzost1 = 14580387.75;
                oilViscosity1 = 5.0E-3;
                Kb1 = 884427965754.0;
                Tu1 = 302.1567;
                Plin1 = 221897.3302;
                break;        
            case 51:
                Cdikt1 = 61.3;
                goto case 50;
            case 52: goto case 50;
            case 60:
                q1 = 8.5E-3;
                Xnkt1 = 125.27;
                Rozhp1 = 1414.4;
                Qminv1 = 0.07;
                Vzhgrp1 = 76.3;
                Vnkt1 = 6.0;
                Vbek1 = 0.18;
                Pvg1 = 49.05E6;
                LambdaNkt1 = 0.034;
                break;
            case 70:
                Pdikt11 = 1.0E6;
                Pdikt21 = 2.0E6;
                Pz1 = 15505347.2;
                Pzrezh1 = 16.0E6;
                Qrezh1 = 1.45;
                piezoconductivityOfTheFormation1 = 6.0;
                rc1 = 0.073;
                skinFactor1 = 2.0;
                k1 = 1.1E-12;
                h1 = 12.0;
                Mjuzh1 = 1.0;
                reservoirPressurePa1 = 16.0E6;
                Rk1 = 250.0;
                Pgrp1 = 35.0E6;
                Q1 = 0.0003927776668;
                ECInnerDiameter1 = 153.4E-3;
                Vzhek1 = 33.93599041;
                Rozh1 = 1.0;
                EpsEk1 = 1.0E-4;
                Lk1 = 2000.0;
                Lcp1 = 1990.0;
                LambdaEk1 = 0.03656601717;
                dnkt1 = 62.0E-3;
                EpsNkt1 = 1.0E-4;
                Vzhnkt1 = 0.004;
                Plin1 = 221897.3302;
                dsht1 = (pTask != 72 ? 0.018 : 0.0);
                alfa1 = 0.0;
                Pens11 = 14585533.98;
                Pens21 = 15.0E6;
                LambdaNkt1 = 0.034;
                Pzost1 = 14580387.75;
                break;
            case 71:
                Cdikt1 = 61.3;
                goto case 70;
            case 72: 
                goto case 70;
            case 80:
                break;
            case 90: 
                N1 = 1;
                H1 = new double[1] {5.0};                
                T1 = new double[1] {282.0};                
                P1 = new double[1] {18795591.4};
                Pnas1 = new double[1] {18795591.4};
                dPdH1 = new double[1];                
                dHdP1 = new double[1];                
                Vgv1 = new double[1];                
                Pz1 = 15000000.0;
                Q1 = 0.0001590194937;
                nv1 = 0.00000000000001;
                EpsEk1 = 0.00000000000001;
                Lk1 = 2000.0;
                Hg11 = 1500.0;
                Nint1 = 1;
                Pnu1 = 101300.0;
                Tnu1 = 273.0;
                Ppr1 = 18648350.94;
                Mjuzh1 = 850.0;
                EpsNkt1 = 0.00000000000001;
                Pbufpk1 = 0.8;
                Vgvust1 = 13.17197416;
                Pbuf21 = 800000.0;
                P1nkt1 = 18000000.0;
                L11 = 1500.0;
                P2ek1 = 1250.0;
                dl1 = 5.0;
                Qrezh1 = 0.001184722824;
                Pzost1 = 18795591.4;
                tzab1 = time;
                htr1 = 1544.831279;
                Tu1 = 282.5;
                Pzatr1 = 800000.0;
                hdin1 = 1544.831279;
                break;
            case 91:
                goto case 90;
            case 92: 
                goto case 90;
            case 100:
                
                break;
            case 101:  
                goto case 100;
            case 102:  
                goto case 100;
            default:  
                break;        
        }
    }
    
    /// <summary>
    ///  Плотность жидкости глушения, кг/м3
    /// </summary>
    private double densityOfKillingFluid1;

    /// <summary>
    /// Вязкость жидкости глушения, Па•с
    /// </summary>
    private double viscosityOfKillingFluid1 = 25.0E-3;

    /// <summary>
    /// Плотность песчано-жидкостной смеси, кг/м3 
    /// </summary>
    private double RoP1; 

    /// <summary>
    /// Вязкость песчано-жидкостной смеси, Па•с 
    /// </summary>
    private double MjuP1 = 60.0E-3; 

    /// <summary>
    ///  Давление поглощения ТЖ пластом, Па
    /// </summary>
    private double Ppgl1; 

    /// <summary>
    /// Давление ГНВП, Па 
    /// </summary>
    private double Pgnvp1; 

    /// <summary>
    /// Давление гидроразрыва пласта, Па 
    /// </summary>
    private double Pgrp1; 

    /// <summary>
    /// Плотность промывочной жидкости (ЖГС), кг/м3 
    /// </summary>
    private double Ropzh1; 

    /// <summary>
    /// Ускорение свободного падения, м/с2
    /// </summary>
    private double g1 = 9.81;

    /// <summary>
    ///  Средний зенитный угол ствола скважины, град.
    /// </summary>
    private double alfa1 = 0.0; 

    /// <summary>
    /// Коэффициент запаса, учитывающий объем поглощения жидкости глушения при глушении, д. ед. 
    /// </summary>
    private double Kz1 = 5.0E-2; 

    /// <summary>
    /// Давление опрессовки ЭК, Па 
    /// </summary>
    private double ekCrimpingPressure1 = 20.0E6; 

    /// <summary>
    ///  Абсолютная шероховатость труб, м
    /// </summary>
    private double K1 = 0.0001; 

    /// <summary>
    ///  Абсолютная шероховатость внутренней поверхности ЭК, м
    /// </summary>
    private double EpsEk1; 

    /// <summary>
    ///  Абсолютная шероховатость внутренней поверхности НКТ, м
    /// </summary>
    private double EpsNkt1; 

    /// <summary>
    /// Число pi
    /// </summary>
    private double pi1 = 3.14;

    /// <summary>
    /// Подача насосного агрегата ЦА-320 на II скорости, м3/с 
    /// </summary>
    private double Ca320Q21 = 2.9E-3; 

    /// <summary>
    /// Подача насосного агрегата ЦА-320 на III скорости, м3/с 
    /// </summary>
    private double Ca320Q31 = 5.2E-3; 

    /// <summary>
    /// Подача насосного агрегата ЦА-320 на IV скорости, м3/с 
    /// </summary>
    private double Ca320Q41 = 7.9E-3; 

    /// <summary>
    /// Подача насосного агрегата ЦА-320 на V скорости, м3/с 
    /// </summary>
    private double Ca320Q51 = 11.9E-3; 

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на I скорости, м3/с 
    /// </summary>
    private double An500Q11 = 5.1E-3; 

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на II скорости, м3/с 
    /// </summary>
    private double An500Q21 = 5.92E-3; 

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на III скорости, м3/с 
    /// </summary>
    private double An500Q31 = 7.33E-3; 

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на IV скорости, м3/с 
    /// </summary>
    private double An500Q41 = 8.92E-3; 

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на V скорости, м3/с 
    /// </summary>
    private double An500Q51 = 11.55E-3; 

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на VI скорости, м3/с 
    /// </summary>
    private double An500Q61 = 14.95E-3; 

    /// <summary>
    /// Давление в выкидном коллекторе (линии), Па 
    /// </summary>
    private double Pkol1 = 1.0E6; 

    /// <summary>
    /// Глубина скважины по вертикали, м 
    /// </summary>
    private double Lc1 = 2000.0; 

    /// <summary>
    ///  Длина скважины (по стволу), м
    /// </summary>
    private double lc1 = 2000.0; 

    /// <summary>
    /// Глубина спуска НКТ в скважину (по вертикали), м 
    /// </summary>
    private double Lcp1 = 1990.0; 

    /// <summary>
    /// Внутренний диаметр эксплуатационной колонны, м 
    /// </summary>
    private double ECInnerDiameter1 = 153.4E-3; 

    /// <summary>
    /// Внешний диаметр эксплуатационной колонны, м 
    /// </summary>
    private double ECOutDiameter1 = 168.0E-3; 

    /// <summary>
    /// Внешний диаметр НКТ, м 
    /// </summary>
    private double Dnkt1 = 73.0E-3; 

    /// <summary>
    /// Внутренний диаметр НКТ, м 
    /// </summary>
    private double dnkt1 = 62.0E-3; 

    /// <summary>
    /// Внутренний диаметр, м 
    /// </summary>
    private double InnerDiam1; 

    /// <summary>
    /// Пластовое давление, Па 
    /// </summary>
    private double reservoirPressurePa1; 

    /// <summary>
    /// Степень открытия задвижки, % (100% - задвижка полностью открыта) 
    /// </summary>
    private double Fi1; 

    /// <summary>
    /// Внутренний диаметр отвода фонтанной арматуры, м 
    /// </summary>
    private double dotv1 = 65.0E-3; 

    /// <summary>
    ///  Число насадок в перфораторе, шт
    /// </summary>
    private double Nperf1 = 4.0; 

    /// <summary>
    /// Внутренний диаметр насадок перфоратора, м 
    /// </summary>
    private double Dperf1 = 0.0045; 

    /// <summary>
    /// Плотность песчано-жидкостной смеси, кг/м3 
    /// </summary>
    private double RoPzhs1; 

    /// <summary>
    /// Плотность жидкости затворения (воды), кг/м3 
    /// </summary>
    private double densityOfWater1;

    /// <summary>
    /// Плотность песка, кг/м3 
    /// </summary>
    private double densityOfSand1 = 2700.0; 

    /// <summary>
    /// Массовая доля песка в песчано-жидкостной смеси, кг/м3 
    /// </summary>
    private double Co1 = 100.0; 

    /// <summary>
    /// Коэффициент расхода 
    /// </summary>
    private double z1 = 0.82; 

    /// <summary>
    /// Потери давления в насадках, Па 
    /// </summary>
    private double DPn1 = 18.0E6; 

    /// <summary>
    /// Потери давления в полости, образованной абразивной струей, Па  
    /// </summary>
    private double DPp1 = 3.5E6; 

    /// <summary>
    /// Коэффициент безопасности работ, д. ед.
    /// </summary>
    private double Kbr1 = 5.0E-2; 

    /// <summary>
    /// Общее требуемое количество жидкости для проведения ГПП, м3  
    /// </summary>
    private double Qrej1; 

    /// <summary>
    /// Объем скважины, м3  
    /// </summary>
    private double Vc1; 

    /// <summary>
    /// Внутренний объем НКТ, м3  
    /// </summary>
    private double Vnkt1; 

    /// <summary>
    /// Объем жидкости, вытесняемой насосно-компрессорными трубами, м3  
    /// </summary>
    private double Vtnkt1; 

    /// <summary>
    /// Объем эксплуатационной колонны под башмаком НКТ, м3  
    /// </summary>
    private double Vbek1; 

    /// <summary>
    /// Объем кольцевого пространства скважины, м3  
    /// </summary>
    private double Vkp1; 

    /// <summary>
    /// Требуемый объем песчано-жидкостной смеси, м3  
    /// </summary>
    private double Vpzhs1; 

    /// <summary>
    /// Требуемый объем промывочной жидкости, м3  
    /// </summary>
    private double Vpzh1; 

    /// <summary>
    /// Требуемый объем продавочной жидкости, м3  
    /// </summary>
    private double Vprodzh1; 

    /// <summary>
    /// Требуемое количество кварцевого песка, кг  
    /// </summary>
    private double Qp1; 

    /// <summary>
    /// Объемная доля песка  
    /// </summary>
    private double Cp1; 

    /// <summary>
    /// Необходимый темп закачки жидкости (расход), м3/c  
    /// </summary>
    private double Qzak1; 

    /// <summary>
    /// Номер скорости подачи насосного агрегата 
    /// </summary>
    private int speedNumber1;
    
    /// <summary>
    /// Требуемое количество насосных агрегатов, шт  
    /// </summary>
    private int Nagr1; 

    /// <summary>
    /// Плотность ПЖС или ЖГС, кг/м3  
    /// </summary>
    private double Ropzhszhgs1; 

    /// <summary>
    /// Плотность ПЖ или ЖГС, кг/м3  
    /// </summary>
    private double Ropzhzhgs1; 

    /// <summary>
    /// Вязкость ПЖС или ЖГС, мПа•с  
    /// </summary>
    private double Mjupzhszhgs1; 

    /// <summary>
    /// Давление на устье, Па
    /// </summary>
    private double Pu1; 

    /// <summary>
    /// Давление на манифольде, Па
    /// </summary>
    private double Pm1; 

    /// <summary>
    /// Давление на затрубе, Па
    /// </summary>
    private double Pzatr1; 

    /// <summary>
    /// Давление на затрубе перед запуском, когда Рзаб =Рпл, Па
    /// </summary>
    private double Pzatr01; 

    /// <summary>
    /// Давление на забое скважины, Па
    /// </summary>
    private double Pz1; 

    /// <summary>
    /// Давление на забое скважины на предыдущем такте, Па
    /// </summary>
    private double Pz_11; 
    
    /// <summary>
    /// Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с  
    /// </summary>
    private double q1; 

    /// <summary>
    /// Вязкость, Па•с
    /// </summary>
    private double viscosity1;

    /// <summary>
    /// Радиус скважины, м
    /// </summary>
    private double rc1 = 76.7E-3;

    /// <summary>
    /// Радиус скважины по долоту, м
    /// </summary>
    private double rcd1;
    
    /// <summary>
    /// Внутренний диаметр НКТ или ЭК, м  
    /// </summary>
    private double dnktek1; 
    
    /// <summary>
    ///  Значение времени на предыдущем такте, секунды
    /// </summary>
    private double time_11; 
    
    /// <summary>
    ///  Время, секунды
    /// </summary>
    private double time1; 
    
    /// <summary>
    ///  Время остановки скважины на исследование, секунды
    /// </summary>
    private double t01 = 1.0; 
    
    /// <summary>
    ///  Время работы скважины на установившемся режиме, секунды
    /// </summary>
    private double tp1; 
    
    /// <summary>
    ///  Продолжительность исследования, секунды
    /// </summary>
    private double dt1; 
    
    /// <summary>
    ///  Время Хорнера, секунды
    /// </summary>
    private double dte1; 
    
    /// <summary>
    ///  Время первого замера, секунды
    /// </summary>
    private double tz11; 
    
    /// <summary>
    ///  Время второго замера, секунды
    /// </summary>
    private double tz21; 
    
    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости (жидкости глушения/песчано-жидкостной смеси)
    /// при течении по трубам лифтовой колонны (НКТ), Па
    /// </summary>
    private double DPpzhn1; 

    /// <summary>
    /// Число Рейнольдса
    /// </summary>
    private double Re1; 

    /// <summary>
    /// Число Рейнольдса для труб (НКТ или ЭК)
    /// </summary>
    private double Retr1; 

    /// <summary>
    /// Число Рейнольдса для кольцевого пространства (между внешним диаметром НКТ и внутренним диаметром ЭК)
    /// </summary>
    private double Rekp1; 

    /// <summary>
    /// Число Рейнольдса в ЭК
    /// </summary>
    private double Reek1; 

    /// <summary>
    /// Число Рейнольдса в НКТ
    /// </summary>
    private double Renkt1; 

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м
    /// </summary>
    private double Xnkt1; 

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК, м
    /// от подачи насоса (q) во времени
    /// </summary>
    private double Xek1; 

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в КП от подачи насоса (q) во времени, м
    /// </summary>
    private double Xkp1;

    /// <summary>
    /// Коэффициент потерь на трение по длине в НКТ
    /// </summary>
    private double LambdaNkt1;

    /// <summary>
    /// Коэффициент потерь на трение по длине в КП
    /// </summary>
    private double LambdaKp1;

    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК
    /// </summary>
    private double LambdaEk1;
    
    /// <summary>
    /// Потери давления на трение в кольцевом пространстве ΔРКП для ПЖС/промывочной жидкости при течении по кольцевому пространству, Па
    /// </summary>
    private double DPpzhk1; 

    /// <summary>
    /// Потери давления на трение в ЭК ΔРЭК для ПЖС/ЖГС при течении по ЭК, Па
    /// </summary>
    private double DPpzhe1; 

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки, Па
    /// </summary>
    private double Pao11; 

    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ, Па
    /// </summary>
    private double Pzo11; 

    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе (заполнение жидкостью разрыва НКТ от устья до башмака НКТ), Па
    /// </summary>
    private double Pap11; 

    /// <summary>
    /// Давление на насосном агрегате при осуществлени прямой промывки, Па
    /// </summary>
    private double Pap21; 

    /// <summary>
    /// Забойное давление в скважине на первом временном этапе (заполнение жидкостью разрыва НКТ от устья до башмака НКТ), Па
    /// </summary>
    private double Pzp11; 

    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖС НКТ от устья до башмака НКТ (до перфоратора), Па
    /// </summary>
    private double Pzp21; 

    /// <summary>
    /// Давление на насосном агрегате при продавливании  ПЖС через перфоратор на циркуляции, Па
    /// </summary>
    private double Pap31; 

    /// <summary>
    /// Забойное давление в скважине при ГПП (формировании перфорационных каналов), Па
    /// </summary>
    private double Pzp31; 

    /// <summary>
    /// Давление на насосном агрегате при продавливании ПЖС, находящейся в НКТ, продавочной жидкостью, Па
    /// </summary>
    private double Pap41; 

    /// <summary>
    /// Давление на насосном агрегате на пятом временном этапе (заполнение ЖП ЭК от башмака НКТ до зумпфа (дна) скважины, полное продавливание ЖР через отверстия перфорации), Па
    /// </summary>
    private double Pap51; 

    /// <summary>
    /// Давление на насосном агрегате на шестом временном этапе (продавливание ЖП через отверстия перфорации в пласт жидкостью пропантоносителем, требуемый объем ЖП должен быть больше VНКТ+VЭК (под башмаком НКТ)), Па
    /// </summary>
    private double Pap61; 

    /// <summary>
    /// Давление на насосном агрегате на седьмом временном этапе (продавливание ЖП через отверстия перфорации продавочной жидкостью ПЖ (ЖГС)), Па
    /// </summary>
    private double Pap71; 

    /// <summary>
    /// Давление на насосном агрегате на восьмом временном этапе (заполнение ПЖ ЭК от башмака НКТ до зумпфа (дна) скважины, полное продавливание ЖП через отверстия перфорации), Па
    /// </summary>
    private double Pap81; 

    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖ НКТ от устья до башмака НКТ (до перфоратора), Па
    /// </summary>
    private double Pzp41; 

    /// <summary>
    /// Забойное давление в скважине на пятом временном этапе (заполнение ЖП ЭК от башмака НКТ
    /// до зумпфа (дна) скважины, полное продавливание ЖР через отверстия перфорации), Па
    /// </summary>
    private double Pzp51; 

    /// <summary>
    /// Забойное давление в скважине на шестом временном этапе (продавливание ЖП через отверстия перфорации
    /// в пласт жидкостью пропантоносителем, требуемый объем ЖП должен быть больше VНКТ+VЭК (под башмаком НКТ)), Па
    /// </summary>
    private double Pzp61; 

    /// <summary>
    /// Забойное давление в скважине на седьмом временном этапе
    /// (продавливание ЖП через отверстия перфорации продавочной жидкостью ПЖ (ЖГС)), Па
    /// </summary>
    private double Pzp71; 

    /// <summary>
    /// Забойное давление в скважине на восьмом временном этапе (заполнение ПЖ ЭК от башмака НКТ до зумпфа (дна) скважины,
    /// полное продавливание ЖП через отверстия перфорации), Па
    /// </summary>
    private double Pzp81; 

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из кольцевого пространства), Па
    /// </summary>
    private double Pao51; 

    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ, Па
    /// </summary>
    private double Pzo51; 

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из НКТ), Па
    /// </summary>
    private double Pao61; 

    /// <summary>
    /// Забойное давление в скважине при заполненном КП промывочной жидкостью, Па
    /// </summary>
    private double Pzo61; 

    /// <summary>
    /// Накопленный объем закачиваемой в скважину ПЖС/ПЖ, м3
    /// </summary>
    private double Vnak1; 

    /// <summary>
    /// Проницаемость пласта, м2
    /// </summary>
    private double k1 = 1.0E-12; 

    /// <summary>
    /// Пористость, д. ед.
    /// </summary>
    private double m1 = 0.2; 

    /// <summary>
    /// Пьезопроводность, м2/с
    /// </summary>
    private double piezoconductivityOfTheFormation1 = 5.0; 

    /// <summary>
    /// Толщина пласта, м
    /// </summary>
    private double h1 = 10.0; 

    /// <summary>
    /// Скин-фактор
    /// </summary>
    private double skinFactor1 = 0.0; 

    /// <summary>
    /// Радиус контура питания, м
    /// </summary>
    private double Rk1 = 250.0; 

    /// <summary>
    /// Предельная депрессия разрушения пласта, Па
    /// </summary>
    private double deltaPpred1 = 25.0E6; 

    /// <summary>
    /// Коэффициент угла наклона для расчета степени разрушения коллектора до ∆Pпред, кг/(м3•Па)
    /// </summary>
    private double A11 = 3.0E-3; 

    /// <summary>
    /// Коэффициент угла наклона для расчета степени разрушения коллектора после ∆Pпред, кг/(м3•Па)
    /// </summary>
    private double A21 = 3.0E-3; 

    /// <summary>
    /// Глубина залегания пласта (длина скважины), м
    /// </summary>
    private double Lskv1 = 2000.0; 

    /// <summary>
    /// Плотность нефти, кг/м3
    /// </summary>
    private double densityOfOil1 = 850.0; 

    /// <summary>
    /// Вязкость нефти, Па•с
    /// </summary>
    private double oilViscosity1 = 5.0E-3; 

    /// <summary>
    /// Газовый фактор, м3/м3
    /// </summary>
    private double gasFactor1 = 100.0; 

    /// <summary>
    /// Давление насыщения нефти газом, Па 
    /// </summary>
    private double Pnas01 = 8.0E6; 

    /// <summary>
    /// Давление насыщения нефти газом при T[j], Па 
    /// </summary>
    private double[] Pnas1; 

    /// <summary>
    /// Объемный коэффициент нефти
    /// </summary>
    private double bOil1 = 1.2; 

    /// <summary>
    /// Вязкость воды, Па•с
    /// </summary>
    private double WaterViscosity1 = 1.0E-3; 

    /// <summary>
    /// Обводненность продукции при ∆P=0, д.ед.
    /// </summary>
    private double productWaterCut01 = 0.0; 

    /// <summary>
    /// Обводненность продукции при ∆P=∆Pmax, д.ед.
    /// </summary>
    private double productWaterCutMax1 = 0.0; 

    /// <summary>
    /// Плотность газа, кг/м3
    /// </summary>
    private double densityOfGas1 = 1.5; 

    /// <summary>
    /// Коэффициент сжимаемости газа
    /// </summary>
    private double Z1 = 1.2; 

    /// <summary>
    /// Молярная доля азота в составе газа, д.ед.
    /// </summary>
    private double Na1 = 0.98; 

    /// <summary>
    /// Молярная доля метана в составе газа, д.ед.
    /// </summary>
    private double Nc1 = 0.02; 

    /// <summary>
    /// Дебит скважины, м3/с
    /// </summary>
    private double Q1; 

    /// <summary>
    /// Дебит скважины во время первого замера, м3/с
    /// </summary>
    private double Qz11; 

    /// <summary>
    /// Дебит скважины во время первого замера, м3/с
    /// </summary>
    private double Qz21; 

    /// <summary>
    /// Коэффициент продуктивности скважины, м3/(с*Па)
    /// </summary>
    private double Kp1; 

    /// <summary>
    /// Удельный коэффициент продуктивности скважины, м2/(с* Па)
    /// </summary>
    private double Kpud1; 

    /// <summary>
    /// Решение уравнения упругого режима (пьезопроводности), Па
    /// </summary>
    private double Pqv1; 

    /// <summary>
    /// Депрессия на пласт, Па
    /// </summary>
    private double DP1; 

    /// <summary>
    /// Депрессия на пласт во время первого замера, Па
    /// </summary>
    private double DPz11; 

    /// <summary>
    /// Депрессия на пласт во время второго замера, Па
    /// </summary>
    private double DPz21; 

    /// <summary>
    /// Максимальная депрессия на пласт, Па
    /// </summary>
    private double DPmax1; 

    /// <summary>
    /// Смещение зависимости депрессии на пласт от логарифма времени по оси ординат, Па
    /// </summary>
    private double Adp1; 

    /// <summary>
    /// Тангенс угла наклона зависимости депрессии на пласт от логарифма времени
    /// </summary>
    private double idp1; 

    /// <summary>
    /// Гидропроводность, м3/(с*Па)
    /// </summary>
    private double eps1; 

    /// <summary>
    /// Приведенный радиус скважины, м
    /// </summary>
    private double rpr1; 

    /// <summary>
    /// Коэффициент совершенства скважины
    /// </summary>
    private double Delc1; 

    /// <summary>
    /// Период стабилизации, с
    /// </summary>
    private double Tstab1; 

    /// <summary>
    /// Коэффициент в формуле вычисления периода стабилизации (диапазон коэффициента: 0.12 - 0.15 включительно)
    /// </summary>
    private double Kstab1; 

    /// <summary>
    /// Коэффициент подвижности пласта, м2/(с*Па)
    /// </summary>
    private double Kpodv1; 

    /// <summary>
    /// Забойное давление в скважине в момент остановки скважины на исследование, Па
    /// </summary>
    private double Pzost1; 

    /// <summary>
    /// Забойное давление после 1 ч с момента остановки скважины на исследование, Па
    /// </summary>
    private double Pzost1h1; 

    /// <summary>
    /// Забойное давление, рассчитанное при помощи распределения давления, Па
    /// </summary>
    private double Pzpk1; 

    /// <summary>
    /// Угловой коэффициент
    /// </summary>
    private double mln1; 

    /// <summary>
    /// Упругоемкость (сжимаемость) пласта
    /// </summary>
    private double Bpl1; 

    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле, Па
    /// </summary>
    private double Pbuf11; 

    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле, Па
    /// </summary>
    private double Pbuf21; 

    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам, Па
    /// </summary>
    private double Pbufm1; 

    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле, Па
    /// </summary>
    private double Pbuf31; 

    /// <summary>
    /// Расход газа, м3/с
    /// </summary>
    private double Qg1; 

    /// <summary>
    /// Линейное давление, Па
    /// </summary>
    private double Plin1; 

    /// <summary>
    /// Диаметр штуцера на предыдущем такте, м
    /// </summary>
    private double dsht_11; 

    /// <summary>
    /// Диаметр штуцера, м
    /// </summary>
    private double dsht1 = 0.018; 

    /// <summary>
    /// Удельный расход выделившегося газа на устье, м3/ м3
    /// </summary>
    private double Vgvust1; 

    /// <summary>
    /// Температура на устье, К
    /// </summary>
    private double Tu1; 

    /// <summary>
    /// Температура на буфере, К
    /// </summary>
    private double Tbuf1; 

    /// <summary>
    /// Давление на приеме в НКТ, Па
    /// </summary>
    private double Ppr1; 

    /// <summary>
    /// Глубина кровли пласта, м
    /// </summary>
    private double Lk1; 

    /// <summary>
    /// Величина динамического уровня в скважине, м
    /// </summary>
    private double hdin1; 

    /// <summary>
    /// Величина динамического уровня перед остановкой скважины, м
    /// </summary>
    private double hdin01; 

    /// <summary>
    /// Статический уровень в скважине, м
    /// </summary>
    private double hst1; 

    /// <summary>
    /// Количество свободного газа, д. ед.
    /// </summary>
    private double ng1; 

    /// <summary>
    /// Количество свободного газа, поступившего в насос, д. ед.
    /// </summary>
    private double ngn1; 

    /// <summary>
    /// Обводненность продукции, д. ед.
    /// </summary>
    private double nv1; 
    
    /// <summary>
    /// Показатель степени в формуле (2.35), д. ед.
    /// </summary>
    private double nst1; 

    /// <summary>
    /// Содержание механических примесей, кг/м3
    /// </summary>
    private double MP1; 

    /// <summary>
    /// Температура пластовая, К
    /// </summary>
    private double Tpl1;

    /// <summary>
    /// Геотермический градиент, К/м
    /// </summary>
    private double Gtgr1 = 0.03; 

    /// <summary>
    /// Плотность жидкости, кг/м3 
    /// </summary>
    private double Rozh1; 

    /// <summary>
    /// Расход жидкости, поступающей из пласта, м3/с 
    /// </summary>
    private double Qskv1; 

    /// <summary>
    /// Производительность насоса, м3/с 
    /// </summary>
    private double Qn1; 

    /// <summary>
    /// Скорость подъема жидкости в ЭК, м/с 
    /// </summary>
    private double Vzhek1; 

    /// <summary>
    /// Скорость подъема жидкости в НКТ, м/с 
    /// </summary>
    private double Vzhnkt1; 

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ в ЭК, м 
    /// </summary>
    private double L11; 

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ, м 
    /// </summary>
    private double Hg11; 

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ в НКТ, м 
    /// </summary>
    private double Hg21; 

    /// <summary>
    /// Верхняя граница глубин для расчета распределения давления газированной жидкости по методике Поэтмана-Карпентера, м 
    /// </summary>
    private double L21; 

    /// <summary>
    /// Давление, с которого начинается расчет распределения давления в НКТ, Па 
    /// </summary>
    private double P1nkt1; 

    /// <summary>
    /// Давление, с которого начинается выделяться газ в ЭК, Па 
    /// </summary>
    private double P2ek1; 

    /// <summary>
    /// Количество интервалов расчета давления газированной жидкости по методике Поэтмана-Карпентера, шт. 
    /// </summary>
    private double Nint1; 

    /// <summary>
    /// Температурный градиент потока, К/м 
    /// </summary>
    private double Tgrp1 = 0.03; 

    /// <summary>
    /// Количество интервалов, на которых осуществляется расчет давления и температуры, м 
    /// </summary>
    private int N1; 

    /// <summary>
    /// Интервал H[j], на котором рассчитывается давление и температура, м 
    /// </summary>
    private double[] H1; 

    /// <summary>
    /// Температура в интервале H[j], К
    /// </summary>
    private double[] T1; 

    /// <summary>
    /// Давление в интервале Н[j], Па
    /// </summary>
    private double[] P1; 

    /// <summary>
    /// Признак, характеризующий то, где производится расчет (1.10) (ЭК (PrEKNKT = 1) или НКТ (PrEKNKT = 0))
    /// </summary>
    private int PrEKNKT1; 

    /// <summary>
    /// Температура на выходе из насоса, К
    /// </summary>
    private double Tvyh1; 

    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа
    /// </summary>
    private double RfP1; 

    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа
    /// </summary>
    private double mfT1; 

    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа
    /// </summary>
    private double DfT1; 

    /// <summary>
    /// Удельный объем выделившегося газа при H[j], м3/м3
    /// </summary>
    private double[] Vgv1; 

    /// <summary>
    /// Удельный объем смеси, м3/м3
    /// </summary>
    private double Vcm1; 

    /// <summary>
    /// Давление при НУ = 101300, Па
    /// </summary>
    private double Pnu1; 

    /// <summary>
    /// Температура при НУ = 273, K
    /// </summary>
    private double Tnu1; 

    /// <summary>
    /// Удельная масса смеси, кг/м3
    /// </summary>
    private double Mcm1; 

    /// <summary>
    /// Плотность газожидкостной смеси, кг/м3
    /// </summary>
    private double Rocm1; 

    /// <summary>
    /// Корреляционный коэффициент
    /// </summary>
    private double f1; 

    /// <summary>
    /// Дифференциал (1.21), соответствующий интервалу H[j], на котором рассчитывается давление и температура, Па/м
    /// </summary>
    private double[] dPdH1; 

    /// <summary>
    /// Число, обратное дифференциалу (1.21), м/Па
    /// </summary>
    private double[] dHdP1; 

    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера, м
    /// </summary>
    private double dl1; 

    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера, Па
    /// </summary>
    private double dР1; 

    /// <summary>
    /// Порядковый номер итерации j
    /// </summary>
    private int pnj1; 

    /// <summary>
    /// Вязкость жидкости, Па•с
    /// </summary>
    private double Mjuzh1; 

    /// <summary>
    /// Давление на выходе из насоса, Па
    /// </summary>
    private double Pvyh1; 

    /// <summary>
    /// Вязкость эмульсии, Па•с
    /// </summary>
    private double MjuE1; 

    /// <summary>
    /// Параметр, учитывающий скорость сдвига
    /// </summary>
    private double A2E1; 

    /// <summary>
    /// Параметр, учитывающий скорость сдвига
    /// </summary>
    private double B2E1; 

    /// <summary>
    /// Объем выделившегося газа на приеме насоса, м3/м3
    /// </summary>
    private double Vgvpr1; 

    /// <summary>
    /// Температура на приеме насоса, К 
    /// </summary>
    private double Tprn1; 

    /// <summary>
    /// Температура на приеме НКТ, К 
    /// </summary>
    private double Tpr1; 

    /// <summary>
    /// Давление на приеме насоса, Па
    /// </summary>
    private double Pprn1; 

    /// <summary>
    /// Приведенная скорость жидкости, м/с
    /// </summary>
    private double Wzhpr1; 

    /// <summary>
    /// Скорость дрейфа газа, м/с
    /// </summary>
    private double Wgdr1; 

    /// <summary>
    /// Коэффициент сепарации газа при переходе откачиваемой продукции из кольцевого пространства скважины во всасывающую камеру насоса
    /// </summary>
    private double Ksvh1; 

    /// <summary>
    /// Коэффициент сепарации за счет работы газосепаратора, если такой установлен
    /// </summary>
    private double Ksgs1; 

    /// <summary>
    /// Коэффициент сепарации
    /// </summary>
    private double Ks1; 

    /// <summary>
    /// Вязкость газожидкостной смеси, Па*с
    /// </summary>
    private double Mjugzh1; 

    /// <summary>
    /// Давление над плунжером насоса, Па
    /// </summary>
    private double Pn1;

    /// <summary>
    /// Давление, развиваемое насосом, Па
    /// </summary>
    private double Prn1;

    /// <summary>
    /// Коэффициент наполнения насоса жидкостью, учитывающий влияние свободного газа 
    /// </summary>
    private double Eta11; 

    /// <summary>
    /// Коэффициент наполнения насоса жидкостью, учитывающий влияние потери хода плунжера
    /// </summary>
    private double Eta21; 

    /// <summary>
    /// Коэффициент усадки жидкости
    /// </summary>
    private double Eta31; 

    /// <summary>
    /// Коэффициент утечек
    /// </summary>
    private double Eta41; 

    /// <summary>
    /// Коэффициент подачи насосной установки 
    /// </summary>
    private double Etap1; 

    /// <summary>
    /// Коэффициент выигрыша хода плунжера
    /// </summary>
    private double Kvpl1; 

    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций штанг и труб, м
    /// </summary>
    private double LMsh1; 

    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций штанг, м
    /// </summary>
    private double LMsht1; 

    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций труб, м
    /// </summary>
    private double LMt1; 

    /// <summary>
    /// Потеря хода плунжера при включении осложнения, м
    /// </summary>
    private double dLM1; 

    /// <summary>
    /// Напор насоса, Н
    /// </summary>
    private double Hn1; 

    /// <summary>
    /// Длина хода плунжера, м
    /// </summary>
    private double S1; 

    /// <summary>
    /// Число качаний (двойных ходов) в секунду, 1/c
    /// </summary>
    private double nk1; 

    /// <summary>
    /// Вес столба жидкости, действующий на плунжер, Н
    /// </summary>
    private double Pzh1; 

    /// <summary>
    /// Площадь сечения штанг, м2
    /// </summary>
    private double fsh1; 

    /// <summary>
    /// Модуль Юнга металла штанг, Па
    /// </summary>
    private double Esh1; 

    /// <summary>
    /// Площадь сечения металла труб, м2
    /// </summary>
    private double ft1; 

    /// <summary>
    /// Модуль Юнга металла труб, Па
    /// </summary>
    private double Et1; 

    /// <summary>
    /// Диаметр штанг, м
    /// </summary>
    private double dsh1; 

    /// <summary>
    /// Площадь сечения плунжера насоса, м2
    /// </summary>
    private double Fpl1; 

    /// <summary>
    /// Разность давлений, действующих на поверхность плунжера, Па
    /// </summary>
    private double DPpl1; 

    /// <summary>
    /// Гидростатическое давление столба жидкости, Па
    /// </summary>
    private double Pgst1; 

    /// <summary>
    /// Напор на преодоление гидравлического трения, м
    /// </summary>
    private double htren1; 

    /// <summary>
    /// Потери давления на трение жидкости в трубах, Па
    /// </summary>
    private double Ptren1; 

    /// <summary>
    /// Напор, соответствующий газлифтному эффекту, м
    /// </summary>
    private double hgl1; 

    /// <summary>
    /// Средний объем выделившегося газа в НКТ, м3/ м3
    /// </summary>
    private double gFactorMdl1; 

    /// <summary>
    /// Давление разгрузки в результате газлифтного эффекта, Па
    /// </summary>
    private double Prgl1; 

    /// <summary>
    /// Объем жидкости, протекающий через зазор между плунжером и цилиндром, м3/с
    /// </summary>
    private double Qut1; 

    /// <summary>
    /// Ширина зазора между плунжером и цилиндром, м
    /// </summary>
    private double Dzaz1; 

    /// <summary>
    /// Длина щели, м
    /// </summary>
    private double Lshl1; 

    /// <summary>
    /// Длина плунжера, м
    /// </summary>
    private double Lpl1; 

    /// <summary>
    /// Дополнительные утечки при включении осложнений, м3/с
    /// </summary>
    private double DQosl1; 

    /// <summary>
    /// Диаметр цилиндра, м
    /// </summary>
    private double Dc1; 

    /// <summary>
    /// Диаметр плунжера насоса, м
    /// </summary>
    private double dpl1; 

    /// <summary>
    /// Вес колонны штанг в воздухе, Н
    /// </summary>
    private double Pshtv1; 

    /// <summary>
    /// Вес колонны штанг в жидкости, Н
    /// </summary>
    private double Pshtzh1; 

    /// <summary>
    /// Вес 1 м штанг в воздухе, Н/м
    /// </summary>
    private double qsht1; 

    /// <summary>
    /// Коэффициент плавучести штанг
    /// </summary>
    private double Karh1; 

    /// <summary>
    /// Плотность материала штанг, кг/м3
    /// </summary>
    private double Rosht1; 

    /// <summary>
    /// Вспомогательный коэффициент
    /// </summary>
    private double mw1; 

    /// <summary>
    /// Вспомогательный коэффициент
    /// </summary>
    private double Psi1; 

    /// <summary>
    /// Угловая скорость вращения вала кривошипа, рад/с
    /// </summary>
    private double wkr1; 

    /// <summary>
    /// Вибрационная нагрузка при ходе вверх, Н
    /// </summary>
    private double Pvibv1; 

    /// <summary>
    /// Вибрационная нагрузка при ходе вниз, Н
    /// </summary>
    private double Pvibn1; 

    /// <summary>
    /// Инерционная нагрузка при ходе вверх, Н
    /// </summary>
    private double Pinv1; 

    /// <summary>
    /// Инерционная нагрузка при ходе вниз, Н
    /// </summary>
    private double Pinn1; 

    /// <summary>
    /// Динамическая нагрузка при ходе вверх, Н
    /// </summary>
    private double Pdinv1; 

    /// <summary>
    /// Динамическая нагрузка при ходе вниз, Н
    /// </summary>
    private double Pdinn1; 

    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    private double aln11; 

    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    private double aln21; 

    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    private double an11; 

    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    private double an21; 

    /// <summary>
    /// Коэффициент для расчета динамической нагрузки при ходе вверх
    /// </summary>
    private double Kdv1; 

    /// <summary>
    /// Коэффициент для расчета динамической нагрузки при ходе вниз
    /// </summary>
    private double Kdn1; 

    /// <summary>
    /// Сила механического трения колонны штанг о стенки НКТ, Н
    /// </summary>
    private double Ptrmeh1; 

    /// <summary>
    /// Коэффициент трения штанг о трубы
    /// </summary>
    private double Csht1 = 0.2; 

    /// <summary>
    /// Сила трения плунжера о стенки цилиндра, Н
    /// </summary>
    private double Ptrpl1; 

    /// <summary>
    /// Сила гидравлического сопротивления от перепада давления в нагнетательном клапане насоса, Н
    /// </summary>
    private double Pkln1; 

    /// <summary>
    /// Потери давления в нагнетательном клапане, Па
    /// </summary>
    private double dpkln1; 

    /// <summary>
    /// Максимальная скорость движения газожидкостной смеси в отверстии седла нагнетательного клапана, м/с
    /// </summary>
    private double Vmaxn1; 

    /// <summary>
    /// Коэффициент расхода нагнетательного клапана
    /// </summary>
    private double Ksikln1 = 0.4; 

    /// <summary>
    /// Диаметр отверстия в седле нагнетательного клапана, м
    /// </summary>
    private double dkln1; 

    /// <summary>
    /// Максимальная нагрузка в точке подвеса штанг, Н
    /// </summary>
    private double Pmaxsht1; 

    /// <summary>
    /// Минимальная  нагрузка в точке подвеса штанг, Н
    /// </summary>
    private double Pminsht1; 

    /// <summary>
    /// Максимальное напряжение цикла, Па
    /// </summary>
    private double SGmax1; 

    /// <summary>
    /// Минимальное  напряжение цикла, Па
    /// </summary>
    private double SGmin1; 

    /// <summary>
    /// Амплитудное напряжение цикла, Па
    /// </summary>
    private double SGa1; 

    /// <summary>
    /// Приведенное  напряжение цикла, Па
    /// </summary>
    private double SGpr1; 

    /// <summary>
    /// Допустимое значение приведенного напряжения цикла, Па
    /// </summary>
    private double SGprdop1; 

    /// <summary>
    /// Полезная мощность электродвигателя, Вт
    /// </summary>
    private double Jpol1; 

    /// <summary>
    /// Потери мощности от утечек жидкости, Вт
    /// </summary>
    private double Etaut1; 

    /// <summary>
    /// Потери мощности в клапанных узлах, Вт
    /// </summary>
    private double Jkl1; 

    /// <summary>
    /// Потери давления во всасывающем клапане, Па
    /// </summary>
    private double dpklvs1; 

    /// <summary>
    /// Максимальная скорость движения газожидкостной смеси в отверстии седла всасывающего клапана, м/с
    /// </summary>
    private double Vmaxvs1; 

    /// <summary>
    /// Коэффициент расхода всасывающего клапана
    /// </summary>
    private double Ksiklvs1 = 0.4; 

    /// <summary>
    /// Диаметр отверстия в седле всасывающего клапана, м
    /// </summary>
    private double dklvs1; 

    /// <summary>
    /// Потери мощности на преодоление механического трения штанг в трубах, Вт
    /// </summary>
    private double Jtrm1; 

    /// <summary>
    /// Потери мощности на преодоление гидравлического трения штанг в трубах, Вт
    /// </summary>
    private double Jtrg1; 

    /// <summary>
    /// Кинематическая вязкость газожидкостной смеси, м2/с
    /// </summary>
    private double vgzh1; 

    /// <summary>
    /// Коэффициент для расчета потери мощности на преодоление гидравлического трения штанг в трубах
    /// </summary>
    private double Msht1; 

    /// <summary>
    /// Коэффициент для расчета потери мощности на преодоление гидравлического трения штанг в трубах
    /// </summary>
    private double Msht21; 

    /// <summary>
    /// Потери мощности на преодоление трения плунжера в цилиндре, Вт
    /// </summary>
    private double Jtrpl1; 

    /// <summary>
    /// Потери мощности подземной части, Вт
    /// </summary>
    private double Jpch1; 

    /// <summary>
    /// К.п.д. подземной части
    /// </summary>
    private double Etapch1; 

    /// <summary>
    /// К.п.д. ШСНУ
    /// </summary>
    private double Etashsnu1; 

    /// <summary>
    /// К.п.д. станка-качалки
    /// </summary>
    private double Etask1; 

    /// <summary>
    /// К.п.д. электродвигателя
    /// </summary>
    private double Etaed1; 

    /// <summary>
    /// Полная мощность, затрачиваемая на подъем жидкости, Вт
    /// </summary>
    private double Jpoln1; 

    /// <summary>
    /// Сила тока, А
    /// </summary>
    private double I1; 

    /// <summary>
    /// Номинальная сила тока, А
    /// </summary>
    private double In1; 

    /// <summary>
    /// Напряжение, В
    /// </summary>
    private double U1; 

    /// <summary>
    /// Длительность одного цикла качаний, с
    /// </summary>
    private double Per1; 

    /// <summary>
    /// Время упругого растяжения колонны штанг, с
    /// </summary>
    private double tupr1; 

    /// <summary>
    /// Теоретическое перемещение штока, м
    /// </summary>
    private double Lteor1; 

    /// <summary>
    /// Теоретическая нагрузка, Н
    /// </summary>
    private double Pteor1; 

    /// <summary>
    /// Практическая нагрузка, Н
    /// </summary>
    private double Pprak1; 

    /// <summary>
    /// Максимальная нагрузка, Н
    /// </summary>
    private double Pmax1; 

    /// <summary>
    /// Минимальная нагрузка, Н
    /// </summary>
    private double Pmin1; 

    /// <summary>
    /// Динамические нагрузки при ходе вверх, Н
    /// </summary>
    private double Pvd1; 

    /// <summary>
    /// Динамические нагрузки при ходе вниз, Н
    /// </summary>
    private double Pnd1; 

    /// <summary>
    /// Практическое перемещение штока, м
    /// </summary>
    private double Lprak1; 

    /// <summary>
    /// Время упругого растяжения колонны штанг для практической динамограммы при ходе вверх, с
    /// </summary>
    private double tuprvv1; 

    /// <summary>
    /// Время упругого растяжения колонны штанг для практической динамограммы при ходе вниз, с
    /// </summary>
    private double tuprvn1; 

    /// <summary>
    /// Добавка при ходе вверх, с
    /// </summary>
    private double dtvv1; 

    /// <summary>
    /// Добавка при ходе вниз, с
    /// </summary>
    private double dtvn1; 

    /// <summary>
    /// Коэффициент затухания
    /// </summary>
    private double Kzat1; 

    /// <summary>
    /// Период колебаний
    /// </summary>
    private double Tkol1; 

    /// <summary>
    /// Величина изменения динамического уровня в скважине, м
    /// </summary>
    private double dhdin1; 

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента, Па
    /// </summary>
    private double DPpzo11; 

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления, с
    /// </summary>
    private double tzab11; 

    /// <summary>
    /// Дебит скважины перед изменением режима работы, м3/с
    /// </summary>
    private double Qrezh1; 

    /// <summary>
    /// Уровень жидкости в НКТ, м
    /// </summary>
    private double htr1; 

    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта, м3
    /// </summary>
    private double Vzhgzab1; 

    /// <summary>
    /// Объем жидкости глушения скважины, находящейся в пласте, м3
    /// </summary>
    private double Vzhgpl1; 

    /// <summary>
    /// Объем жидкости глушения скважины, попавшей в пласт в результате промывки, м3
    /// </summary>
    private double Vprom1; 

    /// <summary>
    /// Общий напор, который необходимо создать ШСНУ, м
    /// </summary>
    private double Hobsh1; 

    /// <summary>
    /// Общий напор, который необходимо создать УЭЦН, м
    /// </summary>
    private double Hobshu1; 

    /// <summary>
    /// Объем жидкости глушения, поступившей на поверхность, м3
    /// </summary>
    private double Vzhgpov1; 

    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта при остановке скважины, м3
    /// </summary>
    private double Vost1;

    /// <summary>
    /// Критическая депрессия, при которой вода начнет поступать в скважину, Па 
    /// </summary>
    private double DPkr1 = 6.0E6;

    /// <summary>
    /// Коэффициент, характеризующий дебит поступающей жидкости из пласта в скважину, м3/с 
    /// </summary>
    private double Bzh1 = 5.0 * 1.157E-5;

    /// <summary>
    /// Коэффициент макрошероховатости пласта, м 
    /// </summary>
    private double Kmshpl1 = 1.0E-9;

    /// <summary>
    /// Коэффициент фильтрационного сопротивления 
    /// </summary>
    private double Afs1;

    /// <summary>
    /// Коэффициент фильтрационного сопротивления 
    /// </summary>
    private double Bfs1;

    /// <summary>
    /// Коэффициент совершенства скважины по степени вскрытия пласта, д. ед.
    /// </summary>
    private double C1ss1;

    /// <summary>
    /// Коэффициент совершенства скважины по характеру вскрытия пласта 
    /// </summary>
    private double C2ss1;

    /// <summary>
    /// Коэффициент совершенства скважины по степени вскрытия пласта, 1/м
    /// </summary>
    private double C3ss1;

    /// <summary>
    /// Коэффициент совершенства скважины по характеру вскрытия пласта, 1/м
    /// </summary>
    private double C4ss1;

    /// <summary>
    /// Значение стандартного давления, Па
    /// </summary>
    private double Pst1 = 101325.0;

    /// <summary>
    /// Значение стандартной температуры, К
    /// </summary>
    private double Tst1 = 293.0;

    /// <summary>
    /// Плотность газа при стандартных условиях, кг/м3
    /// </summary>
    private double Rogst1;

    /// <summary>
    /// Вязкость газа, Па*с
    /// </summary>
    private double Mjug1 = 0.015;

    /// <summary>
    /// Коэффициент, зависящий от ФЕС пласта
    /// </summary>
    private double AlfaFes1;

    /// <summary>
    /// Коэффициент, зависящий от ФЕС пласта
    /// </summary>
    private double BettaFes1;

    /// <summary>
    /// Коэффициент квадратичного фильтрационного сопротивления, Па2/(м3/с)2
    /// </summary>
    private double bkv1;

    /// <summary>
    /// Коэффициент сверхсжимаемости газа в пластовых условиях, д. ед.
    /// </summary>
    private double Zpl1;

    /// <summary>
    /// Забойное давление перед изменением режима работы, Па
    /// </summary>
    private double Pzrezh1;

    /// <summary>
    /// Атмосферное давление, Па
    /// </summary>
    private double Patm1 = 101300.0;

    /// <summary>
    /// Дебит скважины по воде, м3/с
    /// </summary>
    private double Qzh1; 

    /// <summary>
    /// Объемный расход газа при Р=Р0 и Т=Т0, м3/с
    /// </summary>
    private double Qg01; 

    /// <summary>
    /// Объемный расход газа при Р и Т, м3/с
    /// </summary>
    private double Qgpt1; 

    /// <summary>
    /// Температура газа после штуцера, К
    /// </summary>
    private double Tsep1; 

    /// <summary>
    /// Потери давления в сепараторе, Па
    /// </summary>
    private double DPsep1 = 3.0E4;

    /// <summary>
    /// Давление на ДИКТе, рассчитанное по первой формуле для ДИКТ, Па
    /// </summary>
    private double Pdikt11;

    /// <summary>
    /// Давление на ДИКТе, рассчитанное по второй формуле для ДИКТ, Па
    /// </summary>
    private double Pdikt21;

    /// <summary>
    /// Усредненное значение давления на ДИКТе, Па
    /// </summary>
    private double PdiktMdl1;

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона в сепараторе, K/Па
    /// </summary>
    private double Dbuf1;

    /// <summary>
    /// Температура на ДИКТе, К
    /// </summary>
    private double Tdikt1; 

    /// <summary>
    /// Относительная плотность газа, кг/м3
    /// </summary>
    private double Rootn1; 

    /// <summary>
    /// Признак выбора ДИКТ: 0 - ДИКТ 50 мм, 1 - ДИКТ 100 мм
    /// </summary>
    private double PrDikt1; 

    /// <summary>
    /// Коэффициент
    /// </summary>
    private double Cdikt1; 

    /// <summary>
    /// Коэффициент быстроходности ступени
    /// </summary>
    private double ns1; 

    /// <summary>
    /// Частота вращения вала насоса, 1/c
    /// </summary>
    private double nbp1; 

    /// <summary>
    /// Подача насоса при работе на воде в оптимальном режиме, м3/с
    /// </summary>
    private double Qopt1; 

    /// <summary>
    /// Напор насоса при работе на воде в оптимальном режиме, м
    /// </summary>
    private double Hopt1; 

    /// <summary>
    /// Число ступеней в насосе, шт.
    /// </summary>
    private double Zst1; 

    /// <summary>
    /// Подача насоса при работе на воде в соответственном режиме, м3/с
    /// </summary>
    private double Qi1; 

    /// <summary>
    /// Напор насоса при работе на воде в соответственном режиме, м
    /// </summary>
    private double Hi1; 

    /// <summary>
    /// Коэффициент зависимости напора и подачи от вязкости
    /// </summary>
    private double Khq1; 

    /// <summary>
    /// Коэффициент зависимости КПД от вязкости 
    /// </summary>
    private double Kk1; 

    /// <summary>
    /// Теплоемкость нефти, Дж/(кг*К) 
    /// </summary>
    private double Cn1; 

    /// <summary>
    /// КПД насоса, д. е.
    /// </summary>
    private double EtaN1; 

    /// <summary>
    /// КПД двигателя, д. е.
    /// </summary>
    private double EtaD1; 

    /// <summary>
    /// Теплоемкость воды, Дж/(кг*К) 
    /// </summary>
    private double Cv1 = 4380.0; 

    /// <summary>
    /// Теплоемкость смеси, Дж/(кг*К) 
    /// </summary>
    private double Ccm1; 

    /// <summary>
    /// Мощность, потребляемая насосом, Вт 
    /// </summary>
    private double Nh1; 

    /// <summary>
    /// Сумма потерь мощности в ПЭД, Вт 
    /// </summary>
    private double SNDpot1; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double b21; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double c21; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double d21; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double b31; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double c31; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double b41; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double c41; 

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    private double b51; 

    /// <summary>
    /// ???, Вт 
    /// </summary>
    private double Ng1; 

    /// <summary>
    /// Температура перегрева ПЭД, К
    /// </summary>
    private double tdp1; 

    /// <summary>
    /// Коэффициент, учитывающий изменение температуры перегрева от количества воды и свободного газа
    /// </summary>
    private double Kt1; 

    /// <summary>
    /// Коэффициент уменьшения потерь в ПЭД
    /// </summary>
    private double Kup1; 

    /// <summary>
    /// Температура охлаждающей жидкости, K
    /// </summary>
    private double Tohl1; 

    /// <summary>
    /// Сумма потерь мощности в ПЭД при реальной температуре, Вт 
    /// </summary>
    private double SN1;

    /// <summary>
    /// ???, Вт 
    /// </summary>
    private double SNGpot1;

    /// <summary>
    /// Температура двигателя, K
    /// </summary>
    private double Td1;

    /// <summary>
    /// Фактическая скорость газа, м/с
    /// </summary>
    private double Vgf1;

    /// <summary>
    /// Критическая скорость газа, м/с
    /// </summary>
    private double Vgkr1;

    /// <summary>
    /// Температура на забое скважины, К
    /// </summary>
    private double Tz1;

    /// <summary>
    /// Диаметр капелек воды, м
    /// </summary>
    private double dv1;

    /// <summary>
    /// Уровень жидкости в ЭК, м
    /// </summary>
    private double Hzhek1;

    /// <summary>
    /// Уровень жидкости в НКТ, м
    /// </summary>
    private double Hzhnkt1;

    /// <summary>
    /// Массовый расход газа, кг/с
    /// </summary>
    private double G1;

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на уровне пласта, K/Па
    /// </summary>
    private double Dpl1;

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на забое, K/Па
    /// </summary>
    private double Dz1;

    /// <summary>
    /// Разность температур на уровне пласта и на забое, K
    /// </summary>
    private double DTplz1;

    /// <summary>
    /// Теплоемкость газа, Дж/(кг*К) 
    /// </summary>
    private double Cg1 = 2300.0; 

    /// <summary>
    /// Теплоемкость горных пород пласта, Дж/(кг*К) 
    /// </summary>
    private double Cgp1 = 2000.0; 
    
    /// <summary>
    ///  Время с начала работы скважины, секунды
    /// </summary>
    private double tn1;

    /// <summary>
    /// ???, ???
    /// </summary>
    private double fes1;
    
    /// <summary>
    ///  Теплопроводность горных пород, Вт/(м*К)
    /// </summary>
    private double Lgp1 = 5000.0;
    
    /// <summary>
    ///  Температурный градиент, К/м
    /// </summary>
    private double Tgr1;
    
    /// <summary>
    ///  Коэффициент
    /// </summary>
    private double Teta1;
    
    /// <summary>
    ///  Коэффициент
    /// </summary>
    private double KS1;
    
    /// <summary>
    ///  Средняя температура, K
    /// </summary>
    private double Tcp1;
    
    /// <summary>
    ///  Среднее давление, Па
    /// </summary>
    private double Pcp1 = 15.0E6;
    
    /// <summary>
    /// Относительная плотность газа, кг/м3
    /// </summary>
    private double Rogotn1;

    /// <summary>
    /// Коэффициент гидравлического трения
    /// </summary>
    private double Lambda1 = 0.014;

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на спуске НКТ, K/Па
    /// </summary>
    private double Dsp1;

    /// <summary>
    /// Температура на спуске НКТ, K
    /// </summary>
    private double Tsp1;

    /// <summary>
    /// Давление на спуске НКТ, Па
    /// </summary>
    private double Psp1;

    /// <summary>
    /// Давление на уровне жидкости в ЭК, Па
    /// </summary>
    private double Pur11;

    /// <summary>
    /// Давление на уровне жидкости в НКТ, Па
    /// </summary>
    private double Pur21;

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на ДИКТе, K/Па
    /// </summary>
    private double Ddikt1;

    /// <summary>
    /// Коэффициент
    /// </summary>
    private double KA1;

    /// <summary>
    /// Коэффициент
    /// </summary>
    private double KB1;

    /// <summary>
    /// Коэффициент
    /// </summary>
    private double Kb1;

    /// <summary>
    /// Плотность продавочной жидкости, кг/м3
    /// </summary>
    private double Roprodzh1;

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на I скорости, м3/с 
    /// </summary>
    private double An700Q11 = 6.3E-3; 

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на II скорости, м3/с 
    /// </summary>
    private double An700Q21 = 8.5E-3; 

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на III скорости, м3/с 
    /// </summary>
    private double An700Q31 = 12.0E-3; 

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на IV скорости, м3/с 
    /// </summary>
    private double An700Q41 = 15.0E-3; 

    /// <summary>
    /// Диаметр перфорационных отверстий, м
    /// </summary>
    private double dpo1 = 0.011; 

    /// <summary>
    /// Общее число перфорационных отверстий, м
    /// </summary>
    private int npo1 = 200; 

    /// <summary>
    /// Коэффициент расхода, зависящий от характера истечения жидкости, д. ед.
    /// </summary>
    private double Kdelta1 = 0.82; 

    /// <summary>
    /// Приёмистость скважины, м3/с
    /// </summary>
    private double Qc1 = 0.5 * 0.0083; 

    /// <summary>
    /// Коэффициент увеличения проницаемости ПЗП после ГРП, д. ед.
    /// </summary>
    private double Psigrp1 = 5.0; 

    /// <summary>
    /// Коэффициент Пуассона горных пород, д. ед.
    /// </summary>
    private double KPgp1 = 0.3; 

    /// <summary>
    /// Средняя плотность вышележащих пород, кг/м3
    /// </summary>
    private double Rovp1 = 2500.0; 

    /// <summary>
    /// Давление расслоения пород, Па
    /// </summary>
    private double Prp1 = 3.0E6; 

    /// <summary>
    /// Плотность жидкости разрыва, кг/м3
    /// </summary>
    private double Rozhr1 = 1200.0; 

    /// <summary>
    /// Плотность жидкости-пропантоносителя, кг/м3
    /// </summary>
    private double Rozhp1; 

    /// <summary>
    /// Вязкость жидкости разрыва, Па•с
    /// </summary>
    private double Mjuzhr1 = 150.0E-3; 

    /// <summary>
    /// Массовая доля пропанта в жидкости-пропантоносителе, кг/м3
    /// </summary>
    private double C0pr1 = 350.0; 

    /// <summary>
    /// Плотность пропанта, кг/м3
    /// </summary>
    private double Ropr1 = 3650.0; 

    /// <summary>
    /// Требуемое количество пропанта, кг
    /// </summary>
    private double Qpr1 = 20.0E3; 

    /// <summary>
    /// Необходимое превышение забойного давления над давлением разрыва, д. ед.
    /// </summary>
    private double Azr1 = 1.2; 

    /// <summary>
    /// Коэффициент технического состояния агрегата, д. ед.
    /// </summary>
    private double Kts1 = 0.9; 

    /// <summary>
    /// Ширина (раскрытость) трещины на стенке скважины, м
    /// </summary>
    private double Ksi01 = 1.0E-3; 

    /// <summary>
    /// Давление разрыва пласта, Па
    /// </summary>
    private double Prazr1; 

    /// <summary>
    /// Вертикальная составляющая горного давления, Па
    /// </summary>
    private double Pvg1; 

    /// <summary>
    /// Горизонтальная составляющая горного давления, Па
    /// </summary>
    private double Pgg1; 

    /// <summary>
    /// Расчетное давление гидроразрыва на забое скважины, Па
    /// </summary>
    private double Pzabr1; 

    /// <summary>
    /// Потери давления на трение жидкости разрыва в НКТ, Па
    /// </summary>
    private double DPzhrnkt1; 

    /// <summary>
    /// Потери давления на трение жидкости-пропантоносителя в НКТ, Па
    /// </summary>
    private double DPzhpnkt1; 

    /// <summary>
    /// Критическая скорость ЖР/ЖП в НКТ, м/с
    /// </summary>
    private double Wkrnkt1; 

    /// <summary>
    /// Фактическая средняя скорость ЖР/ЖП в НКТ, м/с
    /// </summary>
    private double Wfnkt1; 

    /// <summary>
    /// Плотность ЖР или ЖП, кг/м3
    /// </summary>
    private double Rozhrzhp1;

    /// <summary>
    /// Плотность ЖР, ЖП или ПЖ, кг/м3
    /// </summary>
    private double Rozhrzhppzh1;

    /// <summary>
    /// Потери давления в перфорационных отверстиях при продавливании цементного раствора в призабойную зону пласта, Па
    /// </summary>
    private double DPpo1;
    
    /// <summary>
    /// Объемная доля пропанта
    /// </summary>
    private double Cprop1;

    /// <summary>
    /// Вязкость жидкости-пропантоносителя, Па•с
    /// </summary>
    private double Mjuzhp1;

    /// <summary>
    /// Требуемый объем жидкости разрыва, м3
    /// </summary>
    private double Vzhr1;

    /// <summary>
    /// Требуемый объем жидкости разрыва с пропантом (ЖП), м3
    /// </summary>
    private double Vzhp1;

    /// <summary>
    /// Общий объем закачиваемой жидкости при ГРП, м3
    /// </summary>
    private double Vzhgrp1;

    /// <summary>
    /// Минимальный темп закачки при создании вертикальной трещины, м3/c
    /// </summary>
    private double Qminv1;

    /// <summary>
    /// Рабочее давление агрегата, Па
    /// </summary>
    private double Pp1;

    /// <summary>
    /// Подача агрегата при данном PР, м3/c
    /// </summary>
    private double qp1;

    /// <summary>
    /// Общая продолжительность процесса гидроразрыва, c
    /// </summary>
    private double tgr1;

    /// <summary>
    /// Коэффициент
    /// </summary>
    private double Ans1;

    /// <summary>
    /// Коэффициент
    /// </summary>
    private double Bns1;
    
    /// <summary>
    /// Расход скважины при давлении гидроразрыва, м3/с
    /// </summary>
    private double Qgrp1;
    
    /// <summary>
    /// Буферное давление, рассчитанное при помощи распределения давления, Па
    /// </summary>
    private double Pbufpk1;
    
    /// <summary>
    /// Давление, с которого начинается расчет, Па
    /// </summary>
    private double Pens11;
    
    /// <summary>
    /// Давление, с которого начинается расчет, Па
    /// </summary>
    private double Pens21;
    
    /// <summary>
    /// Объемный коэффициент воды
    /// </summary>
    private double bv1 = 1.0;
    
    /// <summary>
    /// Коэффициент приемистости скважины, м3/( с*Па)
    /// </summary>
    private double Kprm1;
    
    /// <summary>
    /// Удельный коэффициент приемистости скважины, м2/(с* Па)
    /// </summary>
    private double Kprmud1;


    
    
    
    
    
    
        
    /// <summary>
    ///  Плотность жидкости глушения скважины, кг/м3
    /// </summary>
    public double densityOfKillingFluid
    {
        get => densityOfKillingFluid1;
        set => densityOfKillingFluid1 = value;
    }
    
    /// <summary>
    /// Вязкость жидкости глушения, Па•с
    /// </summary>
    public double viscosityOfKillingFluid
    {
        get => viscosityOfKillingFluid1;
        set => viscosityOfKillingFluid1 = value;
    }
    
    /// <summary>
    /// Вязкость жидкости глушения, мПа•с
    /// </summary>
    public double viscosityOfKillingFluidmPas
    {
        get => viscosityOfKillingFluid1 * 1.0E3;
        set => viscosityOfKillingFluid1 = value / 1.0E3;
    }

    /// <summary>
    /// Плотность песчано-жидкостной смеси, кг/м3 
    /// </summary>
    public double RoP
    {
        get => RoP1;
        set => RoP1 = value;
    }
        
    /// <summary>
    ///  Вязкость песчано-жидкостной смеси, Па•с
    /// </summary>
    public double MjuP
    {
        get => MjuP1;
        set => MjuP1 = value;
    }
        
    /// <summary>
    ///  Вязкость песчано-жидкостной смеси, мПа•с
    /// </summary>
    public double MjuPmPas
    {
        get => MjuP1 * 1.0E3;
        set => MjuP1 = value / 1.0E3;
    }
    
    /// <summary>
    /// Давление поглощения ТЖ пластом, Па 
    /// </summary>
    public double Ppgl
    {
        get => Ppgl1;
        set => Ppgl1 = value;
    }    
    
    /// <summary>
    /// Давление поглощения ТЖ пластом, МПа 
    /// </summary>
    public double PpglMPa
    {
        get => Ppgl1 * 1.0E-6;
        set => Ppgl1 = value / 1.0E-6;
    }    

    /// <summary>
    /// Давление ГНВП, Па 
    /// </summary>
    public double Pgnvp
    {
        get => Pgnvp1;
        set => Pgnvp1 = value;
    }

    /// <summary>
    /// Давление ГНВП, МПа 
    /// </summary>
    public double PgnvpMPa
    {
        get => Pgnvp1 * 1.0E-6;
        set => Pgnvp1 = value / 1.0E-6;
    }

    /// <summary>
    /// Давление гидроразрыва пласта, Па 
    /// </summary>
    public double Pgrp
    {
        get => Pgrp1;
        set => Pgrp1 = value;
    }

    /// <summary>
    /// Давление гидроразрыва пласта, МПа 
    /// </summary>
    public double PgrpMPa
    {
        get => Pgrp1 * 1.0E-6;
        set => Pgrp1 = value / 1.0E-6;
    }
     
    /// <summary>
    /// Плотность промывочной жидкости (ЖГС), кг/м3 
    /// </summary>
    public double Ropzh
    {
        get => Ropzh1;
        set => Ropzh1 = value;
    }

    /// <summary>
    /// Ускорение свободного падения, м/с2
    /// </summary>
    public double g
    {
        get => g1;
    }
     
    /// <summary>
    ///  Средний зенитный угол ствола скважины, град.
    /// </summary>
    public double alfa
    {
        get => alfa1;
        set => alfa1 = value;
    }
     
    /// <summary>
    /// Коэффициент запаса, учитывающий объем поглощения жидкости глушения при глушении, д. ед. 
    /// </summary>
    public double Kz
    {
        get => Kz1;
        set => Kz1 = value;
    }
     
    /// <summary>
    /// Коэффициент запаса, учитывающий объем поглощения жидкости глушения при глушении, % 
    /// </summary>
    public double Kzperc
    {
        get => Kz1 * 1.0E2;
        set => Kz1 = value / 1.0E2;
    }
     
    /// <summary>
    /// Давление опрессовки ЭК, Па 
    /// </summary>
    public double ekCrimpingPressure
    {
        get => ekCrimpingPressure1;
        set => ekCrimpingPressure1 = value;
    }
     
    /// <summary>
    /// Давление опрессовки ЭК, МПа 
    /// </summary>
    public double ekCrimpingPressureMPa
    {
        get => ekCrimpingPressure1 * 1.0E-6;
        set => ekCrimpingPressure1 = value / 1.0E-6;
    }
     
    /// <summary>
    /// Абсолютная шероховатость труб, м 
    /// </summary>
    public double K
    {
        get => K1;
        set => K1 = value;
    }
     
    /// <summary>
    /// Абсолютная шероховатость внутренней поверхности ЭК, м 
    /// </summary>
    public double EpsEk
    {
        get => EpsEk1;
        set => EpsEk1 = value;
    }

    /// <summary>
    ///  Абсолютная шероховатость внутренней поверхности НКТ, м
    /// </summary>
    public double EpsNkt
    {
        get => EpsNkt1;
        set => EpsNkt1 = value;
    }
    /// <summary>
    /// Число pi
    /// </summary>
    public double pi
    {
        get => pi1;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на II скорости, м3/с 
    /// </summary>
    public double Ca320Q2
    {
        get => Ca320Q21;
        set => Ca320Q21 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на II скорости, л/с 
    /// </summary>
    public double Ca320Q2ls
    {
        get => Ca320Q21 * 1.0E3;
        set => Ca320Q21 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на III скорости, м3/с 
    /// </summary>
    public double Ca320Q3
    {
        get => Ca320Q31;
        set => Ca320Q31 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на III скорости, л/с 
    /// </summary>
    public double Ca320Q3ls
    {
        get => Ca320Q31 * 1.0E3;
        set => Ca320Q31 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на IV скорости, м3/с 
    /// </summary>
    public double Ca320Q4
    {
        get => Ca320Q41;
        set => Ca320Q41 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на IV скорости, л/с 
    /// </summary>
    public double Ca320Q4ls
    {
        get => Ca320Q41 * 1.0E3;
        set => Ca320Q41 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на V скорости, м3/с 
    /// </summary>
    public double Ca320Q5
    {
        get => Ca320Q51;
        set => Ca320Q51 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата ЦА-320 на V скорости, л/с
    /// </summary>
    public double Ca320Q5ls
    {
        get => Ca320Q51 * 1.0E3;
        set => Ca320Q51 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на I скорости, м3/с 
    /// </summary>
    public double An500Q1
    {
        get => An500Q11;
        set => An500Q11 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на I скорости, л/с 
    /// </summary>
    public double An500Q1ls
    {
        get => An500Q11 * 1.0E3;
        set => An500Q11 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на II скорости, м3/с 
    /// </summary>
    public double An500Q2
    {
        get => An500Q21;
        set => An500Q21 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на II скорости, л/с  
    /// </summary>
    public double An500Q2ls
    {
        get => An500Q21 * 1.0E3;
        set => An500Q21 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на III скорости, м3/с 
    /// </summary>
    public double An500Q3
    {
        get => An500Q31;
        set => An500Q31 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на III скорости, л/с
    /// </summary>
    public double An500Q3ls
    {
        get => An500Q31 * 1.0E3;
        set => An500Q31 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на IV скорости, м3/с 
    /// </summary>
    public double An500Q4
    {
        get => An500Q41;
        set => An500Q41 = value;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на IV скорости, л/с
    /// </summary>
    public double An500Q4ls
    {
        get => An500Q41 * 1.0E3;
        set => An500Q41 = value / 1.0E3;
    }
     
    /// <summary>
    /// Подача насосного агрегата 2АН-500 на V скорости, м3/с 
    /// </summary>
    public double An500Q5
    {
        get => An500Q51;
        set => An500Q51 = value;
    }

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на V скорости, л/с 
    /// </summary>
    public double An500Q5ls
    {
        get => An500Q51 * 1.0E3;
        set => An500Q51 = value / 1.0E3;
    }

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на VI скорости, м3/с 
    /// </summary>
    public double An500Q6
    {
        get => An500Q61;
        set => An500Q61 = value;
    }

    /// <summary>
    /// Подача насосного агрегата 2АН-500 на VI скорости, л/с 
    /// </summary>
    public double An500Q6ls
    {
        get => An500Q61 * 1.0E3;
        set => An500Q61 = value / 1.0E3;
    }
     
    /// <summary>
    ///  Давление в выкидном коллекторе (линии), Па
    /// </summary>
    public double Pkol
    {
        get => Pkol1;
        set => Pkol1 = value;
    }
     
    /// <summary>
    ///  Давление в выкидном коллекторе (линии), МПа
    /// </summary>
    public double PkolMPa
    {
        get => Pkol1 * 1.0E-6;
        set => Pkol1 = value / 1.0E-6;
    }
     
    /// <summary>
    /// Глубина скважины по вертикали, м 
    /// </summary>
    public double Lc
    {
        get => Lc1;
        set => Lc1 = value;
    }
     
    /// <summary>
    /// Длина скважины (по стволу), м 
    /// </summary>
    public double lc
    {
        get => lc1;
        set => lc1 = value;
    }
     
    /// <summary>
    ///  Глубина спуска НКТ в скважину (по вертикали), м
    /// </summary>
    public double Lcp
    {
        get => Lcp1;
        set => Lcp1 = value;
    }
     
    /// <summary>
    /// Внутренний диаметр эксплуатационной колонны, м 
    /// </summary>
    public double ECInnerDiameter
    {
        get => ECInnerDiameter1;
        set => ECInnerDiameter1 = value;
    }
     
    /// <summary>
    /// Внутренний диаметр эксплуатационной колонны, мм 
    /// </summary>
    public double ECInnerDiametermm
    {
        get => ECInnerDiameter1 * 1.0E3;
        set => ECInnerDiameter1 = value / 1.0E3;
    }
     
    /// <summary>
    /// Внешний диаметр эксплуатационной колонны, м 
    /// </summary>
    public double ECOutDiameter
    {
        get => ECOutDiameter1;
        set => ECOutDiameter1 = value;
    }
     
    /// <summary>
    /// Внешний диаметр эксплуатационной колонны, мм 
    /// </summary>
    public double ECOutDiametermm
    {
        get => ECOutDiameter1 * 1.0E3;
        set => ECOutDiameter1 = value / 1.0E3;
    }
     
    /// <summary>
    /// Внешний диаметр НКТ, м 
    /// </summary>
    public double Dnkt
    {
        get => Dnkt1;
        set => Dnkt1 = value;
    }
     
    /// <summary>
    /// Внешний диаметр НКТ, мм 
    /// </summary>
    public double Dnktmm
    {
        get => Dnkt1 * 1.0E3;
        set => Dnkt1 = value / 1.0E3;
    }
     
    /// <summary>
    /// Внутренний диаметр НКТ, м 
    /// </summary>
    public double dnkt
    {
        get => dnkt1;
        set => dnkt1 = value;
    }
     
    /// <summary>
    /// Внутренний диаметр НКТ, мм 
    /// </summary>
    public double dnktmm
    {
        get => dnkt1 * 1.0E3;
        set => dnkt1 = value / 1.0E3;
    }

    /// <summary>
    /// Внутренний диаметр, м 
    /// </summary>
    public double InnerDiam
    {
        get => InnerDiam1;
        set => InnerDiam1 = value;
    } 
    
    /// <summary>
    /// Пластовое давление, Па 
    /// </summary>
    public double reservoirPressurePa
    {
        get => reservoirPressurePa1;
        set => reservoirPressurePa1 = value;
    }
     
    /// <summary>
    /// Пластовое давление, МПа 
    /// </summary>
    public double reservoirPressureMPa
    {
        get => reservoirPressurePa1 * 1.0E-6;
        set => reservoirPressurePa1 = value / 1.0E-6;
    }
     
    /// <summary>
    /// Степень открытия задвижки, % (100% - задвижка полностью открыта) 
    /// </summary>
    public double Fi
    {
        get => Fi1;
        set => Fi1 = value;
    }
     
    /// <summary>
    /// Внутренний диаметр отвода фонтанной арматуры, м 
    /// </summary>
    public double dotv
    {
        get => dotv1;
        set => dotv1 = value;
    }
     
    /// <summary>
    /// Внутренний диаметр отвода фонтанной арматуры, мм 
    /// </summary>
    public double dotvmm
    {
        get => dotv1 * 1.0E3;
        set => dotv1 = value / 1.0E3;
    }
     
    /// <summary>
    /// Число насадок в перфораторе, шт 
    /// </summary>
    public double Nperf
    {
        get => Nperf1;
        set => Nperf1 = value;
    }
     
    /// <summary>
    /// Внутренний диаметр насадок перфоратора, м 
    /// </summary>
    public double Dperf
    {
        get => Dperf1;
        set => Dperf1 = value;
    }
     
    /// <summary>
    /// Плотность песчано-жидкостной смеси, кг/м3 
    /// </summary>
    public double RoPzhs
    {
        get => RoPzhs1;
        set => RoPzhs1 = value;
    }
     
    /// <summary>
    /// Плотность жидкости затворения (воды), кг/м3 
    /// </summary>
    public double densityOfWater
    {
        get => densityOfWater1;
        set => densityOfWater1 = value;
    }
     
    /// <summary>
    /// Плотность песка, кг/м3 
    /// </summary>
    public double densityOfSand
    {
        get => densityOfSand1;
        set => densityOfSand1 = value;
    }
     
    /// <summary>
    /// Массовая доля песка в песчано-жидкостной смеси, кг/м3 
    /// </summary>
    public double Co
    {
        get => Co1;
        set => Co1 = value;
    }
     
    /// <summary>
    /// Коэффициент расхода  
    /// </summary>
    public double z
    {
        get => z1;
        set => z1 = value;
    }
     
    /// <summary>
    /// Потери давления в насадках, Па
    /// </summary>
    public double DPn
    {
        get => DPn1;
        set => DPn1 = value;
    }
     
    /// <summary>
    /// Потери давления в насадках, МПа
    /// </summary>
    public double DPnMPa
    {
        get => DPn1 * 1.0E-6;
        set => DPn1 = value / 1.0E-6;
    }
     
    /// <summary>
    /// Потери давления в полости, образованной абразивной струей, Па 
    /// </summary>
    public double DPp
    {
        get => DPp1;
        set => DPp1 = value;
    }
     
    /// <summary>
    /// Потери давления в полости, образованной абразивной струей, МПа 
    /// </summary>
    public double DPpMPa
    {
        get => DPp1 * 1.0E-6;
        set => DPp1 = value / 1.0E-6;
    }
     
    /// <summary>
    /// Коэффициент безопасности работ, д. ед. 
    /// </summary>
    public double Kbr
    {
        get => Kbr1;
        set => Kbr1 = value;
    }
     
    /// <summary>
    /// Коэффициент безопасности работ, % 
    /// </summary>
    public double KbrPerc
    {
        get => Kbr1 * 1.0E2;
        set => Kbr1 = value / 1.0E2;
    }
     
    /// <summary>
    /// Общее требуемое количество жидкости для проведения ГПП, м3 
    /// </summary>
    public double Qrej
    {
        get => Qrej1;
        set => Qrej1 = value;
    }
     
    /// <summary>
    /// Объем скважины, м3 
    /// </summary>
    public double Vc
    {
        get => Vc1;
        set => Vc1 = value;
    }
     
    /// <summary>
    /// Внутренний объем НКТ, м3 
    /// </summary>
    public double Vnkt
    {
        get => Vnkt1;
        set => Vnkt1 = value;
    }
     
    /// <summary>
    /// Объем жидкости, вытесняемой насосно-компрессорными трубами, м3
    /// </summary>
    public double Vtnkt
    {
        get => Vtnkt1;
        set => Vtnkt1 = value;
    }
     
    /// <summary>
    /// Объем эксплуатационной колонны под башмаком НКТ, м3 
    /// </summary>
    public double Vbek
    {
        get => Vbek1;
        set => Vbek1 = value;
    }
     
    /// <summary>
    /// Объем кольцевого пространства скважины, м3 
    /// </summary>
    public double Vkp
    {
        get => Vkp1;
        set => Vkp1 = value;
    }
     
    /// <summary>
    /// Требуемый объем песчано-жидкостной смеси, м3 
    /// </summary>
    public double Vpzhs
    {
        get => Vpzhs1;
        set => Vpzhs1 = value;
    }
     
    /// <summary>
    /// Требуемый объем промывочной жидкости, м3 
    /// </summary>
    public double Vpzh
    {
        get => Vpzh1;
        set => Vpzh1 = value;
    }
     
    /// <summary>
    /// Требуемый объем продавочной жидкости, м3 
    /// </summary>
    public double Vprodzh
    {
        get => Vprodzh1;
        set => Vprodzh1 = value;
    }
     
    /// <summary>
    /// Требуемое количество кварцевого песка, кг 
    /// </summary>
    public double Qp
    {
        get => Qp1;
        set => Qp1 = value;
    }
     
    /// <summary>
    /// Объемная доля песка 
    /// </summary>
    public double Cp
    {
        get => Cp1;
        set => Cp1 = value;
    }
     
    /// <summary>
    /// Необходимый темп закачки жидкости (расход), м3/c 
    /// </summary>
    public double Qzak
    {
        get => Qzak1;
        set => Qzak1 = value;
    }

    /// <summary>
    /// Номер скорости подачи насосного агрегата 
    /// </summary>
    public int speedNumber
    {
        get => speedNumber1;
        set => speedNumber1 = value;
    }
     
    /// <summary>
    /// Требуемое количество насосных агрегатов, шт
    /// </summary>
    public int Nagr
    {
        get => Nagr1;
        set => Nagr1 = value;
    }

    /// <summary>
    /// Плотность ПЖС или ЖГС, кг/м3 
    /// </summary>
    public double Ropzhszhgs
    {
        get => Ropzhszhgs1;
        set => Ropzhszhgs1 = value;
    }

    /// <summary>
    /// Плотность ПЖ или ЖГС, кг/м3 
    /// </summary>
    public double Ropzhzhgs
    {
        get => Ropzhzhgs1;
        set => Ropzhzhgs1 = value;
    }

    /// <summary>
    /// Вязкость ПЖС или ЖГС, мПа•с 
    /// </summary>
    public double Mjupzhszhgs
    {
        get => Mjupzhszhgs1;
        set => Mjupzhszhgs1 = value;
    }
     
    /// <summary>
    /// Давление на устье, Па
    /// </summary>
    public double Pu
    {
        get => Pu1;
        set => Pu1 = value;
    }
     
    /// <summary>
    /// Давление на устье, МПа 
    /// </summary>
    public double PuMPa
    {
        get => Pu1 * 1.0E-6;
    }

    /// <summary>
    /// Давление на манифольде, Па
    /// </summary>
    public double Pm
    {
        get => Pm1;
        set => Pm1 = value;
    }
     
    /// <summary>
    /// Давление на манифольде, МПа 
    /// </summary>
    public double PmMPa
    {
        get => Pm1 * 1.0E-6;
    }
     
    /// <summary>
    /// Давление на затрубе, Па
    /// </summary>
    public double Pzatr
    {
        get => Pzatr1;
        set => Pzatr1 = value;
    }
     
    /// <summary>
    /// Давление на затрубе, МПа 
    /// </summary>
    public double PzatrMPa
    {
        get => Pzatr1 * 1.0E-6;
    }

    /// <summary>
    /// Давление на затрубе перед запуском, когда Рзаб =Рпл, Па
    /// </summary>
    private double Pzatr0
    {
        get => Pzatr01;
        set => Pzatr01 = value;
    }
     
    /// <summary>
    /// Давление на забое скважины на предыдущем такте, Па
    /// </summary>
    public double Pz_1
    {
        get => Pz_11;
        set => Pz_11 = value;
    }
    /// <summary>
    /// Давление на забое скважины, Па
    /// </summary>
    public double Pz
    {
        get => Pz1;
        set => Pz1 = value;
    }
     
    /// <summary>
    /// Давление на забое скважины, МПа 
    /// </summary>
    public double PzMPa
    {
        get => Pz1 * 1.0E-6;
    }

    /// <summary>
    /// Объемный расход закачивания жидкости в лифтовую колонну (подача насосного агрегата), зависящая от выбранной передачи КПП, м3/с 
    /// </summary>
    public double q
    {
        get => q1;
        set => q1 = value;
    }

    /// <summary>
    /// Внутренний диаметр НКТ или ЭК, м   
    /// </summary>
    public double dnktek
    {
        get => dnktek1;
        set => dnktek1 = value;
    }
   
    /// <summary>
    ///  Значение времени на предыдущем такте, секунды
    /// </summary>
    public double time_1 
    {
        get => time_11;
        set => time_11 = value;
    }
    /// <summary>
    /// Время, секунды  
    /// </summary>
    public double time
    {
        get => time1;
        set => time1 = value;
    }

    /// <summary>
    /// Время остановки скважины на исследование, секунды  
    /// </summary>
    public double t0
    {
        get => t01;
        set => t01 = value;
    }

    /// <summary>
    /// Время работы скважины на установившемся режиме, секунды  
    /// </summary>
    public double tp
    {
        get => tp1;
        set => tp1 = value;
    }

    /// <summary>
    /// Продолжительность исследования, секунды  
    /// </summary>
    public double dt
    {
        get => dt1;
        set => dt1 = value;
    }

    /// <summary>
    /// Время Хорнера, секунды  
    /// </summary>
    public double dte
    {
        get => dte1;
        set => dte1 = value;
    }

    /// <summary>
    /// Время первого замера, секунды  
    /// </summary>
    public double tz1
    {
        get => tz11;
        set => tz11 = value;
    }

    /// <summary>
    /// Время второго замера, секунды  
    /// </summary>
    public double tz2
    {
        get => tz21;
        set => tz21 = value;
    }
     
    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости
    /// (жидкости глушения/песчано-жидкостной смеси/продавочной жидкости)
    /// при течении по трубам лифтовой колонны (НКТ), Па
    /// </summary>
    public double DPpzhn
    {
        get => DPpzhn1;
        set => DPpzhn1 = value;
    }
     
    /// <summary>
    /// Потери давления на трение в НКТ ΔРНКТ для ньютоновской жидкости (жидкости глушения/песчано-жидкостной смеси)
    /// при течении по трубам лифтовой колонны (НКТ), МПа
    /// </summary>
    public double DPpzhnMPa
    {
        get => DPpzhn1 * 1.0E-6;
    }

    /// <summary>
    /// Число Рейнольдса
    /// </summary>
    public double Re 
    {
        get => Re1;
        set => Re1 = value;
    }
     
    /// <summary>
    /// Число Рейнольдса для труб (НКТ или ЭК)
    /// </summary>
    public double Retr
    {
        get => Retr1;
        set => Retr1 = value;
    }
     
    /// <summary>
    /// Число Рейнольдса для кольцевого пространства (между внешним диаметром НКТ и внутренним диаметром ЭК)
    /// </summary>
    public double Rekp
    {
        get => Rekp1;
        set => Rekp1 = value;
    }
     
    /// <summary>
    /// Число Рейнольдса в ЭК
    /// </summary>
    public double Reek
    {
        get => Reek1;
        set => Reek1 = value;
    }
     
    /// <summary>
    /// Число Рейнольдса в НКТ
    /// </summary>
    public double Renkt
    {
        get => Renkt1;
        set => Renkt1 = value;
    }

    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС в НКТ) в НКТ от подачи насоса (q) во времени, м
    /// </summary>
    public double Xnkt
    {
        get => Xnkt1;
        set => Xnkt1 = value;
    }
     
    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» (высоты столба ПЖС/ЖГС в ЭК от башмака НКТ до зумпфа (дна) скважины) в ЭК
    /// от подачи насоса (q) во времени, м
    /// </summary>
    public double Xek
    {
        get => Xek1;
        set => Xek1 = value;
    }
     
    /// <summary>
    /// Зависимость изменения положения границы «ПЖС/ЖГС – ЖГС» в КП от подачи насоса (q) во времени, м
    /// </summary>
    public double Xkp
    {
        get => Xkp1;
        set => Xkp1 = value;
    }
     
    /// <summary>
    /// Коэффициент потерь на трение по длине в НКТ
    /// </summary>
    public double LambdaNkt
    {
        get => LambdaNkt1;
        set => LambdaNkt1 = value;
    }
     
    /// <summary>
    /// Коэффициент потерь на трение по длине в КП 
    /// </summary>
    public double LambdaKp
    {
        get => LambdaKp1;
        set => LambdaKp1 = value;
    }
     
    /// <summary>
    /// Коэффициент гидравлического трения потока в ЭК 
    /// </summary>
    public double LambdaEk
    {
        get => LambdaEk1;
        set => LambdaEk1 = value;
    }
     
    /// <summary>
    /// Потери давления на трение в кольцевом пространстве ΔРКП для ПЖС/промывочной жидкости при течении по кольцевому пространству, Па
    /// </summary>
    public double DPpzhk
    {
        get => DPpzhk1;
        set => DPpzhk1 = value;
    }
     
    /// <summary>
    /// Потери давления на трение в кольцевом пространстве ΔРКП для ПЖС/промывочной жидкости при течении по кольцевому пространству, МПа 
    /// </summary>
    public double DPpzhkMPa
    {
        get => DPpzhk1 * 1.0E-6;
    }
     
    /// <summary>
    /// Потери давления на трение в ЭК ΔРЭК для ПЖС/ЖГС при течении по ЭК, Па
    /// </summary>
    public double DPpzhe
    {
        get => DPpzhe1;
        set => DPpzhe1 = value;
    }
     
    /// <summary>
    /// Потери давления на трение в ЭК ΔРЭК для ПЖС/ЖГС при течении по ЭК, МПа 
    /// </summary>
    public double DPpzheMPa
    {
        get => DPpzhe1 * 1.0E-6;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки, Па
    /// </summary>
    public double Pao1
    {
        get => Pao11;
        set => Pao11 = value;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки, МПа 
    /// </summary>
    public double Pao1MPa
    {
        get => Pao11 * 1.0E-6;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ, Па
    /// </summary>
    public double Pzo1
    {
        get => Pzo11;
        set => Pzo11 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ, МПа 
    /// </summary>
    public double Pzo1MPa
    {
        get => Pzo11 * 1.0E-6;
    }

    /// <summary>
    /// Давление на насосном агрегате на первом временном этапе
    /// (заполнение жидкостью разрыва НКТ от устья до башмака НКТ), Па
    /// </summary>
    public double Pap1 
    {
        get => Pap11;
        set => Pap11 = value;
    } 
    /// <summary>
    /// Давление на насосном агрегате при осуществлени прямой промывки, Па
    /// </summary>
    public double Pap2
    {
        get => Pap21;
        set => Pap21 = value;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при осуществлени прямой промывки, МПа 
    /// </summary>
    public double Pap2MPa
    {
        get => Pap21 * 1.0E-6;
    }
     
    /// <summary>
    /// Забойное давление в скважине на первом временном этапе (заполнение жидкостью разрыва НКТ от устья до башмака НКТ), Па
    /// </summary>
    public double Pzp1
    {
        get => Pzp11;
        set => Pzp11 = value;
    }

    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖС НКТ от устья до башмака НКТ (до перфоратора), Па
    /// </summary>
    public double Pzp2
    {
        get => Pzp21;
        set => Pzp21 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖС НКТ от устья до башмака НКТ (до перфоратора), МПа 
    /// </summary>
    public double Pzp2MPa
    {
        get => Pzp21 * 1.0E-6;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при продавливании  ПЖС через перфоратор на циркуляции, Па
    /// </summary>
    public double Pap3
    {
        get => Pap31;
        set => Pap31 = value;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при продавливании  ПЖС через перфоратор на циркуляции, МПа 
    /// </summary>
    public double Pap3MPa
    {
        get => Pap31 * 1.0E-6;
    }

    /// <summary>
    /// Забойное давление в скважине при ГПП (формировании перфорационных каналов), Па
    /// </summary>
    public double Pzp3
    {
        get => Pzp31;
        set => Pzp31 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине при ГПП (формировании перфорационных каналов), МПа 
    /// </summary>
    public double Pzp3MPa
    {
        get => Pzp31 * 1.0E-6;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при продавливании ПЖС, находящейся в НКТ, продавочной жидкостью, Па
    /// </summary>
    public double Pap4
    {
        get => Pap41;
        set => Pap41 = value;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при продавливании ПЖС, находящейся в НКТ, продавочной жидкостью, МПа 
    /// </summary>
    public double Pap4MPa
    {
        get => Pap41 * 1.0E-6;
    }

    /// <summary>
    /// Давление на насосном агрегате на пятом временном этапе (заполнение ЖП ЭК от башмака НКТ до зумпфа (дна) скважины, полное продавливание ЖР через отверстия перфорации), Па
    /// </summary>
    public double Pap5 
    {
        get => Pap51;
        set => Pap51 = value;
    }

    /// <summary>
    /// Давление на насосном агрегате на шестом временном этапе (продавливание ЖП через отверстия перфорации в пласт жидкостью пропантоносителем, требуемый объем ЖП должен быть больше VНКТ+VЭК (под башмаком НКТ)), Па
    /// </summary>
    public double Pap6
    {
        get => Pap61;
        set => Pap61 = value;
    }

    /// <summary>
    /// Давление на насосном агрегате на седьмом временном этапе (продавливание ЖП через отверстия перфорации продавочной жидкостью ПЖ (ЖГС)), Па
    /// </summary>
    public double Pap7
    {
        get => Pap71;
        set => Pap71 = value;
    }

    /// <summary>
    /// Давление на насосном агрегате на восьмом временном этапе (заполнение ПЖ ЭК от башмака НКТ до зумпфа (дна) скважины, полное продавливание ЖП через отверстия перфорации), Па
    /// </summary>
    public double Pap8
    {
        get => Pap81;
        set => Pap81 = value;
    }
    
    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖ НКТ от устья до башмака НКТ (до перфоратора), Па
    /// </summary>
    public double Pzp4
    {
        get => Pzp41;
        set => Pzp41 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполнении ПЖ НКТ от устья до башмака НКТ (до перфоратора), МПа 
    /// </summary>
    public double Pzp4MPa
    {
        get => Pzp41 * 1.0E-6;
    }

    /// <summary>
    /// Забойное давление в скважине на пятом временном этапе (заполнение ЖП ЭК от башмака НКТ
    /// до зумпфа (дна) скважины, полное продавливание ЖР через отверстия перфорации), Па
    /// </summary>
    public double Pzp5
    {
        get => Pzp51;
        set => Pzp51 = value;
    }

    /// <summary>
    /// Забойное давление в скважине на шестом временном этапе (продавливание ЖП через отверстия перфорации
    /// в пласт жидкостью пропантоносителем, требуемый объем ЖП должен быть больше VНКТ+VЭК (под башмаком НКТ)), Па
    /// </summary>
    public double Pzp6
    {
        get => Pzp61;
        set => Pzp61 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине на седьмом временном этапе
    /// (продавливание ЖП через отверстия перфорации продавочной жидкостью ПЖ (ЖГС)), Па
    /// </summary>
    public double Pzp7
    {
        get => Pzp71;
        set => Pzp71 = value;
    }

    /// <summary>
    /// Забойное давление в скважине на восьмом временном этапе (заполнение ПЖ ЭК от башмака НКТ до зумпфа (дна) скважины,
    /// полное продавливание ЖП через отверстия перфорации), Па
    /// </summary>
    public double Pzp8
    {
        get => Pzp81;
        set => Pzp81 = value;
    }

    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из кольцевого пространства), Па
    /// </summary>
    public double Pao5
    {
        get => Pao51;
        set => Pao51 = value;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из кольцевого пространства), МПа 
    /// </summary>
    public double Pao5MPa
    {
        get => Pao51 * 1.0E-6;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ, Па
    /// </summary>
    public double Pzo5
    {
        get => Pzo51;
        set => Pzo51 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполнении промывочной жидкостью КП от устья до башмака НКТ, МПа 
    /// </summary>
    public double Pzo5MPa
    {
        get => Pzo51 * 1.0E-6;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из НКТ), Па
    /// </summary>
    public double Pao6
    {
        get => Pao61;
        set => Pao61 = value;
    }
     
    /// <summary>
    /// Давление на насосном агрегате при осуществлени обратной промывки (вымывании ПЖС промывочной жидкостью из НКТ), МПа 
    /// </summary>
    public double Pao6MPa
    {
        get => Pao61 * 1.0E-6;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполненном КП промывочной жидкостью, Па
    /// </summary>
    public double Pzo6
    {
        get => Pzo61;
        set => Pzo61 = value;
    }
     
    /// <summary>
    /// Забойное давление в скважине при заполненном КП промывочной жидкостью, МПа 
    /// </summary>
    public double Pzo6MPa
    {
        get => Pzo61 * 1.0E-6;
    }
     
    /// <summary>
    /// Накопленный объем закачиваемой в скважину ПЖС/ПЖ, м3
    /// </summary>
    public double Vnak
    {
        get => Vnak1;
        set => Vnak1 = value;
    }
    
    /// <summary>
    /// Проницаемость пласта, м2
    /// </summary>
    public double k
    {
        get => k1;
        set => k1 = value;
    }
    
    /// <summary>
    /// Проницаемость пласта, мкм2
    /// </summary>
    public double kmkm
    {
        get => k1 * 1.0E12;
        set => k1 = value / 1.0E12;
    }
    
    /// <summary>
    /// Пористость пласта, д. ед.
    /// </summary>
    public double m
    {
        get => m1;
        set => m1 = value;
    }
    
    /// <summary>
    /// Коэффициент пьезопроводности, м2/с
    /// </summary>
    public double piezoconductivityOfTheFormation
    {
        get => piezoconductivityOfTheFormation1;
        set => piezoconductivityOfTheFormation1 = value;
    }
    
    /// <summary>
    /// Толщина пласта, м
    /// </summary>
    public double h
    {
        get => h1;
        set => h1 = value;
    }
    
    /// <summary>
    /// Скин-фактор
    /// </summary>
    public double skinFactor
    {
        get => skinFactor1;
        set => skinFactor1 = value;
    }
    
    /// <summary>
    /// Радиус контура питания, м
    /// </summary>
    public double Rk
    {
        get => Rk1;
        set => Rk1 = value;
    }
    
    /// <summary>
    /// Предельная депрессия разрушения пласта, Па
    /// </summary>
    public double deltaPpred
    {
        get => deltaPpred1;
        set => deltaPpred1 = value;
    }
    
    /// <summary>
    /// Предельная депрессия разрушения пласта, МПа
    /// </summary>
    public double deltaPpredMPa
    {
        get => deltaPpred1 * 1.0E-6;
        set => deltaPpred1 = value / 1.0E-6;
    }
    
    /// <summary>
    /// Коэффициент угла наклона для расчета степени разрушения коллектора до ∆Pпред, кг/(м3•Па)
    /// </summary>
    public double A1
    {
        get => A11;
        set => A11 = value;
    }
    
    /// <summary>
    /// Коэффициент угла наклона для расчета степени разрушения коллектора до ∆Pпред, кг/(м3•Па)
    /// </summary>
    public double A1gr
    {
        get => A11 * 1.0E3;
        set => A11 = value / 1.0E3;
    }
    
    /// <summary>
    /// Коэффициент угла наклона для расчета степени разрушения коллектора после ∆Pпред, кг/(м3•Па)
    /// </summary>
    public double A2
    {
        get => A21;
        set => A21 = value;
    }
    
    /// <summary>
    /// Коэффициент угла наклона для расчета степени разрушения коллектора после ∆Pпред, кг/(м3•Па)
    /// </summary>
    public double A2gr
    {
        get => A21 * 1.0E3;
        set => A21 = value / 1.0E3;
    }
    
    /// <summary>
    /// Глубина залегания пласта (длина скважины), м
    /// </summary>
    public double Lskv
    {
        get => Lskv1;
        set => Lskv1 = value;
    }
    
    /// <summary>
    /// Плотность нефти, кг/м3
    /// </summary>
    public double densityOfOil
    {
        get => densityOfOil1;
        set => densityOfOil1 = value;
    }
    
    /// <summary>
    /// Вязкость нефти, Па•с
    /// </summary>
    public double oilViscosity
    {
        get => oilViscosity1;
        set => oilViscosity1 = value;
    }
    
    /// <summary>
    /// Вязкость нефти, мПа•с
    /// </summary>
    public double oilViscositymPas
    {
        get => oilViscosity1 * 1.0E3;
        set => oilViscosity1 = value / 1.0E3;
    }
    
    /// <summary>
    /// Газовый фактор, м3/м3
    /// </summary>
    public double gasFactor
    {
        get => gasFactor1;
        set => gasFactor1 = value;
    }

    /// <summary>
    /// Давление насыщения нефти газом, Па 
    /// </summary>
    public double Pnas0
    {
        get => Pnas01;
        set => Pnas01 = value;
    }
    
    /// <summary>
    /// Давление насыщения нефти газом, МПа 
    /// </summary>
    public double Pnas0MPa
    {
        get => Pnas01 * 1.0E-6;
        set => Pnas01 = value / 1.0E-6;
    }

    /// <summary>
    /// Давление насыщения нефти газом при T[j], Па 
    /// </summary>
    public double[] Pnas
    {
        get => Pnas1;
        set => Pnas1 = value;
    }
    
    /// <summary>
    /// Объемный коэффициент нефти
    /// </summary>
    public double bOil
    {
        get => bOil1;
        set => bOil1 = value;
    }
    
    /// <summary>
    /// Вязкость воды, Па•с
    /// </summary>
    public double WaterViscosity
    {
        get => WaterViscosity1;
        set => WaterViscosity1 = value;
    }
    
    /// <summary>
    /// Вязкость воды, мПа•с
    /// </summary>
    public double WaterViscositymPas
    {
        get => WaterViscosity1 * 1.0E3;
        set => WaterViscosity1 = value / 1.0E3;
    }
    
    /// <summary>
    /// Обводненность продукции при ∆P=0, д.ед.
    /// </summary>
    public double productWaterCut0
    {
        get => productWaterCut01;
        set => productWaterCut01 = value;
    }
    
    /// <summary>
    /// Обводненность продукции при ∆P=∆Pmax, д.ед.
    /// </summary>
    public double productWaterCutMax
    {
        get => productWaterCutMax1;
        set => productWaterCutMax1 = value;
    }
    
    /// <summary>
    /// Плотность газа, кг/м3
    /// </summary>
    public double densityOfGas
    {
        get => densityOfGas1;
        set => densityOfGas1 = value;
    }
    
    /// <summary>
    /// Коэффициент сжимаемости газа
    /// </summary>
    public double Z
    {
        get => Z1;
        set => Z1 = value;
    }
    
    /// <summary>
    /// Молярная доля азота в составе газа, д.ед.
    /// </summary>
    public double Na
    {
        get => Na1;
        set => Na1 = value;
    }
    
    /// <summary>
    /// Молярная доля метана в составе газа, д.ед.
    /// </summary>
    public double Nc
    {
        get => Nc1;
        set => Nc1 = value;
    }
     
    /// <summary>
    /// Дебит скважины, м3/с
    /// </summary>
    public double Q
    {
        get => Q1;
        set => Q1 = value;
    }
     
    /// <summary>
    /// Дебит скважины во время первого замера, м3/с
    /// </summary>
    public double Qz1
    {
        get => Qz11;
        set => Qz11 = value;
    }
     
    /// <summary>
    /// Дебит скважины во время второго замера, м3/с
    /// </summary>
    public double Qz2
    {
        get => Qz21;
        set => Qz21 = value;
    }
     
    /// <summary>
    /// Коэффициент продуктивности скважины, м3/(с*Па)
    /// </summary>
    public double Kp
    {
        get => Kp1;
        set => Kp1 = value;
    }
     
    /// <summary>
    /// Удельный коэффициент продуктивности скважины, м2/(с* Па)
    /// </summary>
    public double Kpud
    {
        get => Kpud1;
        set => Kpud1 = value;
    }
    
    /// <summary>
    /// Вязкость, Па•с
    /// </summary>
    public double viscosity
    {
        get => viscosity1;
        set => viscosity1 = value;
    }
    
    /// <summary>
    /// Радиус скважины, м
    /// </summary>
    public double rc
    {
        get => rc1;
        set => rc1 = value;
    }
    
    /// <summary>
    /// Радиус скважины по долоту, м
    /// </summary>
    public double rcd
    {
        get => rcd1;
        set => rcd1 = value;
    }
     
    /// <summary>
    /// Решение уравнения упругого режима (пьезопроводности), Па
    /// </summary>
    public double Pqv
    {
        get => Pqv1;
        set => Pqv1 = value;
    }
     
    /// <summary>
    /// Депрессия на пласт, Па
    /// </summary>
    public double DP
    {
        get => DP1;
        set => DP1 = value;
    }
    
    /// <summary>
    /// Депрессия на пласт во время первого замера, Па
    /// </summary>
    public double DPz1
    {
        get => DPz11;
        set => DPz11 = value;
    }
    
    /// <summary>
    /// Депрессия на пласт во время второго замера, Па
    /// </summary>
    public double DPz2
    {
        get => DPz21;
        set => DPz21 = value;
    }
     
    /// <summary>
    /// Максимальная депрессия на пласт, Па
    /// </summary>
    public double DPmax
    {
        get => DPmax1;
        set => DPmax1 = value;
    }
     
    /// <summary>
    /// Смещение зависимости депрессии на пласт от логарифма времени по оси ординат, Па
    /// </summary>
    public double Adp
    {
        get => Adp1;
        set => Adp1 = value;
    }
     
    /// <summary>
    /// Тангенс угла наклона зависимости депрессии на пласт от логарифма времени
    /// </summary>
    public double idp
    {
        get => idp1;
        set => idp1 = value;
    }
     
    /// <summary>
    /// Гидропроводность, м3/(с*Па)
    /// </summary>
    public double eps
    {
        get => eps1;
        set => eps1 = value;
    }
     
    /// <summary>
    /// Приведенный радиус скважины, м
    /// </summary>
    public double rpr
    {
        get => rpr1;
        set => rpr1 = value;
    }
     
    /// <summary>
    /// Коэффициент совершенства скважины
    /// </summary>
    public double Delc
    {
        get => Delc1;
        set => Delc1 = value;
    }
     
    /// <summary>
    /// Период стабилизации, с
    /// </summary>
    public double Tstab
    {
        get => Tstab1;
        set => Tstab1 = value;
    }
    
    /// <summary>
    /// Коэффициент в формуле вычисления периода стабилизации
    /// </summary>
    public double Kstab
    {
        get => Kstab1;
        set => Kstab1 = value;
    }
    
    /// <summary>
    /// Коэффициент подвижности пласта, м2/(с*Па)
    /// </summary>
    public double Kpodv
    {
        get => Kpodv1;
        set => Kpodv1 = value;
    }
    
    /// <summary>
    /// Забойное давление в скважине в момент остановки скважины на исследование, Па
    /// </summary>
    public double Pzost
    {
        get => Pzost1;
        set => Pzost1 = value;
    }
    
    /// <summary>
    /// Забойное давление после 1 ч с момента остановки скважины на исследование, Па
    /// </summary>
    public double Pzost1h
    {
        get => Pzost1h1;
        set => Pzost1h1 = value;
    }

    /// <summary>
    /// Забойное давление, рассчитанное при помощи распределения давления, Па
    /// </summary>
    public double Pzpk
    {
        get => Pzpk1;
        set => Pzpk1 = value;
    } 
    
    /// <summary>
    /// Угловой коэффициент
    /// </summary>
    public double mln
    {
        get => mln1;
        set => mln1 = value;
    }
    
    /// <summary>
    /// Упругоемкость (сжимаемость) пласта
    /// </summary>
    public double Bpl
    {
        get => Bpl1;
        set => Bpl1 = value;
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по первой формуле, Па
    /// </summary>
    public double Pbuf1
    {
        get => Pbuf11;
        set => Pbuf11 = value;
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по второй формуле, Па
    /// </summary>
    public double Pbuf2
    {
        get => Pbuf21;
        set => Pbuf21 = value;
    }
    
    /// <summary>
    /// Наибольшее значение буферного давления из рассчитанных по формулам 1 и 2, Па
    /// </summary>
    public double Pbufm
    {
        get => Pbufm1;
        set => Pbufm1 = value;
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное по третьей формуле, Па
    /// </summary>
    public double Pbuf3
    {
        get => Pbuf31;
        set => Pbuf31 = value;
    }
    
    /// <summary>
    /// Расход газа, м3/с
    /// </summary>
    public double Qg
    {
        get => Qg1;
        set => Qg1 = value;
    }
    
    /// <summary>
    /// Линейное давление, Па
    /// </summary>
    public double Plin
    {
        get => Plin1;
        set => Plin1 = value;
    }
    
    /// <summary>
    /// Диаметр штуцера на предыдущем такте, м
    /// </summary>
    public double dsht_1
    {
        get => dsht_11;
        set => dsht_11 = value;
    }
    /// <summary>
    /// Диаметр штуцера, м
    /// </summary>
    public double dsht
    {
        get => dsht1;
        set => dsht1 = value;
    }
    
    /// <summary>
    /// Удельный расход выделившегося газа на устье, м3/ м3
    /// </summary>
    public double Vgvust
    {
        get => Vgvust1;
        set => Vgvust1 = value;
    }

    /// <summary>
    /// Температура на устье, К
    /// </summary>
    public double Tu
    {
        get => Tu1;
        set => Tu1 = value;
    }

    /// <summary>
    /// Температура на буфере, К
    /// </summary>
    public double Tbuf
    {
        get => Tbuf1;
        set => Tbuf1 = value;
    }

    /// <summary>
    /// Давление на приеме в НКТ, Па
    /// </summary>
    public double Ppr
    {
        get => Ppr1;
        set => Ppr1 = value;
    }

    /// <summary>
    /// Глубина кровли пласта, м
    /// </summary>
    public double Lk
    {
        get => Lk1;
        set => Lk1 = value;
    }

    /// <summary>
    /// Величина динамического уровня в скважине, м
    /// </summary>
    public double hdin
    {
        get => hdin1;
        set => hdin1 = value;
    }

    /// <summary>
    /// Величина динамического уровня перед остановкой скважины, м
    /// </summary>
    public double hdin0
    {
        get => hdin01;
        set => hdin01 = value;
    } 
    
    /// <summary>
    /// Статический уровень в скважине перед запуском, когда Рзаб =Рпл, м
    /// </summary>
    public double hst
    {
        get => hst1;
        set => hst1 = value;
    }

    /// <summary>
    /// Количество свободного газа, д. ед.
    /// </summary>
    public double ng
    {
        get => ng1;
        set => ng1 = value;
    }

    /// <summary>
    /// Количество свободного газа, поступившего в насос, д. ед.
    /// </summary>
    public double ngn
    {
        get => ngn1;
        set => ngn1 = value;
    }

    /// <summary>
    /// Обводненность продукции, д. ед.
    /// </summary>
    public double nv
    {
        get => nv1;
        set => nv1 = value;
    }

    /// <summary>
    /// Показатель степени в формуле (2.35), д. ед.
    /// </summary>
    public double nst
    {
        get => nst1;
        set => nst1 = value;
    }

    /// <summary>
    /// Содержание механических примесей, кг/м3
    /// </summary>
    public double MP
    {
        get => MP1;
        set => MP1 = value;
    }

    /// <summary>
    /// Температура пластовая, К
    /// </summary>
    public double Tpl
    {
        get => Tpl1;
        set => Tpl1 = value;
    }

    /// <summary>
    /// Температура пластовая, K
    /// </summary>
    public double Tplc
    {
        get => Tpl1 - 273;
        set => Tpl1 = value + 273;
    }

    /// <summary>
    /// Геотермический градиент, К/м
    /// </summary>
    public double Gtgr
    {
        get => Gtgr1;
        set => Gtgr1 = value;
    }

    /// <summary>
    /// Плотность жидкости, кг/м3 
    /// </summary>
    public double Rozh
    {
        get => Rozh1;
        set => Rozh1 = value;
    } 

    /// <summary>
    /// Расход жидкости, поступающей из пласта, м3/с 
    /// </summary>
    public double Qskv
    {
        get => Qskv1;
        set => Qskv1 = value;
    }

    /// <summary>
    /// Производительность насоса, м3/с 
    /// </summary>
    public double Qn
    {
        get => Qn1;
        set => Qn1 = value;
    }
    
    /// <summary>
    /// Скорость подъема жидкости в ЭК, м/с 
    /// </summary>
    public double Vzhek
    {
        get => Vzhek1;
        set => Vzhek1 = value;
    }
    
    /// <summary>
    /// Скорость подъема жидкости в НКТ, м/с 
    /// </summary>
    public double Vzhnkt
    {
        get => Vzhnkt1;
        set => Vzhnkt1 = value;
    }

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ в ЭК, м 
    /// </summary>
    public double L1
    {
        get => L11;
        set => L11 = value;
    }

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ, м 
    /// </summary>
    public double Hg1
    {
        get => Hg11;
        set => Hg11 = value;
    }

    /// <summary>
    /// Глубина, начиная с которой начнет выделяться газ в НКТ, м 
    /// </summary>
    public double Hg2
    {
        get => Hg21;
        set => Hg21 = value;
    }

    /// <summary>
    /// Верхняя граница глубин для расчета распределения давления газированной жидкости по методике Поэтмана-Карпентера, м 
    /// </summary>
    public double L2 
    {
        get => L21;
        set => L21 = value;
    }
   
    /// <summary>
    /// Давление, с которого начинается выделяться газ в ЭК, Па 
    /// </summary>
    public double P2ek
    {
        get => P2ek1;
        set => P2ek1 = value;
    }
    /// <summary>
    /// Давление, с которого начинается расчет распределения давления в НКТ, Па 
    /// </summary>
    public double P1nkt
    {
        get => P1nkt1;
        set => P1nkt1 = value;
    }

    /// <summary>
    /// Количество интервалов расчета давления газированной жидкости по методике Поэтмана-Карпентера, шт. 
    /// </summary>
    public double Nint
    {
        get => Nint1;
        set => Nint1 = value;
    }

    /// <summary>
    /// Температурный градиент потока, К/м 
    /// </summary>
    public double Tgrp
    {
        get => Tgrp1;
        set => Tgrp1 = value;
    } 

    /// <summary>
    /// Количество интервалов, на которых осуществляется рассчет давления и температуры, м 
    /// </summary>
    public int N 
    {
        get => N1;
        set => N1 = value;
    }
    /// <summary>
    /// Интервал H[j], на котором рассчитывается давление и температура, м 
    /// </summary>
    public double[] H
    {
        get => H1;
        set => H1 = value;
    }

    /// <summary>
    /// Температура в интервале H[j], К
    /// </summary>
    public double[] T
    {
        get => T1;
        set => T1 = value;
    }

    /// <summary>
    /// Давление в интервале Н[j], Па
    /// </summary>
    public double[] P
    {
        get => P1;
        set => P1 = value;
    }

    /// <summary>
    /// Признак, характеризующий то, где производится расчет (1.10) (ЭК (PrEKNKT = 1) или НКТ (PrEKNKT = 0))
    /// </summary>
    public int PrEKNKT
    {
        get => PrEKNKT1;
        set => PrEKNKT1 = value;
    }

    /// <summary>
    /// Температура на выходе из насоса, К
    /// </summary>
    public double Tvyh
    {
        get => Tvyh1;
        set => Tvyh1 = value;
    }

    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа
    /// </summary>
    public double RfP
    {
        get => RfP1;
        set => RfP1 = value;
    }

    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа
    /// </summary>
    public double mfT
    {
        get => mfT1;
        set => mfT1 = value;
    }

    /// <summary>
    /// Коэффициент для расчета объема выделившегося газа
    /// </summary>
    public double DfT
    {
        get => DfT1;
        set => DfT1 = value;
    }

    /// <summary>
    /// Удельный объем выделившегося газа при H[j], м3/м3
    /// </summary>
    public double[] Vgv
    {
        get => Vgv1;
        set => Vgv1 = value;
    }

    /// <summary>
    /// Удельный объем смеси, м3/м3
    /// </summary>
    public double Vcm
    {
        get => Vcm1;
        set => Vcm1 = value;
    }

    /// <summary>
    /// Давление при НУ = 101300, Па
    /// </summary>
    public double Pnu
    {
        get => Pnu1;
        set => Pnu1 = value;
    }
    
    /// <summary>
    /// Температура при НУ = 273, K
    /// </summary>
    public double Tnu
    {
        get => Tnu1;
        set => Tnu1 = value;
    }

    /// <summary>
    /// Удельная масса смеси, кг/м3
    /// </summary>
    public double Mcm
    {
        get => Mcm1;
        set => Mcm1 = value;
    }

    /// <summary>
    /// Плотность газожидкостной смеси, кг/м3
    /// </summary>
    public double Rocm
    {
        get => Rocm1;
        set => Rocm1 = value;
    }

    /// <summary>
    /// Корреляционный коэффициент
    /// </summary>
    public double f
    {
        get => f1;
        set => f1 = value;
    }

    /// <summary>
    /// Дифференциал (1.21), соответствующий интервалу H[j], на котором рассчитывается давление и температура, Па/м
    /// </summary>
    public double[] dPdH
    {
        get => dPdH1;
        set => dPdH1 = value;
    }
    
    /// <summary>
    /// Число, обратное дифференциалу (1.21), м/Па
    /// </summary>
    public double[] dHdP
    {
        get => dHdP1;
        set => dHdP1 = value;
    }

    /// <summary>
    /// Шаг изменения глубины для расчета давления газированной жидкости по методике Поэтмана-Карпентера, м
    /// </summary>
    public double dl
    {
        get => dl1;
        set => dl1 = value;
    }

    /// <summary>
    /// Шаг распределения давления газированной жидкости по методике Поэтмана-Карпентера, Па
    /// </summary>
    public double dP
    {
        get => dР1;
        set => dР1 = value;
    }

    /// <summary>
    /// Вязкость жидкости, Па•с
    /// </summary>
    public double Mjuzh
    {
        get => Mjuzh1;
        set => Mjuzh1 = value;
    }

    /// <summary>
    /// Давление на выходе из насоса, Па
    /// </summary>
    public double Pvyh
    {
        get => Pvyh1;
        set => Pvyh1 = value;
    }

    /// <summary>
    /// Вязкость эмульсии, Па•с
    /// </summary>
    public double MjuE
    {
        get => MjuE1;
        set => MjuE1 = value;
    }

    /// <summary>
    /// Параметр, учитывающий скорость сдвига
    /// </summary>
    public double A2E
    {
        get => A2E1;
        set => A2E1 = value;
    }

    /// <summary>
    /// Параметр, учитывающий скорость сдвига
    /// </summary>
    public double B2E
    {
        get => B2E1;
        set => B2E1 = value;
    }

    /// <summary>
    /// Объем выделившегося газа на приеме насоса, м3/м3
    /// </summary>
    public double Vgvpr
    {
        get => Vgvpr1;
        set => Vgvpr1 = value;
    }

    /// <summary>
    /// Температура на приеме насоса, К 
    /// </summary>
    public double Tprn
    {
        get => Tprn1;
        set => Tprn1 = value;
    }

    /// <summary>
    /// Температура на приеме НКТ, К 
    /// </summary>
    public double Tpr
    {
        get => Tpr1;
        set => Tpr1 = value;
    }

    /// <summary>
    /// Давление на приеме насоса, Па
    /// </summary>
    public double Pprn
    {
        get => Pprn1;
        set => Pprn1 = value;
    }

    /// <summary>
    /// Приведенная скорость жидкости, м/с
    /// </summary>
    public double Wzhpr
    {
        get => Wzhpr1;
        set => Wzhpr1 = value;
    }

    /// <summary>
    /// Скорость дрейфа газа, м/с
    /// </summary>
    public double Wgdr
    {
        get => Wgdr1;
        set => Wgdr1 = value;
    }

    /// <summary>
    /// Коэффициент сепарации газа при переходе откачиваемой продукции из кольцевого пространства скважины во всасывающую камеру насоса
    /// </summary>
    public double Ksvh
    {
        get => Ksvh1;
        set => Ksvh1 = value;
    }
    
    /// <summary>
    /// Коэффициент сепарации за счет работы газосепаратора, если такой установлен
    /// </summary>
    public double Ksgs
    {
        get => Ksgs1;
        set => Ksgs1 = value;
    }

    /// <summary>
    /// Коэффициент сепарации
    /// </summary>
    public double Ks
    {
        get => Ks1;
        set => Ks1 = value;
    }

    /// <summary>
    /// Вязкость газожидкостной смеси, Па*с
    /// </summary>
    public double Mjugzh
    {
        get => Mjugzh1;
        set => Mjugzh1 = value;
    }

    /// <summary>
    /// Давление над плунжером насоса, Па
    /// </summary>
    public double Pn
    {
        get => Pn1;
        set => Pn1 = value;
    }

    /// <summary>
    /// Давление, развиваемое насосом, Па
    /// </summary>
    public double Prn
    {
        get => Prn1;
        set => Prn1 = value;
    }

    /// <summary>
    /// Коэффициент наполнения насоса жидкостью, учитывающий влияние свободного газа 
    /// </summary>
    public double Eta1 
    {
        get => Eta11;
        set => Eta11 = value;
    }

    /// <summary>
    /// Коэффициент наполнения насоса жидкостью, учитывающий влияние потери хода плунжера
    /// </summary>
    public double Eta2 
    {
        get => Eta21;
        set => Eta21 = value;
    }

    /// <summary>
    /// Коэффициент усадки жидкости
    /// </summary>
    public double Eta3 
    {
        get => Eta31;
        set => Eta31 = value;
    }

    /// <summary>
    /// Коэффициент утечек
    /// </summary>
    public double Eta4
    {
        get => Eta41;
        set => Eta41 = value;
    }

    /// <summary>
    /// Коэффициент подачи насосной установки 
    /// </summary>
    public double Etap 
    {
        get => Etap1;
        set => Etap1 = value;
    }

    /// <summary>
    /// Коэффициент выигрыша хода плунжера
    /// </summary>
    public double Kvpl
    {
        get => Kvpl1;
        set => Kvpl1 = value;
    }

    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций штанг и труб, м
    /// </summary>
    public double LMsh
    {
        get => LMsh1;
        set => LMsh1 = value;
    }
    
    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций штанг, м
    /// </summary>
    public double LMsht
    {
        get => LMsht1;
        set => LMsht1 = value;
    }

    /// <summary>
    /// Потеря хода плунжера за счет упругих деформаций труб, м
    /// </summary>
    public double LMt
    {
        get => LMt1;
        set => LMt1 = value;
    }

    /// <summary>
    /// Потеря хода плунжера при включении осложнения, м
    /// </summary>
    public double dLM
    {
        get => dLM1;
        set => dLM1 = value;
    }

    /// <summary>
    /// Напор насоса, Н
    /// </summary>
    public double Hn
    {
        get => Hn1;
        set => Hn1 = value;
    }

    /// <summary>
    /// Длина хода плунжера, м
    /// </summary>
    public double S
    {
        get => S1;
        set => S1 = value;
    }

    /// <summary>
    /// Число качаний (двойных ходов) в секунду, 1/c
    /// </summary>
    public double nk 
    {
        get => nk1;
        set => nk1 = value;
    }

    /// <summary>
    /// Вес столба жидкости, действующий на плунжер, Н
    /// </summary>
    public double Pzh
    {
        get => Pzh1;
        set => Pzh1 = value;
    }

    /// <summary>
    /// Площадь сечения штанг, м2
    /// </summary>
    public double fsh
    {
        get => fsh1;
        set => fsh1 = value;
    }

    /// <summary>
    /// Модуль Юнга металла штанг, Па
    /// </summary>
    public double Esh
    {
        get => Esh1;
        set => Esh1 = value;
    }

    /// <summary>
    /// Площадь сечения металла труб, м2
    /// </summary>
    public double ft
    {
        get => ft1;
        set => ft1 = value;
    }

    /// <summary>
    /// Модуль Юнга металла труб, Па
    /// </summary>
    public double Et
    {
        get => Et1;
        set => Et1 = value;
    }

    /// <summary>
    /// Диаметр штанг, м
    /// </summary>
    public double dsh 
    {
        get => dsh1;
        set => dsh1 = value;
    }

    /// <summary>
    /// Площадь сечения плунжера насоса, м2
    /// </summary>
    public double Fpl 
    {
        get => Fpl1;
        set => Fpl1 = value;
    }

    /// <summary>
    /// Разность давлений, действующих на поверхность плунжера, Па
    /// </summary>
    public double DPpl
    {
        get => DPpl1;
        set => DPpl1 = value;
    }

    /// <summary>
    /// Гидростатическое давление столба жидкости, Па
    /// </summary>
    public double Pgst
    {
        get => Pgst1;
        set => Pgst1 = value;
    }

    /// <summary>
    /// Напор на преодоление гидравлического трения, м
    /// </summary>
    public double htren
    {
        get => htren1;
        set => htren1 = value;
    }

    /// <summary>
    /// Потери давления на трение жидкости в трубах, Па
    /// </summary>
    public double Ptren
    {
        get => Ptren1;
        set => Ptren1 = value;
    }

    /// <summary>
    /// Напор, соответствующий газлифтному эффекту, м
    /// </summary>
    public double hgl
    {
        get => hgl1;
        set => hgl1 = value;
    }
    
    /// <summary>
    /// Средний объем выделившегося газа в НКТ, м3/ м3
    /// </summary>
    public double gFactorMdl
    {
        get => gFactorMdl1;
        set => gFactorMdl1 = value;
    }

    /// <summary>
    /// Давление разгрузки в результате газлифтного эффекта, Па
    /// </summary>
    public double Prgl
    {
        get => Prgl1;
        set => Prgl1 = value;
    }

    /// <summary>
    /// Объем жидкости, протекающий через зазор между плунжером и цилиндром, м3/с
    /// </summary>
    public double Qut
    {
        get => Qut1;
        set => Qut1 = value;
    }

    /// <summary>
    /// Ширина зазора между плунжером и цилиндром, м
    /// </summary>
    public double Dzaz
    {
        get => Dzaz1;
        set => Dzaz1 = value;
    }

    /// <summary>
    /// Длина щели, м
    /// </summary>
    public double Lshl
    {
        get => Lshl1;
        set => Lshl1 = value;
    }

    /// <summary>
    /// Длина плунжера, м
    /// </summary>
    public double Lpl 
    {
        get => Lpl1;
        set => Lpl1 = value;
    }

    /// <summary>
    /// Дополнительные утечки при включении осложнений, м3/с
    /// </summary>
    public double DQosl
    {
        get => DQosl1;
        set => DQosl1 = value;
    }

    /// <summary>
    /// Диаметр цилиндра, м
    /// </summary>
    public double Dc
    {
        get => Dc1;
        set => Dc1 = value;
    }

    /// <summary>
    /// Диаметр плунжера насоса, м
    /// </summary>
    public double dpl
    {
        get => dpl1;
        set => dpl1 = value;
    }

    /// <summary>
    /// Вес колонны штанг в воздухе, Н
    /// </summary>
    public double Pshtv
    {
        get => Pshtv1;
        set => Pshtv1 = value;
    }

    /// <summary>
    /// Вес колонны штанг в жидкости, Н
    /// </summary>
    public double Pshtzh
    {
        get => Pshtzh1;
        set => Pshtzh1 = value;
    }

    /// <summary>
    /// Вес 1 м штанг в воздухе, Н/м
    /// </summary>
    public double qsht
    {
        get => qsht1;
        set => qsht1 = value;
    }

    /// <summary>
    /// Коэффициент плавучести штанг
    /// </summary>
    public double Karh
    {
        get => Karh1;
        set => Karh1 = value;
    }

    /// <summary>
    /// Плотность материала штанг, кг/м3
    /// </summary>
    public double Rosht
    {
        get => Rosht1;
        set => Rosht1 = value;
    }

    /// <summary>
    /// Вспомогательный коэффициент
    /// </summary>
    public double mw
    {
        get => mw1;
        set => mw1 = value;
    }
    
    /// <summary>
    /// Вспомогательный коэффициент
    /// </summary>
    public double Psi
    {
        get => Psi1;
        set => Psi1 = value;
    }

    /// <summary>
    /// Угловая скорость вращения вала кривошипа, рад/с
    /// </summary>
    public double wkr
    {
        get => wkr1;
        set => wkr1 = value;
    }

    /// <summary>
    /// Вибрационная нагрузка при ходе вверх, Н
    /// </summary>
    public double Pvibv
    {
        get => Pvibv1;
        set => Pvibv1 = value;
    }

    /// <summary>
    /// Вибрационная нагрузка при ходе вниз, Н
    /// </summary>
    public double Pvibn
    {
        get => Pvibn1;
        set => Pvibn1 = value;
    }

    /// <summary>
    /// Инерционная нагрузка при ходе вверх, Н
    /// </summary>
    public double Pinv
    {
        get => Pinv1;
        set => Pinv1 = value;
    }

    /// <summary>
    /// Инерционная нагрузка при ходе вниз, Н
    /// </summary>
    public double Pinn
    {
        get => Pinn1;
        set => Pinn1 = value;
    }

    /// <summary>
    /// Динамическая нагрузка при ходе вверх, Н
    /// </summary>
    public double Pdinv
    {
        get => Pdinv1;
        set => Pdinv1 = value;
    }

    /// <summary>
    /// Динамическая нагрузка при ходе вниз, Н
    /// </summary>
    public double Pdinn
    {
        get => Pdinn1;
        set => Pdinn1 = value;
    }

    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    public double aln1
    {
        get => aln11;
        set => aln11 = value;
    }
    
    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    public double aln2
    {
        get => aln21;
        set => aln21 = value;
    }
    
    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    public double an1
    {
        get => an11;
        set => an11 = value;
    }
    
    /// <summary>
    /// Коэф-нт для расчета вибрац. и инерц. нагрузок при ходе вверх и вниз
    /// </summary>
    public double an2
    {
        get => an21;
        set => an21 = value;
    }

    /// <summary>
    /// Коэффициент для расчета динамической нагрузки при ходе вверх
    /// </summary>
    public double Kdv
    {
        get => Kdv1;
        set => Kdv1 = value;
    }

    /// <summary>
    /// Коэффициент для расчета динамической нагрузки при ходе вниз
    /// </summary>
    public double Kdn
    {
        get => Kdn1;
        set => Kdn1 = value;
    }

    /// <summary>
    /// Сила механического трения колонны штанг о стенки НКТ, Н
    /// </summary>
    public double Ptrmeh
    {
        get => Ptrmeh1;
        set => Ptrmeh1 = value;
    }   

    /// <summary>
    /// Коэффициент трения штанг о трубы
    /// </summary>
    public double Csht
    {
        get => Csht1;
        set => Csht1 = value;
    }

    /// <summary>
    /// Сила трения плунжера о стенки цилиндра, Н
    /// </summary>
    public double Ptrpl
    {
        get => Ptrpl1;
        set => Ptrpl1 = value;
    }

    /// <summary>
    /// Сила гидравлического сопротивления от перепада давления в нагнетательном клапане насоса, Н
    /// </summary>
    public double Pkln
    {
        get => Pkln1;
        set => Pkln1 = value;
    }
    
    /// <summary>
    /// Потери давления в нагнетательном клапане, Па
    /// </summary>
    public double dpkln
    {
        get => dpkln1;
        set => dpkln1 = value;
    }

    /// <summary>
    /// Максимальная скорость движения газожидкостной смеси в отверстии седла нагнетательного клапана, м/с
    /// </summary>
    public double Vmaxn
    {
        get => Vmaxn1;
        set => Vmaxn1 = value;
    }

    /// <summary>
    /// Коэффициент расхода нагнетательного клапана
    /// </summary>
    public double Ksikln
    {
        get => Ksikln1;
        set => Ksikln1 = value;
    }

    /// <summary>
    /// Диаметр отверстия в седле нагнетательного клапана, м
    /// </summary>
    public double dkln
    {
        get => dkln1;
        set => dkln1 = value;
    }

    /// <summary>
    /// Максимальная нагрузка в точке подвеса штанг, Н
    /// </summary>
    public double Pmaxsht
    {
        get => Pmaxsht1;
        set => Pmaxsht1 = value;
    }

    /// <summary>
    /// Минимальная  нагрузка в точке подвеса штанг, Н
    /// </summary>
    public double Pminsht
    {
        get => Pminsht1;
        set => Pminsht1 = value;
    }

    /// <summary>
    /// Максимальное напряжение цикла, Па
    /// </summary>
    public double SGmax
    {
        get => SGmax1;
        set => SGmax1 = value;
    }

    /// <summary>
    /// Минимальное  напряжение цикла, Па
    /// </summary>
    public double SGmin
    {
        get => SGmin1;
        set => SGmin1 = value;
    }

    /// <summary>
    /// Амплитудное напряжение цикла, Па
    /// </summary>
    public double SGa
    {
        get => SGa1;
        set => SGa1 = value;
    }

    /// <summary>
    /// Приведенное напряжение цикла, Па
    /// </summary>
    public double SGpr
    {
        get => SGpr1;
        set => SGpr1 = value;
    }

    /// <summary>
    /// Допустимое значение приведенного напряжения цикла, Па
    /// </summary>
    public double SGprdop
    {
        get => SGprdop1;
        set => SGprdop1 = value;
    }

    /// <summary>
    /// Полезная мощность электродвигателя, Вт
    /// </summary>
    public double Jpol 
    {
        get => Jpol1;
        set => Jpol1 = value;
    }

    /// <summary>
    /// Потери мощности от утечек жидкости, Вт
    /// </summary>
    public double Etaut
    {
        get => Etaut1;
        set => Etaut1 = value;
    }

    /// <summary>
    /// Потери мощности в клапанных узлах, Вт
    /// </summary>
    public double Jkl
    {
        get => Jkl1;
        set => Jkl1 = value;
    }

    /// <summary>
    /// Потери давления во всасывающем клапане, Па
    /// </summary>
    public double dpklvs
    {
        get => dpklvs1;
        set => dpklvs1 = value;
    }

    /// <summary>
    /// Максимальная скорость движения газожидкостной смеси в отверстии седла всасывающего клапана, м/с
    /// </summary>
    public double Vmaxvs
    {
        get => Vmaxvs1;
        set => Vmaxvs1 = value;
    }

    /// <summary>
    /// Коэффициент расхода всасывающего клапана
    /// </summary>
    public double Ksiklvs
    {
        get => Ksiklvs1;
        set => Ksiklvs1 = value;
    }

    /// <summary>
    /// Диаметр отверстия в седле всасывающего клапана, м
    /// </summary>
    public double dklvs
    {
        get => dklvs1;
        set => dklvs1 = value;
    }

    /// <summary>
    /// Потери мощности на преодоление механического трения штанг в трубах, Вт
    /// </summary>
    public double Jtrm
    {
        get => Jtrm1;
        set => Jtrm1 = value;
    }

    /// <summary>
    /// Потери мощности на преодоление гидравлического трения штанг в трубах, Вт
    /// </summary>
    public double Jtrg
    {
        get => Jtrg1;
        set => Jtrg1 = value;
    }

    /// <summary>
    /// Кинематическая вязкость газожидкостной смеси, м2/с
    /// </summary>
    public double vgzh
    {
        get => vgzh1;
        set => vgzh1 = value;
    }

    /// <summary>
    /// Коэффициент для расчета потери мощности на преодоление гидравлического трения штанг в трубах
    /// </summary>
    public double Msht
    {
        get => Msht1;
        set => Msht1 = value;
    }
    
    /// <summary>
    /// Коэффициент для расчета потери мощности на преодоление гидравлического трения штанг в трубах
    /// </summary>
    public double Msht2
    {
        get => Msht21;
        set => Msht21 = value;
    }

    /// <summary>
    /// Потери мощности на преодоление трения плунжера в цилиндре, Вт
    /// </summary>
    public double Jtrpl
    {
        get => Jtrpl1;
        set => Jtrpl1 = value;
    }

    /// <summary>
    /// Потери мощности подземной части, Вт
    /// </summary>
    public double Jpch 
    {
        get => Jpch1;
        set => Jpch1 = value;
    }

    /// <summary>
    /// К.п.д. подземной части
    /// </summary>
    public double Etapch 
    {
        get => Etapch1;
        set => Etapch1 = value;
    }

    /// <summary>
    /// К.п.д. ШСНУ
    /// </summary>
    public double Etashsnu
    {
        get => Etashsnu1;
        set => Etashsnu1 = value;
    }

    /// <summary>
    /// К.п.д. станка-качалки
    /// </summary>
    public double Etask
    {
        get => Etask1;
        set => Etask1 = value;
    }

    /// <summary>
    /// К.п.д. электродвигателя
    /// </summary>
    public double Etaed
    {
        get => Etaed1;
        set => Etaed1 = value;
    }

    /// <summary>
    /// Полная мощность, затрачиваемая на подъем жидкости, Вт
    /// </summary>
    public double Jpoln
    {
        get => Jpoln1;
        set => Jpoln1 = value;
    }

    /// <summary>
    /// Сила тока, А
    /// </summary>
    public double I
    {
        get => I1;
        set => I1 = value;
    }

    /// <summary>
    /// Номинальная сила тока, А
    /// </summary>
    public double In
    {
        get => In1;
        set => In1 = value;
    }

    /// <summary>
    /// Напряжение, В
    /// </summary>
    public double U
    {
        get => U1;
        set => U1 = value;
    }

    /// <summary>
    /// Длительность одного цикла качаний, с
    /// </summary>
    public double Per
    {
        get => Per1;
        set => Per1 = value;
    }

    /// <summary>
    /// Время упругого растяжения колонны штанг, с
    /// </summary>
    public double tupr
    {
        get => tupr1;
        set => tupr1 = value;
    }

    /// <summary>
    /// Теоретическое перемещение штока, м
    /// </summary>
    public double Lteor
    {
        get => Lteor1;
        set => Lteor1 = value;
    }

    /// <summary>
    /// Теоретическая нагрузка, Н
    /// </summary>
    public double Pteor
    {
        get => Pteor1;
        set => Pteor1 = value;
    }
    
    /// <summary>
    /// Практическая нагрузка, Н
    /// </summary>
    public double Pprak
    {
        get => Pprak1;
        set => Pprak1 = value;
    }

    /// <summary>
    /// Максимальная нагрузка, Н
    /// </summary>
    public double Pmax
    {
        get => Pmax1;
        set => Pmax1 = value;
    }
    
    /// <summary>
    /// Минимальная нагрузка, Н
    /// </summary>
    public double Pmin
    {
        get => Pmin1;
        set => Pmin1 = value;
    }

    /// <summary>
    /// Динамические нагрузки при ходе вверх, Н
    /// </summary>
    public double Pvd
    {
        get => Pvd1;
        set => Pvd1 = value;
    }

    /// <summary>
    /// Динамические нагрузки при ходе вниз, Н
    /// </summary>
    public double Pnd
    {
        get => Pnd1;
        set => Pnd1 = value;
    }

    /// <summary>
    /// Теоретическое перемещение штока, м
    /// </summary>
    public double Lprak
    {
        get => Lprak1;
        set => Lprak1 = value;
    }

    /// <summary>
    /// Время упругого растяжения колонны штанг для практической динамограммы при ходе вверх, с
    /// </summary>
    public double tuprvv
    {
        get => tuprvv1;
        set => tuprvv1 = value;
    }

    /// <summary>
    /// Время упругого растяжения колонны штанг для практической динамограммы при ходе вниз, с
    /// </summary>
    public double tuprvn
    {
        get => tuprvn1;
        set => tuprvn1 = value;
    }

    /// <summary>
    /// Добавка при ходе вверх, с
    /// </summary>
    public double dtvv
    {
        get => dtvv1;
        set => dtvv1 = value;
    }

    /// <summary>
    /// Добавка при ходе вниз, с
    /// </summary>
    public double dtvn
    {
        get => dtvn1;
        set => dtvn1 = value;
    }

    /// <summary>
    /// Коэффициент затухания
    /// </summary>
    public double Kzat 
    {
        get => Kzat1;
        set => Kzat1 = value;
    }

    /// <summary>
    /// Период колебаний
    /// </summary>
    public double Tkol
    {
        get => Tkol1;
        set => Tkol1 = value;
    }

    /// <summary>
    /// Величина изменения динамического уровня в скважине, м
    /// </summary>
    public double dhdin
    {
        get => dhdin1;
        set => dhdin1 = value;
    }

    /// <summary>
    /// Разность пластового и забойного (перед остановкой) давлений с использованием поправочного коэффициента, Па
    /// </summary>
    public double DPpzo1 
    {
        get => DPpzo11;
        set => DPpzo11 = value;
    }

    /// <summary>
    /// Значение времени, являющееся критерием для выбора формулы расчета забойного давления, с
    /// </summary>
    public double tzab1
    {
        get => tzab11;
        set => tzab11 = value;
    }

    /// <summary>
    /// Дебит скважины перед изменением режима работы, м3/с
    /// </summary>
    public double Qrezh
    {
        get => Qrezh1;
        set => Qrezh1 = value;
    }

    /// <summary>
    /// Уровень жидкости в НКТ, м
    /// </summary>
    public double htr
    {
        get => htr1;
        set => htr1 = value;
    }

    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта, м3
    /// </summary>
    public double Vzhgzab
    {
        get => Vzhgzab1;
        set => Vzhgzab1 = value;
    }

    /// <summary>
    /// Объем жидкости глушения скважины, находящейся в пласте, м3
    /// </summary>
    public double Vzhgpl
    {
        get => Vzhgpl1;
        set => Vzhgpl1 = value;
    }

    /// <summary>
    /// Объем жидкости глушения скважины, попавшей в пласт в результате промывки, м3
    /// </summary>
    public double Vprom
    {
        get => Vprom1;
        set => Vprom1 = value;
    }

    /// <summary>
    /// Общий напор, который необходимо создать ШСНУ, м
    /// </summary>
    public double Hobsh
    {
        get => Hobsh1;
        set => Hobsh1 = value;
    }

    /// <summary>
    /// Общий напор, который необходимо создать УЭЦН, м
    /// </summary>
    public double Hobshu
    {
        get => Hobshu1;
        set => Hobshu1 = value;
    }

    /// <summary>
    /// Объем жидкости глушения, поступившей на поверхность, м3
    /// </summary>
    public double Vzhgpov 
    {
        get => Vzhgpov1;
        set => Vzhgpov1 = value;
    }

    /// <summary>
    /// Объем жидкости глушения скважины, поступившей из пласта при остановке скважины, м3
    /// </summary>
    public double Vost
    {
        get => Vost1;
        set => Vost1 = value;
    }
    
    /// <summary>
    /// Критическая депрессия, при которой вода начнет поступать в скважину, Па 
    /// </summary>
    public double DPkr
    {
        get => DPkr1;
        set => DPkr1 = value;
    }
     
    /// <summary>
    /// Критическая депрессия, при которой вода начнет поступать в скважину, МПа 
    /// </summary>
    public double DPkrMPa
    {
        get => DPkr1 * 1.0E-6;
        set => DPkr1 = value / 1.0E-6;
    }

    /// <summary>
    /// Коэффициент, характеризующий дебит поступающей жидкости из пласта в скважину, м3/с 
    /// </summary>
    public double Bzh
    {
        get => Bzh1;
        set => Bzh1 = value; 
    }

    /// <summary>
    /// Коэффициент, характеризующий дебит поступающей жидкости из пласта в скважину, м3/сут 
    /// </summary>
    public double Bzhsut
    {
        get => Bzh1 / 1.157E-5;
        set => Bzh1 = value * 1.157E-5; 
    }

    /// <summary>
    /// Коэффициент макрошероховатости пласта, м 
    /// </summary>
    public double Kmshpl
    {
        get => Kmshpl1;
        set => Kmshpl1 = value;
    }

    /// <summary>
    /// Коэффициент фильтрационного сопротивления 
    /// </summary>
    public double Afs
    {
        get => Afs1;
        set => Afs1 = value;
    }
    
    /// <summary>
    /// Коэффициент фильтрационного сопротивления 
    /// </summary>
    public double Bfs
    {
        get => Bfs1;
        set => Bfs1 = value;
    }

    /// <summary>
    /// Коэффициент совершенства скважины по степени вскрытия пласта, д. ед.
    /// </summary>
    public double C1ss
    {
        get => C1ss1;
        set => C1ss1 = value; 
    }

    /// <summary>
    /// Коэффициент совершенства скважины по характеру вскрытия пласта 
    /// </summary>
    public double C2ss
    {
        get => C2ss1;
        set => C2ss1 = value; 
    }

    /// <summary>
    /// Коэффициент совершенства скважины по степени вскрытия пласта, 1/м
    /// </summary>
    public double C3ss
    {
        get => C3ss1;
        set => C3ss1 = value; 
    }

    /// <summary>
    /// Коэффициент совершенства скважины по характеру вскрытия пласта, 1/м
    /// </summary>
    public double C4ss
    {
        get => C4ss1;
        set => C4ss1 = value; 
    }

    /// <summary>
    /// Значение стандартного давления, Па
    /// </summary>
    public double Pst
    {
        get => Pst1;
        set => Pst1 = value; 
    }

    /// <summary>
    /// Значение стандартной температуры, К
    /// </summary>
    public double Tst
    {
        get => Tst1;
        set => Tst1 = value; 
    }

    /// <summary>
    /// Плотность газа при стандартных условиях, кг/м3
    /// </summary>
    public double Rogst
    {
        get => Rogst1;
        set => Rogst1 = value; 
    }

    /// <summary>
    /// Вязкость газа, Па*с
    /// </summary>
    public double Mjug
    {
        get => Mjug1;
        set => Mjug1 = value; 
    }
    
    /// <summary>
    /// Вязкость газа, мПа•с
    /// </summary>
    public double MjugmPas
    {
        get => Mjug1 * 1.0E3;
        set => Mjug1 = value / 1.0E3;
    }

    /// <summary>
    /// Коэффициент, зависящий от ФЕС пласта
    /// </summary>
    public double AlfaFes
    {
        get => AlfaFes1;
        set => AlfaFes1 = value;
    }

    /// <summary>
    /// Коэффициент, зависящий от ФЕС пласта
    /// </summary>
    public double BettaFes
    {
        get => BettaFes1;
        set => BettaFes1 = value;
    }

    /// <summary>
    /// Коэффициент квадратичного фильтрационного сопротивления, Па2/(м3/с)2
    /// </summary>
    public double bkv
    {
        get => bkv1;
        set => bkv1 = value; 
    }

    /// <summary>
    /// Коэффициент сверхсжимаемости газа в пластовых условиях, д. ед.
    /// </summary>
    public double Zpl
    {
        get => Zpl1;
        set => Zpl1 = value; 
    }

    /// <summary>
    /// Забойное давление перед изменением режима работы, Па
    /// </summary>
    public double Pzrezh
    {
        get => Pzrezh1;
        set => Pzrezh1 = value; 
    }

    /// <summary>
    /// Атмосферное давление, Па
    /// </summary>
    public double Patm
    {
        get => Patm1;
        set => Patm1 = value; 
    }

    /// <summary>
    /// Дебит скважины по воде, м3/с
    /// </summary>
    public double Qzh
    {
        get => Qzh1;
        set => Qzh1 = value;
    }

    /// <summary>
    /// Объемный расход газа при Р=Рst и Т=Тst, м3/с
    /// </summary>
    public double Qg0
    {
        get => Qg01;
        set => Qg01 = value; 
    }
    
    /// <summary>
    /// Объемный расход газа при Р и Т, м3/с
    /// </summary>
    public double Qgpt
    {
        get => Qgpt1;
        set => Qgpt1 = value;
    }

    /// <summary>
    /// Температура газа после штуцера, К
    /// </summary>
    public double Tsep 
    {
        get => Tsep1;
        set => Tsep1 = value;
    }

    /// <summary>
    /// Потери давления в сепараторе, Па
    /// </summary>
    public double DPsep
    {
        get => DPsep1;
        set => DPsep1 = value; 
    }
    
    /// <summary>
    /// Усредненное значение давления на ДИКТе, Па
    /// </summary>
    public double PdiktMdl
    {
        get => PdiktMdl1;
        set => PdiktMdl1 = value;
    }
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по первой формуле для ДИКТ, Па
    /// </summary>
    public double Pdikt1
    {
        get => Pdikt11;
        set => Pdikt11 = value;
    }
    
    /// <summary>
    /// Давление на ДИКТе, рассчитанное по второй формуле для ДИКТ, Па
    /// </summary>
    public double Pdikt2
    {
        get => Pdikt21;
        set => Pdikt21 = value;
    }

    /// <summary>
    /// ???, K/Па
    /// </summary>
    public double Dbuf
    {
        get => Dbuf1;
        set => Dbuf1 = value;
    }

    /// <summary>
    /// Температура на ДИКТе, К
    /// </summary>
    public double Tdikt
    {
        get => Tdikt1;
        set => Tdikt1 = value;
    }

    /// <summary>
    /// Относительная плотность газа, кг/м3
    /// </summary>
    public double Rootn
    {
        get => Rootn1;
        set => Rootn1 = value; 
    }

    /// <summary>
    /// Признак выбора ДИКТ: 0 - ДИКТ 50 мм, 1 - ДИКТ 100 мм
    /// </summary>
    public double PrDikt
    {
        get => PrDikt1;
        set => PrDikt1 = value;
    }

    /// <summary>
    /// Коэффициент
    /// </summary>
    public double Cdikt
    {
        get => Cdikt1;
        set => Cdikt1 = value;
    }

    /// <summary>
    /// Коэффициент быстроходности ступени
    /// </summary>
    public double ns
    {
        get => ns1;
        set => ns1 = value;
    }

    /// <summary>
    /// Частота вращения вала насоса, 1/c
    /// </summary>
    public double nbp
    {
        get => nbp1;
        set => nbp1 = value; 
    }

    /// <summary>
    /// Подача насоса при работе на воде в оптимальном режиме, м3/с
    /// </summary>
    public double Qopt
    {
        get => Qopt1;
        set => Qopt1 = value; 
    }
    /// <summary>
    /// Напор насоса при работе на воде в оптимальном режиме, м
    /// </summary>
    public double Hopt
    {
        get => Hopt1;
        set => Hopt1 = value; 
    }

    /// <summary>
    /// Число ступеней в насосе, шт.
    /// </summary>
    public double Zst
    {
        get => Zst1;
        set => Zst1 = value; 
    }

    /// <summary>
    /// Подача насоса при работе на воде в соответственном режиме, м3/с
    /// </summary>
    public double Qi 
    {
        get => Qi1;
        set => Qi1 = value;
    }

    /// <summary>
    /// Напор насоса при работе на воде в соответственном режиме, м
    /// </summary>
    public double Hi 
    {
        get => Hi1;
        set => Hi1 = value;
    }

    /// <summary>
    /// Коэффициент зависимости напора и подачи от вязкости
    /// </summary>
    public double Khq
    {
        get => Khq1;
        set => Khq1 = value;
    }

    /// <summary>
    /// Коэффициент зависимости КПД от вязкости 
    /// </summary>
    public double Kk
    {
        get => Kk1;
        set => Kk1 = value;
    }

    /// <summary>
    /// Теплоемкость нефти, Дж/(кг*К) 
    /// </summary>
    public double Cn
    {
        get => Cn1;
        set => Cn1 = value;
    }

    /// <summary>
    /// КПД насоса, д. е.
    /// </summary>
    public double EtaN
    {
        get => EtaN1;
        set => EtaN1 = value; 
    }

    /// <summary>
    /// КПД двигателя, д. е.
    /// </summary>
    public double EtaD
    {
        get => EtaD1;
        set => EtaD1 = value;
    }

    /// <summary>
    /// Теплоемкость воды, Дж/(кг*К) 
    /// </summary>
    public double Cv
    {
        get => Cv1;
        set => Cv1 = value; 
    }
    /// <summary>
    /// Теплоемкость смеси, Дж/(кг*К) 
    /// </summary>
    public double Ccm
    {
        get => Ccm1;
        set => Ccm1 = value;
    }

    /// <summary>
    /// Мощность, потребляемая насосом, Вт 
    /// </summary>
    public double Nh 
    {
        get => Nh1;
        set => Nh1 = value;
    }

    /// <summary>
    /// Сумма потерь мощности в ПЭД, Вт 
    /// </summary>
    public double SNDpot
    {
        get => SNDpot1;
        set => SNDpot1 = value;
    }

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double b2
    {
        get => b21;
        set => b21 = value; 
    }

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double c2
    {
        get => c21;
        set => c21 = value; 
    }

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double d2
    {
        get => d21;
        set => d21 = value; 
    }

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double b3
    {
        get => b31;
        set => b31 = value; 
    }

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double c3
    {
        get => c31;
        set => c31 = value; 
    }

    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double b4
    {
        get => b41;
        set => b41 = value; 
    }
    
    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double c4
    {
        get => c41;
        set => c41 = value; 
    }
    
    /// <summary>
    /// Эмпирический коэффициент 
    /// </summary>
    public double b5
    {
        get => b51;
        set => b51 = value; 
    }

    /// <summary>
    /// ???, Вт 
    /// </summary>
    public double Ng
    {
        get => Ng1;
        set => Ng1 = value; 
    }

    /// <summary>
    /// Температура перегрева ПЭД, К
    /// </summary>
    public double tdp
    {
        get => tdp1;
        set => tdp1 = value;
    }

    /// <summary>
    /// Коэффициент, учитывающий изменение температуры перегрева от количества воды и свободного газа
    /// </summary>
    public double Kt
    {
        get => Kt1;
        set => Kt1 = value;
    }

    /// <summary>
    /// Коэффициент уменьшения потерь в ПЭД
    /// </summary>
    public double Kup
    {
        get => Kup1;
        set => Kup1 = value;
    }

    /// <summary>
    /// Температура охлаждающей жидкости, K
    /// </summary>
    public double Tohl
    {
        get => Tohl1;
        set => Tohl1 = value; 
    }

    /// <summary>
    /// Сумма потерь мощности в ПЭД при реальной температуре, Вт 
    /// </summary>
    public double SN
    {
        get => SN1;
        set => SN1 = value;
    }

    /// <summary>
    /// ???, Вт 
    /// </summary>
    public double SNGpot
    {
        get => SNGpot1;
        set => SNGpot1 = value; 
    }

    /// <summary>
    /// Температура двигателя, K
    /// </summary>
    public double Td
    {
        get => Td1;
        set => Td1 = value;
    }

    /// <summary>
    /// Фактическая скорость газа, м/с
    /// </summary>
    public double Vgf
    {
        get => Vgf1;
        set => Vgf1 = value;
    }

    /// <summary>
    /// Критическая скорость газа, м/с
    /// </summary>
    public double Vgkr
    {
        get => Vgkr1;
        set => Vgkr1 = value;
    }

    /// <summary>
    /// Температура на забое скважины, К
    /// </summary>
    public double Tz
    {
        get => Tz1;
        set => Tz1 = value;
    }

    /// <summary>
    /// Диаметр капелек воды, м
    /// </summary>
    public double dv
    {
        get => dv1;
        set => dv1 = value; 
    }

    /// <summary>
    /// Уровень жидкости в ЭК, м
    /// </summary>
    public double Hzhek
    {
        get => Hzhek1;
        set => Hzhek1 = value;
    }

    /// <summary>
    /// Уровень жидкости в НКТ, м
    /// </summary>
    public double Hzhnkt
    {
        get => Hzhnkt1;
        set => Hzhnkt1 = value;
    }

    /// <summary>
    /// Массовый расход газа, кг/с
    /// </summary>
    public double G
    {
        get => G1;
        set => G1 = value;
    }

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на уровне пласта, K/Па
    /// </summary>
    public double Dpl
    {
        get => Dpl1;
        set => Dpl1 = value;
    }

    /// <summary>
    /// Разность температур на уровне пласта и на забое, K
    /// </summary>
    public double DTplz
    {
        get => DTplz1;
        set => DTplz1 = value;
    }

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на забое, K/Па
    /// </summary>
    public double Dz
    {
        get => Dz1;
        set => Dz1 = value;
    }

    /// <summary>
    /// Теплоемкость газа, Дж/(кг*К) 
    /// </summary>
    public double Cg
    {
        get => Cg1;
        set => Cg1 = value; 
    }

    /// <summary>
    /// Теплоемкость горных пород пласта, Дж/(кг*К) 
    /// </summary>
    public double Cgp
    {
        get => Cgp1;
        set => Cgp1 = value; 
    }
    
    /// <summary>
    ///  Время с начала работы скважины, секунды
    /// </summary>
    public double tn
    {
        get => tn1;
        set => tn1 = value; 
    }

    /// <summary>
    /// ???, ???
    /// </summary>
    public double fes
    {
        get => fes1;
        set => fes1 = value;
    }
    
    /// <summary>
    ///  Теплопроводность горных пород, Вт/(м*К)
    /// </summary>
    public double Lgp
    {
        get => Lgp1;
        set => Lgp1 = value; 
    }
    
    /// <summary>
    ///  Температурный градиент, К/м
    /// </summary>
    public double Tgr
    {
        get => Tgr1;
        set => Tgr1 = value; 
    }
    
    /// <summary>
    ///  Коэффициент
    /// </summary>
    public double Teta
    {
        get => Teta1;
        set => Teta1 = value;
    }

    /// <summary>
    ///  Коэффициент
    /// </summary>
    public double KS
    {
        get => KS1;
        set => KS1 = value;
    }
    
    /// <summary>
    ///  Средняя температура, K
    /// </summary>
    public double Tcp
    {
        get => Tcp1;
        set => Tcp1 = value;
    }
    
    /// <summary>
    ///  Среднее давление, Па
    /// </summary>
    public double Pcp
    {
        get => Pcp1;
        set => Pcp1 = value;
    }
    
    /// <summary>
    /// Относительная плотность газа, кг/м3
    /// </summary>
    public double Rogotn
    {
        get => Rogotn1;
        set => Rogotn1 = value; 
    }  

    /// <summary>
    /// Коэффициент гидравлического трения
    /// </summary>
    public double Lambda
    {
        get => Lambda1;
        set => Lambda1 = value; 
    } 

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на спуске НКТ, K/Па
    /// </summary>
    public double Dsp
    {
        get => Dsp1;
        set => Dsp1 = value;
    }

    /// <summary>
    /// Температура на спуске НКТ, K
    /// </summary>
    public double Tsp
    {
        get => Tsp1;
        set => Tsp1 = value; 
    } 

    /// <summary>
    /// Давление на спуске НКТ, Па
    /// </summary>
    public double Psp
    {
        get => Psp1;
        set => Psp1 = value; 
    } 

    /// <summary>
    /// Давление на уровне жидкости в ЭК, Па
    /// </summary>
    public double Pur1
    {
        get => Pur11;
        set => Pur11 = value;
    }
    /// <summary>
    /// Давление на уровне жидкости в НКТ, Па
    /// </summary>
    public double Pur2
    {
        get => Pur21;
        set => Pur21 = value;
    }

    /// <summary>
    /// Дифференциальный коэффициент Джоуля-Томпсона на ДИКТе, K/Па
    /// </summary>
    public double Ddikt
    {
        get => Ddikt1;
        set => Ddikt1 = value;
    }

    /// <summary>
    /// Коэффициент
    /// </summary>
    public double KA
    {
        get => KA1;
        set => KA1 = value;
    }
    
    /// <summary>
    /// Коэффициент
    /// </summary>
    public double KB
    {
        get => KB1;
        set => KB1 = value;
    }
    
    /// <summary>
    /// Коэффициент
    /// </summary>
    public double Kb
    {
        get => Kb1;
        set => Kb1 = value; 
    }

    /// <summary>
    /// Плотность продавочной жидкости, кг/м3
    /// </summary>
    public double Roprodzh
    {
        get => Roprodzh1;
        set => Roprodzh1 = value; 
    }

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на I скорости, м3/с 
    /// </summary>
    public double An700Q1 
    {
        get => An700Q11;
        set => An700Q11 = value; 
    }
     
    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на I скорости, л/с 
    /// </summary>
    public double An700Q1ls
    {
        get => An700Q11 * 1.0E3;
        set => An700Q11 = value / 1.0E3;
    }
    
    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на II скорости, м3/с 
    /// </summary>
    private double An700Q2
    {
        get => An700Q21;
        set => An700Q21 = value; 
    }

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на II скорости, л/с 
    /// </summary>
    public double An700Q2ls
    {
        get => An700Q21 * 1.0E3;
        set => An700Q21 = value / 1.0E3;
    }
    
    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на III скорости, м3/с 
    /// </summary>
    private double An700Q3
    {
        get => An700Q31;
        set => An700Q31 = value; 
    }
     
    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на III скорости, л/с 
    /// </summary>
    public double An700Q3ls
    {
        get => An700Q31 * 1.0E3;
        set => An700Q31 = value / 1.0E3;
    }

    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на IV скорости, м3/с 
    /// </summary>
    private double An700Q4
    {
        get => An700Q41;
        set => An700Q41 = value; 
    }
     
    /// <summary>
    /// Подача насосного агрегата 4АН-700 (при диаметре плунжера 100 мм) на IV скорости, л/с 
    /// </summary>
    public double An700Q4ls
    {
        get => An700Q41 * 1.0E3;
        set => An700Q41 = value / 1.0E3;
    }

    /// <summary>
    /// Диаметр перфорационных отверстий, м
    /// </summary>
    public double dpo
    {
        get => dpo1;
        set => dpo1 = value;
    }

    /// <summary>
    /// Общее число перфорационных отверстий, м
    /// </summary>
    public int npo
    {
        get => npo1;
        set => npo1 = value;
    }

    /// <summary>
    /// Коэффициент расхода, зависящий от характера истечения жидкости, д. ед.
    /// </summary>
    public double Kdelta 
    {
        get => Kdelta1;
        set => Kdelta1 = value;
    }

    /// <summary>
    /// Приёмистость скважины , м3/с
    /// </summary>
    public double Qc 
    {
        get => Qc1;
        set => Qc1 = value;
    }

    /// <summary>
    /// Приёмистость скважины, м3/мин
    /// </summary>
    public double Qcm 
    {
        get => Qc1 / 0.0083;
        set => Qc1 = value * 0.0083;
    }

    /// <summary>
    /// Коэффициент увеличения проницаемости ПЗП после ГРП, д. ед.
    /// </summary>
    public double Psigrp
    {
        get => Psigrp1;
        set => Psigrp1 = value;
    }

    /// <summary>
    /// Коэффициент Пуассона горных пород, д. ед.
    /// </summary>
    public double KPgp 
    {
        get => KPgp1;
        set => KPgp1 = value;
    }

    /// <summary>
    /// Средняя плотность вышележащих пород, кг/м3
    /// </summary>
    public double Rovp
    {
        get => Rovp1;
        set => Rovp1 = value;
    }

    /// <summary>
    /// Давление расслоения пород, Па
    /// </summary>
    public double Prp
    {
        get => Prp1;
        set => Prp1 = value;
    }
     
    /// <summary>
    /// Давление расслоения пород, МПа 
    /// </summary>
    public double PrpMPa
    {
        get => Prp1 * 1.0E-6;
        set => Prp1 = value / 1.0E-6;
    }

    /// <summary>
    /// Плотность жидкости разрыва, кг/м3
    /// </summary>
    public double Rozhr 
    {
        get => Rozhr1;
        set => Rozhr1 = value;
    }

    /// <summary>
    /// Плотность жидкости-пропантоносителя, кг/м3
    /// </summary>
    public double Rozhp 
    {
        get => Rozhp1;
        set => Rozhp1 = value;
    }

    /// <summary>
    /// Вязкость жидкости разрыва, Па•с
    /// </summary>
    public double Mjuzhr
    {
        get => Mjuzhr1;
        set => Mjuzhr1 = value;
    }
    
    /// <summary>
    /// Вязкость жидкости разрыва, мПа•с
    /// </summary>
    public double MjuzhrmPas
    {
        get => Mjuzhr1 * 1.0E3;
        set => Mjuzhr1 = value / 1.0E3;
    }

    /// <summary>
    /// Массовая доля пропанта в жидкости-пропантоносителе, кг/м3
    /// </summary>
    public double C0pr
    {
        get => C0pr1;
        set => C0pr1 = value;
    }

    /// <summary>
    /// Плотность пропанта, кг/м3
    /// </summary>
    public double Ropr
    {
        get => Ropr1;
        set => Ropr1 = value;
    }

    /// <summary>
    /// Требуемое количество пропанта, кг
    /// </summary>
    public double Qpr
    {
        get => Qpr1;
        set => Qpr1 = value;
    }
     
    /// <summary>
    /// Требуемое количество пропанта, т
    /// </summary>
    public double Qprt
    {
        get => Qpr1 * 1.0E-3;
        set => Qpr1 = value / 1.0E-3;
    }

    /// <summary>
    /// Необходимое превышение забойного давления над давлением разрыва, д. ед.
    /// </summary>
    public double Azr
    {
        get => Azr1;
        set => Azr1 = value;
    }

    /// <summary>
    /// Коэффициент технического состояния агрегата, д. ед.
    /// </summary>
    public double Kts
    {
        get => Kts1;
        set => Kts1 = value;
    }

    /// <summary>
    /// Ширина (раскрытость) трещины на стенке скважины, м
    /// </summary>
    public double Ksi0 
    {
        get => Ksi01;
        set => Ksi01 = value;
    }
        
    /// <summary>
    /// Ширина (раскрытость) трещины на стенке скважины, мм
    /// </summary>
    public double Ksi0mm
    {
        get => Ksi01 * 1.0E3;
        set => Ksi01 = value / 1.0E3;
    }

    /// <summary>
    /// Давление разрыва пласта, Па
    /// </summary>
    public double Prazr
    {
        get => Prazr1;
        set => Prazr1 = value;
    }

    /// <summary>
    /// Вертикальная составляющая горного давления, Па
    /// </summary>
    public double Pvg
    {
        get => Pvg1;
        set => Pvg1 = value;
    }
    
    /// <summary>
    /// Горизонтальная составляющая горного давления, Па
    /// </summary>
    public double Pgg
    {
        get => Pgg1;
        set => Pgg1 = value;
    }

    /// <summary>
    /// Расчетное давление гидроразрыва на забое скважины, Па
    /// </summary>
    public double Pzabr
    {
        get => Pzabr1;
        set => Pzabr1 = value;
    }

    /// <summary>
    /// Потери давления на трение жидкости разрыва в НКТ, Па
    /// </summary>
    public double DPzhrnkt
    {
        get => DPzhrnkt1;
        set => DPzhrnkt1 = value;
    }

    /// <summary>
    /// Потери давления на трение жидкости-пропантоносителя в НКТ, Па
    /// </summary>
    public double DPzhpnkt
    {
        get => DPzhpnkt1;
        set => DPzhpnkt1 = value;
    }

    /// <summary>
    /// Критическая скорость ЖР/ЖП в НКТ, м/с
    /// </summary>
    public double Wkrnkt 
    {
        get => Wkrnkt1;
        set => Wkrnkt1 = value;
    }

    /// <summary>
    /// Фактическая средняя скорость ЖР/ЖП в НКТ, м/с
    /// </summary>
    public double Wfnkt 
    {
        get => Wfnkt1;
        set => Wfnkt1 = value;
    }

    /// <summary>
    /// Плотность ЖР или ЖП, кг/м3 
    /// </summary>
    public double Rozhrzhp
    {
        get => Rozhrzhp1;
        set => Rozhrzhp1 = value;
    }

    /// <summary>
    /// Плотность ЖР, ЖП или ПЖ, кг/м3
    /// </summary>
    public double Rozhrzhppzh
    {
        get => Rozhrzhppzh1;
        set => Rozhrzhppzh1 = value;
    }

    /// <summary>
    /// Потери давления в перфорационных отверстиях при продавливании цементного раствора в призабойную зону пласта, Па
    /// </summary>
    public double DPpo
    {
        get => DPpo1;
        set => DPpo1 = value;
    }

    /// <summary>
    /// Объемная доля пропанта
    /// </summary>
    public double Cprop
    {
        get => Cprop1;
        set => Cprop1 = value;
    }

    /// <summary>
    /// Вязкость жидкости-пропантоносителя, Па•с
    /// </summary>
    public double Mjuzhp
    {
        get => Mjuzhp1;
        set => Mjuzhp1 = value;
    }  

    /// <summary>
    /// Требуемый объем жидкости разрыва, м3
    /// </summary>
    public double Vzhr
    {
        get => Vzhr1;
        set => Vzhr1 = value;
    }  

    /// <summary>
    /// Требуемый объем жидкости разрыва с пропантом (ЖП), м3
    /// </summary>
    public double Vzhp
    {
        get => Vzhp1;
        set => Vzhp1 = value;
    }  

    /// <summary>
    /// Общий объем закачиваемой жидкости при ГРП, м3
    /// </summary>
    public double Vzhgrp
    {
        get => Vzhgrp1;
        set => Vzhgrp1 = value;
    }  

    /// <summary>
    /// Минимальный темп закачки при создании вертикальной трещины, м3/c
    /// </summary>
    public double Qminv
    {
        get => Qminv1;
        set => Qminv1 = value;
    }  

    /// <summary>
    /// Рабочее давление агрегата, Па
    /// </summary>
    public double Pp
    {
        get => Pp1;
        set => Pp1 = value;
    }
    /// <summary>
    /// Подача агрегата при данном Pp, м3/c
    /// </summary>
    public double qp
    {
        get => qp1;
        set => qp1 = value;
    }

    /// <summary>
    /// Общая продолжительность процесса гидроразрыва, c
    /// </summary>
    public double tgr
    {
        get => tgr1;
        set => tgr1 = value;
    }  

    /// <summary>
    /// Коэффициент
    /// </summary>
    public double Ans
    {
        get => Ans1;
        set => Ans1 = value;
    }
    
    /// <summary>
    /// Коэффициент
    /// </summary>
    public double Bns
    {
        get => Bns1;
        set => Bns1 = value;
    }
    
    /// <summary>
    /// Расход скважины при давлении гидроразрыва, м3/с
    /// </summary>
    public double Qgrp
    {
        get => Qgrp1;
        set => Qgrp1 = value;
    }
    
    /// <summary>
    /// Буферное давление, рассчитанное при помощи распределения давления, Па
    /// </summary>
    public double Pbufpk
    {
        get => Pbufpk1;
        set => Pbufpk1 = value;
    }
    
    /// <summary>
    /// Давление, с которого начинается расчет, Па
    /// </summary>
    public double Pens1
    {
        get => Pens11;
        set => Pens11 = value;
    }

    
    /// <summary>
    /// Давление, с которого начинается расчет, Па
    /// </summary>
    public double Pens2
    {
        get => Pens21;
        set => Pens21 = value;
    }
    
    /// <summary>
    /// Объемный коэффициент воды
    /// </summary>
    public double bv
    {
        get => bv1;
        set => bv1 = value;
    }
    
    /// <summary>
    /// Коэффициент приемистости скважины, м3/( с*Па)
    /// </summary>
    public double Kprm
    {
        get => Kprm1;
        set => Kprm1 = value;
    }

    /// <summary>
    /// Удельный коэффициент приемистости скважины, м2/(с* Па)
    /// </summary>
    public double Kprmud
    {
        get => Kprmud1;
        set => Kprmud1 = value;
    }


  
    
}